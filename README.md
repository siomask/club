### WARNING
we store db and uploaded files in docker containers, in case you will decide to kill containers and clean up, make sure you
made db dumbs and saved files from ```resources/uploads```, otherwise you will loose all data after cleaning all containers
```
- run docker exec tclub-mongo sh -c 'exec mongodump --db somedb --gzip --archive' > dump_`date "+%Y-%m-%d"`.gz  - will make a db dump
- run docker cp uploads tclub-admin:resources/uploads  - will dump all upload files
```
### IMPORTANT
- Dockerfile.admin - should have correct REMOTE_UPLOAD_HOST(host where app was deployed)
- Dockerfile.mobile - should have correct REMOTE_UPLOAD_HOST(host where app was deployed)

### TClub
- mobile [app](https://marvelapp.com/7iacehj/screen/63588275)
- mobile [BE](http://35.217.32.24:7005/graphql)
- admin [BE](http://35.217.32.24:7006/graphql)
- admin [dashboard](http://35.217.32.24:3002)
- dev db seed (utils/db/tclub)  
- postman API  (utils/postman)  

# DEV
```
> yarn start:admin - to start admin process

> yarn start:mobile - to start mobile process

```

# Install [Docker and Compose](https://docs.docker.com/install)
```
- run docker-compose up -d

In case you'll need some docker cleanup
docker rmi $(docker images -a -q)
docker stop $(docker ps -a -q)
docker rm $(docker ps -a -q)
docker system prune -af
```


# Development
### Cron
```
npm run start:cron -- start cron to check events state and send notifications
```

### Admin
```
npm run start:admin -- up the admin api
```
### Mobile
```
npm run start:mobile -- up the mobile api
```
