
TCLUB-215 - this is for Event backend implementation.

-------

1) Need to check card expiry date during auth or authorization / 1h
2) Error handling should be revisited / 4h
3) Allow admin to update phone number for Tmember and update info under Authy / 3h
4) Test TMember removal and Authy removal / 3h

5) Create basic Data models for mobile app / 4-6h

6) Create server and deploy all under server / 11h
    - install script for mongo 3h
    - install script for nginx and configure 3h
    - install script for nodejs, npm, git etc ... 2h 
    - Dockerize everything and test - 6h


7)  Allow admin to add/edit members (mostly done but need to cleanup) 4h
    - Allow admin to create a member 
    - Allow admin to remove a member 
    - Allow admin to update a member 


8)  Allow admin to add/edit concierge (mostly done but need to cleanup) 8h
    - Allow admin to create a concierge and send Email  
    - Allow admin to remove a concierge 
    - Allow admin to update a concierge 
    - Allow admin to update a concierge email (resend invite)
    - Consierge login with email/pass

9) Create EmailService based on Sendgrid, maybe it'll also require email templates created (Who will do it ?) 3h
