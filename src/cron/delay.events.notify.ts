require('dotenv').config();
const cron = require('node-cron');
const mongoose = require('mongoose');
import { FirebaseEntity } from '../modules/core/providers/firebase.entity';
import { NOTIFICATINOS } from '../modules/core/providers/notifications';

function openConnect() {
  return new Promise((resolve, reject) => {
    const uri = `${process.env.MASTER_MONGO_URL}/${process.env.MASTER_MONGO_DATABASE}`;
    console.log('------', uri);
    mongoose.connect(uri);
    mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
    mongoose.connection.once('open', () => {
      console.log(` db_user:   connected to mongodb`);
      resolve();
    });
  });
}

async function notify(firebaseProvider, event) {
  mongoose.connection
    .collection('tevents'.toLowerCase())
    .update({ _id: event._id }, { $set: { notified: Date.now() } }, function(err, info) {
      if (err) {
        console.log(err);
      } else {
        'Events was updated';
      }
    });
  console.log('Event will start in 1 day ' + event._id);
  let users = await mongoose.connection.collection('t_member_event_info'.toLowerCase()).distinct('memberId', {
    $or: [
      {
        eventId: event._id.toString()
      },
      {
        eventId: event._id
      }
    ]
  });
  await firebaseProvider.notifyUsers(
    NOTIFICATINOS.EVENT_WILL_START({
      object: event,
      users
    })
  );
}

async function checkEvents() {
  const firebaseProvider: FirebaseEntity = new FirebaseEntity(process.env.FIREBASE_CREDENTIALS);
  await openConnect();
  const events = await mongoose.connection
		.collection('tevents'.toLowerCase())
		.find({}, { _id: 1, startDate: 1, title: 1 })
		.toArray();

  for (let i = 0; i < events.length; i++) {
    const timeStart = new Date(events[i].startDate).getTime() - Date.now();
    if (timeStart > 0 && timeStart < 1000 * 60 * 60 * 24) {
      notify(firebaseProvider, events[i]);
    }
  }
  mongoose.connection.close();
}

cron.schedule('* * 1 * *', () => {
  //each 1 am will init checkEvents
  checkEvents();
});
checkEvents();
export {};
