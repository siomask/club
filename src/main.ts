// ** AVAR::DONE
import 'reflect-metadata';
import 'module-alias/register';
import path from 'path';
import * as dotenv from 'dotenv';

let envPostfix;
let debug;

switch (process.env.NODE_ENV) {
	case 'dev':
	case 'development':
		envPostfix = 'dev';
		debug = true;
		break;
	case 'prod':
	case 'production':
		envPostfix = 'prod';
		debug = false;
		break;
	default:
		throw new Error(`process.env.NODE_ENV should be one of those values - [dev, development, prod, production]`);
}

const dotEnvPath = path.resolve(process.cwd(), `.env.${envPostfix}`);

dotenv.config({
	path: dotEnvPath,
	debug
});

import { startServer } from './server';

const startServerPromise = new Promise(startServer);

startServerPromise
	.then(() => {
		console.log('!!! Started TClub !!!!');
	})
	.catch((err) => {
		console.error('!!! ERROR !!!!', err);
	});
