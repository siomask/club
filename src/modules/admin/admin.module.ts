import path from 'path';
import { GraphQLModule } from '@graphql-modules/core';
import { loadSchemaFiles, loadResolversFiles } from 'graphql-toolkit';

import { NamespaceEntityProvider, EntityEnum } from '@tclub/core';
import { GraphEntityParser } from '@sfast/gql-schema-parser';

import { createCoreModule, SchemaProvider, ISession, TokenProvider, TwofaServiceProvider } from '@tclub/core';

import { AdminDBInitProvider, AdminInitProvider } from './providers';
import { isAuthorized, validCred, parseFilter } from './resolvers/middlewares';
import gql from 'graphql-tag';

export interface IAdminModuleContext {
	version: string;
}

const AUTHORIZATION_HEADER_NAME = 'authorization';
let gm: GraphQLModule = null;

export const createAdminModule = (): GraphQLModule => {
	if (gm) return gm;

	gm = new GraphQLModule<{}, {}, IAdminModuleContext>({
		imports: [createCoreModule()],
		typeDefs: ({ injector }) => {
			// ** NOTE:: using the final Graphql generated
			const schemaPath: string = path.resolve(__dirname, './schema/');

			const adminModuleTypeDefs: String[] = loadSchemaFiles(schemaPath);
			const baseTypeDefs: String = injector.get(SchemaProvider).typeDefs;

			const typeDef = [baseTypeDefs, ...adminModuleTypeDefs].join('\n');

			const graphNodeGQLed = gql(typeDef);

			const processedWithGql = new GraphEntityParser(graphNodeGQLed);
			const finalSchema = processedWithGql.getSchema(graphNodeGQLed);

			return processedWithGql.printSchema(finalSchema);
		},
		resolvers: loadResolversFiles(path.resolve(__dirname, './resolvers/')),
		context: async (session: ISession, currentContext, { injector }) => {
			console.log('Admin module context creation');

			const token = session.req.headers[AUTHORIZATION_HEADER_NAME] || session.req.headers['Authorization'];
			return {
				version: '0.0.1',
				token
			};
		},
		providers: [AdminDBInitProvider, AdminInitProvider, TokenProvider, TwofaServiceProvider],
		resolversComposition: {
			'Mutation.login': [validCred()],
			'Query.isAuth': [isAuthorized()],
			'Mutation.removeEventSpeaker': [isAuthorized()],
			'Mutation.updateEventSpeaker': [isAuthorized()],
			'Mutation.addEventSpeaker': [isAuthorized()],
			'Mutation.addEventAgenda': [isAuthorized()],
			'Mutation.updateEventAgenda': [isAuthorized()],
			'Mutation.removeEventAgenda': [isAuthorized()],
			'Mutation.addPrivilege': [isAuthorized()],
			'Mutation.notifyAllMembers': [isAuthorized()],
			'Mutation.updatePrivilege': [isAuthorized()],
			'Mutation.removePrivilege': [isAuthorized()],
			'Mutation.applyMemberToPrivilege': [isAuthorized()],
			'Mutation.removeMemberFromPrivilege': [isAuthorized()],
			'Mutation.addService': [isAuthorized()],
			'Mutation.updateService': [isAuthorized()],
			'Mutation.removeService': [isAuthorized()],
			'Mutation.addAsset': [isAuthorized()],
			'Mutation.addAssetPeriod': [isAuthorized()],
			'Mutation.editAssetPeriod': [isAuthorized()],
			'Mutation.addMember': [isAuthorized()],
			'Mutation.updateMember': [isAuthorized()],
			'Mutation.blockMember': [isAuthorized()],
			'Mutation.removeMember': [isAuthorized()],
			'Mutation.addEvent': [isAuthorized()],
			'Mutation.updateEvent': [isAuthorized()],
			'Mutation.removeEvent': [isAuthorized()],
			'Mutation.addConcierge': [isAuthorized()],
			'Mutation.updateConcierge': [isAuthorized()],
			'Mutation.removeConcierge': [isAuthorized()],
			'Mutation.confirmEventBookingForUser': [isAuthorized()],
			'Mutation.rejectEventBookingForUser': [isAuthorized()],
			'Query.fetchAllEventSpeakers': [isAuthorized(), parseFilter()],
			'Query.memberList': [isAuthorized(), parseFilter()],
			'Query.memberPrivilegeActivationsList': [isAuthorized(), parseFilter()],
			'Query.memberPrivilegeList': [isAuthorized(), parseFilter()],
			'Query.memberById': [isAuthorized()],
			'Query.memberServicesBookingList': [isAuthorized(), parseFilter()],
			'Query.conciergeList': [isAuthorized(), parseFilter()],
			'Query.conciergeById': [isAuthorized()],
			'Query.privilegeList': [isAuthorized(), parseFilter()],
			'Query.fetchEventBookings': [isAuthorized(), parseFilter()],
			'Query.fetchEventById': [isAuthorized()],
			'Query.fetchEvents': [isAuthorized(), parseFilter()],
			'Query.bookingServiceList': [isAuthorized(), parseFilter()],
			'Query.serviceById': [isAuthorized(), parseFilter()],
			'Query.serviceTypeList': [isAuthorized(), parseFilter()],
			'Query.fetchEventAgenda': [isAuthorized(), parseFilter()],
			'Query.privilegeMembersList': [isAuthorized(), parseFilter()],
			'Query.serviceList': [isAuthorized(), parseFilter()]
		}
	});

	return gm;
};
