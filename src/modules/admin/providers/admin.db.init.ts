import { Injectable, ProviderScope, Inject } from '@graphql-modules/di';

import {
	NamespaceEntityProvider,
	NamespaceConfig,
	SchemaProvider,
	DefaultEnvProvider,
	ASyncInitProvider,
	IEntityModel
} from '@tclub/core';

// ** Init provider - for everything we need for portal initialization
@Injectable({
	scope: ProviderScope.Application
})
export class AdminDBInitProvider extends ASyncInitProvider {
	constructor(
		@Inject(NamespaceEntityProvider) private nsp: NamespaceEntityProvider,
		@Inject(SchemaProvider) private schemaProvider: SchemaProvider,
		@Inject(DefaultEnvProvider) private envProvider: DefaultEnvProvider
	) {
		super('AdminDBInitProvider');
	}

	public async initializer() {
		console.log(`AdminDBInitProvider onInit - STARTED`);
		// ** INITIALIZATION PART
		this.envProvider.onInit();
		this.schemaProvider.onInit();
		this.nsp.onInit();

		const { MASTER_MONGO_URL, MASTER_MONGO_DATABASE, MASTER_MONGO_ARGS } = this.envProvider;

		const tclubNamespace: NamespaceConfig = {
			namespace: 'tclub',
			type: 'mongo',
			url: `${MASTER_MONGO_URL}/${MASTER_MONGO_DATABASE}${MASTER_MONGO_ARGS}`,
			label: 'tclub'
		};

		const tclubEntitites = this.schemaProvider.getNamespaceEntities('tclub');

		await this.nsp.registerEntities('tclub', tclubEntitites);

		await this.nsp.registerNamespace(tclubNamespace);

		this.nsp.ready();
		console.log('AdminDBInitProvider onInit - END');
	}

	public getEntityModel(namespace: string, entityName: string): IEntityModel {
		return this.nsp.getEntityModel(namespace, entityName);
	}
}
