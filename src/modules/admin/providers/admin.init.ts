import { Injectable, ProviderScope, Inject } from '@graphql-modules/di';
import { ASyncInitProvider } from '@tclub/core';

import { AdminDBInitProvider } from './admin.db.init';

@Injectable({
	scope: ProviderScope.Application
})
export class AdminInitProvider extends ASyncInitProvider {
	constructor(@Inject(AdminDBInitProvider) private dbProvider: AdminDBInitProvider) {
		super('AdminInitProvider');
	}

	public async initializer() {
		await this.dbProvider.onInit();

		/* Your code goes here */
		console.log('AdminInitProvider onInit - END');
	}
}
