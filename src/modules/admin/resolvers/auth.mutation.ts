import { ModuleContext } from '@graphql-modules/core';
import { filter, map } from 'lodash';
import { ApolloError } from 'apollo-server-core';

import {
	TwofaServiceProvider,
	DefaultEnvProvider,
	TokenProvider,
	NamespaceEntityProvider,
	EntityEnum,
	AuthyResponseType,
	TokenTypeEnum
} from '@tclub/core';

import { MutationResolvers, TMember } from '@tclub/types';

export const Mutation: any = {
	login: async (root, args, opt: any, info: any) => {
		return {
			token: info.token,
			user: info.user
		};
	}, 
	resetPsw: async (root, args, opt: any, info: any) => {
		return {
			success: true
		};
	}
};
