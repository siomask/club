import { QueryResolvers } from '@tclub/types';

export const Query: any = {
	isAuth: async (root, args, opt: any, info: any) => {
		return {
			token: info.token,
			user: info.user.toObject()
		};
	}
};
