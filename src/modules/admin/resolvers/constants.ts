const base = {
	_id: true,
	uuid: true,
	deletedAt: true,
	createdAt: true,
	updatedAt: true
};
export const tMemberInfoProjection = {
	firstname: true,
	lastname: true,

	privateName: true,

	preview: true,
	phone: true,
	birthday: true,

	isBlocked: true,
	isVerified: true,

	appSettings: true,
	cardInfo: true,

	test: true,
	...base
};

export const tConciergeInfoProjection = {
	username: true,
	firstname: true,
	lastname: true,
	email: true,
	phone: true,
	birthday: true,
	...base
};
export const tAssetsInfoProjection = {
	path: true,
	filename: true,
	mimetype: true,
	encoding: true,
	...base
};
export const tServiceInfoProjection = {
	title: true,
	description: true,
	occupation: true,
	assets: true,
	preview: true,
	price: true,
	...base
};
