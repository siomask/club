import { ModuleContext } from '@graphql-modules/core';
import { TConciergeType} from '@tclub/types';
import { TokenProvider, TokenTypeEnum, NamespaceEntityProvider, EntityEnum, QUERY, POPULATE } from '@tclub/core';
import { ApolloError, AuthenticationError } from 'apollo-server-core';

export const isAuthorized = () => (next) => async (root, args, { injector, token }: any, info) => {
	if (!token) throw new ApolloError('Token is not provided');
	const tokenProvider = injector.get(TokenProvider);
	const { memberId } = tokenProvider.decodeToken(token, TokenTypeEnum.AUTHORIZATION);
	const TMemberModel = injector
		.get(NamespaceEntityProvider)
		.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);

	if (!memberId) throw new AuthenticationError('Token is not valid');
	const user = await TMemberModel.findById(memberId).populate(POPULATE.CONCIERGE(injector));
 
	// tslint:disable-next-line:curly
	if (!user || user.isBlocked) {
		throw new ApolloError('AccountDisabledError:: please contact TClub');
	}
	const own: any = {};
	// if (user.type === TConciergeType.Admin) {
	// } else {
	// 	own.createdBy = user._id;
	// }
	return next(
		root,
		args,
		{ injector, token },
		{ ...info, user, token, userId: user._id.toString(), own, createdBy: user._id, updatedBy: user._id }
	);
};
export const validCred = () => (next) => async (root, args, { injector }: any, info) => {
	const tokenProvider = injector.get(TokenProvider);
	const TMemberModel = injector
		.get(NamespaceEntityProvider)
		.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);
	const user = await TMemberModel.findOne({
		email: args.email
	});
	if (!user || !TMemberModel.isValidPassword(user.password, args.password)) {
		throw new ApolloError('Email or Password is not valid');
	}
	if (user.isBlocked) {
		throw new ApolloError('AccountDisabledError:: please contact TClub');
	}

	const userId = user._id.toString();
	console.log(userId);
	const token = tokenProvider.generatAuthorizationToken(userId);
	return next(root, args, { injector, token }, { ...info, token, user, userId });
};

export const parseFilter = () => (next) => async (root, args, { injector, token }: any, info) => {
	const limit = args.input ? args.input.limit || QUERY.LIMIT : QUERY.LIMIT;
	const skip = args.input ? args.input.skip || QUERY.SKIP : QUERY.SKIP;
	const sort = args.input && args.input.sort ? { [args.input.sort.field]: args.input.sort.dir } : QUERY.SORT;

	return next(
		root,
		args,
		{ injector, token },
		{
			...info,
			limit,
			skip,
			sort
		}
	);
};
