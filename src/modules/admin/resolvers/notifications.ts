import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TEvent, TMemberEventInfo, TEventStatusEnum, TEventBookingStatusEnum } from '@tclub/types';

import {
	NamespaceEntityProvider,
	EntityEnum,
	TwofaServiceProvider,
	AuthyResponseType,
	NOTIFICATINOS,
	POPULATE,
	FirebaseProvider
} from '@tclub/core';
import { ApolloError } from 'apollo-server-core';

export const Mutation: any = {
	notifyAllMembers: async (root, args, { injector }: ModuleContext<any>) => {
		const firebaseProvider = injector.get(FirebaseProvider);
		const TEventBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);
		let users;
		if (args.eventId) {
			const bookQuery: any = {
				eventId: args.eventId,
				bookingStatus: {
					$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
				}
			};

			users = await TEventBookingModel.distinctActive('memberId', bookQuery);
		}
		firebaseProvider.notifyUsers(
			NOTIFICATINOS.ADMIN_NOTIFY_ALL_USERS({
				object: {
					title: args.title,
					text: args.text
				}
			}),
			users
		);
		return { success: true };
	}
};
