import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers } from '@tclub/types';

const shortid = require('shortid');
import { NamespaceEntityProvider, EntityEnum, storeUpload, removeUpload } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';
import { ApolloError } from 'apollo-server-core';

export const Mutation: any = {
	addAssetPeriod: async (root, args: any, { injector }: any, { createdBy }: any) => {
		const TAssetsModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSET_PERIOD);

		const tAsset = await TAssetsModel.create({
			...args.input,
			createdBy
		});
		return (
			await TAssetsModel.findById(tAsset._id).populate([
				{
					path: 'assets',
					model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
				}
			])
		).toObject();
	},
	editAssetPeriod: async (root, args: any, { injector }: any, { updatedBy }: any) => {
		const TAssetsModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSET_PERIOD);

		const tAsset = await TAssetsModel.findByIdAndUpdate(args.id, {
			$set: {
				...args.input,
				updatedBy
			}
		});
    if (!tAsset) throw new ApolloError(`Cant find asset period with id = '${args.id}'`);
		return (
			await TAssetsModel.findById(tAsset._id).populate([
				{
					path: 'assets',
					model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
				}
			])
		).toObject();
	},

	removeAssetPeriod: async (root, args, { injector }: ModuleContext<any>) => {
		const TAssetsModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSET_PERIOD);
		const tAsset = TAssetsModel.findById(args.id);
		if (!tAsset) throw new ApolloError(`Cant find asset period with id = '${args.id}'`);

		await TAssetsModel.findByIdAndRemove(args.id);
		return {
			success: true
		};
	},
	addAsset: async (root, args: any, { injector }: any, { createdBy }: any) => {
		const TAssetsModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS);

		const file = await storeUpload(args.file);
		const tAsset = await TAssetsModel.create({
			...args.input,
			...file,
			createdBy
		});
		return tAsset.toObject();
	},

	removeAsset: async (root, args, { injector }: ModuleContext<any>) => {
		const TAssetsModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS);
		const tAsset = TAssetsModel.findById(args.id);
		if (!tAsset) throw new ApolloError(`Cant find asset with id = '${args.id}'`);

		removeUpload(await TAssetsModel.findByIdAndRemove(args.id));
		return {
			success: true
		};
	}
};
