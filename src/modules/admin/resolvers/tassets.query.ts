import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

import { tAssetsInfoProjection } from './constants';

export const Query: any = {
	assetById: async (root, { id }, { injector }: ModuleContext<IAdminModuleContext>) => {
		const TAssetsModel = injector.get(NamespaceEntityProvider).getEntityModel('tclub', EntityEnum.T_ASSETS);
		const tAsset = await TAssetsModel.findById(id, tAssetsInfoProjection);
		return tAsset.toObject();
	},

	assetsList: async (root, args, { injector }: ModuleContext<any>) => {
		const TAssetsModel = injector.get(NamespaceEntityProvider).getEntityModel('tclub', EntityEnum.T_ASSETS);
		return (await TAssetsModel.findActive({}, tAssetsInfoProjection)).map((el) => el.toObject());
	}
};
