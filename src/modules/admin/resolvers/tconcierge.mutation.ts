import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TConciergeType } from '@tclub/types';
const bCrypt = require('bcrypt-nodejs');

import { NamespaceEntityProvider, EntityEnum, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

import { UserInputError, ApolloError } from 'apollo-server-core';

export const Mutation: any = {
	addConcierge: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { createdBy }: any) => {
		const TConciergeModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);

		try {
			const tConcierge = await TConciergeModel.create({
				...args.input,
				password: bCrypt.hashSync(args.input.password, bCrypt.genSaltSync(10), null),
				isVerified: Date.now(),
				createdBy,
				type: TConciergeType.Concierge
			});
			return (await TConciergeModel.findById(tConcierge._id).populate(POPULATE.CONCIERGE(injector))).toObject();
		} catch (e) {
			if (e.code === 11000) {
				throw new UserInputError(`Email already in use, please input new one`);
			}
			throw e;
		}
	},

	updateConcierge: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		try {
			const TConciergeModel = injector
				.get(NamespaceEntityProvider)
				.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);
			if (args.input.password) {
				args.input.password = bCrypt.hashSync(args.input.password, bCrypt.genSaltSync(10), null);
			}
			const updates = {
				$set: { ...args.input, updatedBy }
			};
			if (!args.input.isBlocked) {
				updates['$unset'] = { isBlocked: 1 };
				delete updates['$set']['isBlocked']
			}
      const tConcierge = await TConciergeModel.findByIdAndUpdate(args.conciergeId,updates);
			return (await TConciergeModel.findById(tConcierge._id).populate(POPULATE.CONCIERGE(injector))).toObject();
		} catch (e) {
			if (e.code === 11000) {
				throw new UserInputError(`Email already in use, please input new one`);
			}
			throw e;
		}
	},

	removeConcierge: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const TConciergeModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);

		const deleted: any = await TConciergeModel.softDelete(args.conciergeId, { updatedBy });
		if (!deleted) throw new ApolloError(`Cant find consierge with conciergeId = '${args.conciergeId}'`);

		return { success: true };
	}
};
