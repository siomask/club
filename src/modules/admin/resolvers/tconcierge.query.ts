import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

export const Query: any = {
	conciergeById: async (root, { id }, { injector }: ModuleContext<IAdminModuleContext>) => {
		const TConciergeModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);
		const tConcierge = await TConciergeModel.findById(id).populate(POPULATE.CONCIERGE(injector));
		return tConcierge.toObject();
	},

	conciergeList: async (root, args, { injector }: ModuleContext<any>, info: any) => {
		const TConciergeModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);
		return {
			data: (
				await TConciergeModel.findActive(info.own)
					.populate(POPULATE.CONCIERGE(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TConciergeModel.countActive()
			}
		};
	}
};
