import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TEvent, TMemberEventInfo, TEventStatusEnum, TEventBookingStatusEnum } from '@tclub/types';

import {
	NamespaceEntityProvider,
	EntityEnum,
	TwofaServiceProvider,
	AuthyResponseType,
	NOTIFICATINOS,
	POPULATE,
	addMemberToEvent,
	FirebaseProvider
} from '@tclub/core';
import { IAdminModuleContext } from '../admin.module';
import { ApolloError } from 'apollo-server-core';

export const Mutation: any = {
	addEventAgenda: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { createdBy }: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventAgendaModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_AGENDA);

		const tEventAgenda = await TEventAgendaModel.create({
			...args.input,
			createdBy
		});

		await TEventModel.update({ _id: args.input.eventId }, { $push: { agenda: tEventAgenda._id } });
		return (
			await TEventAgendaModel.findById(tEventAgenda._id).populate(POPULATE.EVENT_AGENDA(injector))
		).toObject();
	},
	updateEventAgenda: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { updatedBy }: any) => {
		const TEventAgendaModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_AGENDA);

		const tEventAg = await TEventAgendaModel.findByIdAndUpdate(args.input._id, {
			...args.input,
			updatedBy
		});
		if (!tEventAg) {
			throw new ApolloError('No Item exist');
		}
		return (await TEventAgendaModel.findById(tEventAg._id).populate(POPULATE.EVENT_AGENDA(injector))).toObject();
	},
	removeEventAgenda: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { updatedBy }: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventAgendaModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_AGENDA);

		const tEventAg = await TEventAgendaModel.findByIdAndRemove(args._id);

		if (!tEventAg) {
			throw new ApolloError('No Item exist');
		}
		await TEventModel.update({ _id: tEventAg.eventId }, { $pull: { agenda: tEventAg._id } });
		return { success: true };
	}
};
