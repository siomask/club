import { map, isString } from 'lodash';
import moment from 'moment';
import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers, TConciergeType } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, UtilsProvider, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

export const Query: any = {
	fetchEventAgenda: async (root, { eventId }: any, { injector }: any, info: any) => {
		const TEventAgModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_AGENDA);
		const utilsProvider = injector.get(UtilsProvider);

		const query = {
			eventId
		};
		Object.assign(query, info.own);
		return {
			data: (
				await TEventAgModel.findActive(query)
					.populate(POPULATE.EVENT_AGENDA(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TEventAgModel.countActive(query)
			}
		};
	}
};
