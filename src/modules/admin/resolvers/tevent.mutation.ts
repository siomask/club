import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TEvent, TMemberEventInfo, TEventStatusEnum, TEventBookingStatusEnum } from '@tclub/types';

import {
	NamespaceEntityProvider,
	EntityEnum,
	TwofaServiceProvider,
	AuthyResponseType,
	NOTIFICATINOS,
	POPULATE,
	addMemberToEvent,
	FirebaseProvider
} from '@tclub/core';
import { IAdminModuleContext } from '../admin.module';
import { ApolloError } from 'apollo-server-core';

export const Mutation: any = {
	addEvent: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { createdBy }: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const firebaseProvider = injector.get(FirebaseProvider);

		const tEvent = await TEventModel.create({
			...args.input,
			createdBy
		});

		firebaseProvider.notifyUsers(NOTIFICATINOS.NEW_EVENT({ object: tEvent }));
		return (await TEventModel.findById(tEvent._id).populate(POPULATE.EVENTS(injector))).toObject();
	},

	removeEvent: async (root, { _id }: any, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const firebaseProvider = injector.get(FirebaseProvider);
		const ITemModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);
		const deleted: any = await ITemModel.softDelete(_id, { updatedBy });
		if (!deleted) throw new ApolloError(`Cant find item with eventId = '${_id}'`);
		firebaseProvider.notifyUsers(
			NOTIFICATINOS.DELETE_EVENT({ object: deleted }),
			await TEventBookingModel.distinctActive('memberId', { eventId: _id })
		);
		return { success: true };
	},
	updateEvent: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const firebaseProvider = injector.get(FirebaseProvider);
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const object = await TEventModel.findByIdAndUpdate(args.eventId, { ...args.input, updatedBy });
		firebaseProvider.notifyUsers(NOTIFICATINOS.UPDATE_EVENT({ object: object }));
		return (await TEventModel.findById(args.eventId).populate(POPULATE.EVENTS(injector))).toObject();
	},

	confirmEventBookingForUser: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const firebaseProvider = injector.get(FirebaseProvider);
		const TEventBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);

		const { eventId } = args;

		// NOTE:: check if event exists and capacity is allowing
		const tEvent = await TEventModel.findById(eventId);

		// tslint:disable-next-line:curly
		if (!tEvent) {
			throw new ApolloError(`No such event with id ${eventId}`);
		}

		const tEventData: TEvent = tEvent.toObject() as TEvent;

		// tslint:disable-next-line:curly
		if (tEventData.status !== TEventStatusEnum.Open) {
			throw new ApolloError(`You can't book a private event, contact.`);
		}

		// tslint:disable-next-line:curly
		if (tEventData.bookedCapacity === tEventData.capacity) {
			throw new ApolloError(`You can't book an event with full capacity.`);
		}

		const tEventBooking = await TEventBookingModel.findById(args._id);
		// tslint:disable-next-line:curly
		if (!tEventBooking) {
			throw new ApolloError(
				`Could not find a booking order for event( id = '${args.eventId}') and user = '${args.userId}'`
			);
		}

		tEventBooking.bookingStatus = TEventBookingStatusEnum.ConciergeConfirmed;
		await tEventBooking.save();

		// NOTE:: only when approved by concierge we are adding bookedCapacity
		const object = await TEventModel.findOneAndUpdate({ _id: args.eventId }, { $inc: { bookedCapacity: 1 } });

		firebaseProvider.notifyUsers(
			NOTIFICATINOS.ADMIN_CONFIRM_BOOKED_EVENT({ object: { event: object, booking: tEventBooking } }),
			[args.userId]
		);
		return tEventBooking.toObject();
	},

	rejectEventBookingForUser: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const firebaseProvider = injector.get(FirebaseProvider);
		const TEventBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);

		const tEventBooking = await TEventBookingModel.findById(args._id);
		// tslint:disable-next-line:curly
		if (!tEventBooking) {
			throw new ApolloError(
				`Could not find a booking order for event( id = '${args.eventId}') and user = '${args.userId}'`
			);
		}

		tEventBooking.bookingStatus = TEventBookingStatusEnum.ConciergeRejected;
		await tEventBooking.save();

		// NOTE:: only when rejected by concierge we are decreasing bookedCapacity
		const object = await TEventModel.findOneAndUpdate({ _id: args.eventId }, { $inc: { bookedCapacity: -1 } });

		firebaseProvider.notifyUsers(
			NOTIFICATINOS.ADMIN_REJECT_BOOKED_EVENT({ object: { event: object, booking: tEventBooking } }),
			[args.userId]
		);
		return tEventBooking.toObject();
	},
	addMemberToEvent: async (root, args, details: ModuleContext<any>) => {
		return await addMemberToEvent(
			{ ...args, memberId: args.memberId, status: TEventBookingStatusEnum.ConciergeConfirmed },
			details
		);
	}
};
