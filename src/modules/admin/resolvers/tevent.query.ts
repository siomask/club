import { map, isString } from 'lodash';
import moment from 'moment';
import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers, TConciergeType } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, UtilsProvider, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

export const Query: any = {
	fetchEvents: async (root, args: any, { injector }: any, info: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const utilsProvider = injector.get(UtilsProvider);

		const query: any = {};
		if (args.input) {
			// tslint:disable-next-line:curly
			if (isString(args.input.range)) {
				const { from, to } = utilsProvider.parseDailyRange(args.input.range);
				query.startDate = { $gt: from };
				query.endDate = { $lt: to };
			}

			// ** NOTE:: if range is defined the rest will be ignored

			// tslint:disable-next-line:curly
			if (!query.startDate && args.input.from) {
				query.startDate = { $gt: args.input.from };
			}

			// tslint:disable-next-line:curly
			if (!!query.endDate && args.input.to) {
				query.endDate = { $lt: args.input.to };
			}
		}

		Object.assign(query,info.own)
		return {
			data: (
				await TEventModel.findActive(query)
					.populate(POPULATE.EVENTS(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TEventModel.countActive(query)
			}
		};
	},
	fetchEventById: async (root, args, { injector }: ModuleContext<IAdminModuleContext>) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);

		const tEvent = await TEventModel.findById(args.eventId).populate(POPULATE.EVENTS(injector));
		return tEvent.toObject();
	},
	fetchEventBookings: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, info: any) => {
		const TEventBookingModel: any = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);

		return {
			data: (
				await TEventBookingModel.findActive({ eventId: args.eventId })
					.populate(POPULATE.EVENTS(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TEventBookingModel.countActive({ eventId: args.eventId })
			}
		};
	},
};
