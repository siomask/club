import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TEvent, TMemberEventInfo, TEventStatusEnum, TEventBookingStatusEnum } from '@tclub/types';

import {
  NamespaceEntityProvider,
  EntityEnum,
  TwofaServiceProvider,
  AuthyResponseType,
  NOTIFICATINOS,
  POPULATE,
  addMemberToEvent,
  FirebaseProvider
} from '@tclub/core';
import { IAdminModuleContext } from '../admin.module';
import { ApolloError } from 'apollo-server-core';

export const Mutation: any = {
  addEventSpeaker: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { createdBy }: any) => {
    const TEventItemModel = injector
      .get(NamespaceEntityProvider)
      .getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_SPEAKER);

    const tEventAgenda = await TEventItemModel.create({
      ...args.input,
      createdBy
    });

    return (
      await TEventItemModel.findById(tEventAgenda._id).populate(POPULATE.SPEAKER(injector))
    ).toObject();
  },
  updateEventSpeaker: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { updatedBy }: any) => {
    const TEventItemModel = injector
      .get(NamespaceEntityProvider)
      .getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_SPEAKER);

    const tEventAg = await TEventItemModel.findByIdAndUpdate(args.input._id, {
      ...args.input,
      updatedBy
    });
    if (!tEventAg) {
      throw new ApolloError('No Item exist');
    }
    return (await TEventItemModel.findById(tEventAg._id).populate(POPULATE.SPEAKER(injector))).toObject();
  },
  removeEventSpeaker: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { updatedBy }: any) => {
    const TEventItemModel = injector
      .get(NamespaceEntityProvider)
      .getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_SPEAKER);

    const tEventAg = await TEventItemModel.findByIdAndRemove(args._id);
    if (!tEventAg) {
      throw new ApolloError('No Item exist');
    }
    return { success: true };
  }
};
