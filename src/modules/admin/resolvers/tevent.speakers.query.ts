import { map, isString } from 'lodash';
import moment from 'moment';
import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers, TConciergeType } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, UtilsProvider, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

export const Query: any = {
	fetchAllEventSpeakers: async (root, args: any, { injector }: any, info: any) => {
		const TEventSpModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_SPEAKER);

		const query: any = {};
		Object.assign(query, info.own);
		return {
			data: (
				await TEventSpModel.findActive(query)
					.populate(POPULATE.EVENTS(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TEventSpModel.countActive(query)
			}
		};
	}
};
