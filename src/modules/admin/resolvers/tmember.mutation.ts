import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TMember } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, TwofaServiceProvider, AuthyResponseType, POPULATE } from '@tclub/core';
import { IAdminModuleContext } from '../admin.module';
import { ApolloError } from 'apollo-server-core';

import shortid from 'shortid';

const generateShortId = (): string => {
	return shortid.generate();
};

export const Mutation: any = {
	addMember: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { createdBy }: any) => {
		const TMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);

		if (
			!args.input.cardInfo ||
			!args.input.cardInfo.code ||
			(await TMemberModel.count({ 'cardInfo.code': args.input.cardInfo.code })) > 0
		) {
			throw new ApolloError(`You already have member with such code`);
		}
		const twofaService = injector.get(TwofaServiceProvider);

		const twofaData = {
			backupKeys: twofaService.generateBackupKeys(),
			backupTimeStamp: Date.now(),
			sendMessagesTimes: []
		};

		const tMember = await TMemberModel.create({
			...args.input,
			createdBy,
      // isVerified:Date.now(),
			twofa: twofaData
		});

		return (await TMemberModel.findById(tMember._id).populate(POPULATE.MEMBER(injector))).toObject();
	},

	updateMember: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const TMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);
		if (
			args.input.cardInfo &&
			(!args.input.cardInfo.code ||
				(await TMemberModel.count({
					'cardInfo.code': args.input.cardInfo.code,
					_id: { $nin: [args.memberId] }
				})) > 0)
		) {
			throw new ApolloError(`You already have member with such code`);
		}
    const tMember = await TMemberModel.findByIdAndUpdate(args.memberId, { $set: { ...args.input, updatedBy } });
		return (await TMemberModel.findById(tMember._id).populate(POPULATE.MEMBER(injector))).toObject();
	},

	blockMember: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const TMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);
		const el = await TMemberModel.findById(args.memberId);
		await TMemberModel.update(
			{ _id: el._id },
			el.isBlocked
				? {
						$set: { updatedBy },
						$unset: { isBlocked: 1 }
				  }
				: { $set: { isBlocked: Date.now(), updatedBy } }
		);
		return { success: true };
	},

	removeMember: async (root, args, { injector }: ModuleContext<any>, { updatedBy }: any) => {
		const TMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);

		const tMemberData = TMemberModel.findById(args.memberId);
		if (!tMemberData) throw new ApolloError(`Cant find member with memberId = '${args.memberId}'`);

		if (tMemberData.isVerified && tMemberData.twofa.enabled) {
			const authyId = tMemberData.twofa.authyid;
			const twofaService = injector.get(TwofaServiceProvider);
			const twillioRemoveResponse = (await twofaService.deleteUser(authyId)) as AuthyResponseType;
			if (!twillioRemoveResponse.success)
				throw new ApolloError(`Can't remove from member with memberId = '${args.memberId}' from Twillo.`);
		}
		await TMemberModel.softDelete(args.memberId, { updatedBy });
		return { success: true };
	}
};
