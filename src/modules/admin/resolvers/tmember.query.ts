import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers, TPrivilegeStatus } from '@tclub/types';
import mongoose from 'mongoose';

import { NamespaceEntityProvider, EntityEnum, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

import { tMemberInfoProjection } from './constants';

export const Query: any = {
	memberById: async (root, { id }, { injector }: ModuleContext<IAdminModuleContext>, { createdBy }: any) => {
		const TMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);

		const tMember = await TMemberModel.findOne({ _id: id, deletedAt: { $exists: false } }).populate(
			POPULATE.MEMBER(injector)
		);
		return tMember.toObject();
	},
	memberList: async (root, args, { injector }: ModuleContext<any>, { createdBy, ...info }: any) => {
		const TMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);
		return {
			data: (
				await TMemberModel.findActive({})
					.populate(POPULATE.MEMBER(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TMemberModel.countActive(info.own)
			}
		};
	},
	memberEventsFavourites: async (root, args: any, { injector }: ModuleContext<any>, { createdBy, ...info }: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventFavouritegModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_FAVOURITES);

		const eventId = await TEventModel.distinctActive();
		const activeFav = {
			memberId: args.input.memberId,
			eventId,
			isFavourite: true
		};
		return {
			data: (
				await TEventFavouritegModel.findActive(activeFav)
					.populate(POPULATE.EVENT_FAVOURITE(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TEventFavouritegModel.countActive(activeFav)
			}
		};
	},
	memberServicesBookingList: async (
		root,
		{ input: { memberId } }: any,
		{ injector }: ModuleContext<any>,
		info: any
	) => {
		const TSerModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		const TSerBModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_BOOKING);
		const activeEv = await TSerModel.distinctActive();
		return {
			data: (
				await TSerBModel.findActive({ memberId, serviceId: activeEv })
					.populate(POPULATE.SERVICE_BOOKINGS(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TSerBModel.countActive({ memberId, serviceId: activeEv })
			}
		};
	},
	memberPrivilegeList: async (root, { input: { memberId } }: any, { injector }: ModuleContext<any>, info: any) => {
		const TPrivMemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);
		const TPrivModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);
		const privilegeId = await TPrivModel.distinctActive('_id', { status: TPrivilegeStatus.Active });
		const q = {
			memberId,
			privilegeId
		};
		return {
			data: (
				await TPrivMemModel.findActive(q)
					.populate(POPULATE.PRIVILEGE_MEMBER(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TPrivMemModel.countActive(q)
			}
		};
	},
	memberPrivilegeActivationsList: async (
		root,
		{ input: { memberId, privilegeId } }: any,
		{ injector }: ModuleContext<any>,
		info: any
	) => {
		const PrivilegeBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_BOOKING);
		return {
			data: (
				await PrivilegeBookingModel.findActive({ memberId, privilegeId })
					.populate(POPULATE.BOOKING_PRIVILEGE(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await PrivilegeBookingModel.countActive({ memberId, privilegeId })
			}
		};
	}
};
