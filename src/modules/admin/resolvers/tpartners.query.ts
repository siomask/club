import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

import { tMemberInfoProjection } from './constants';

export const Query: any = {
  partnersList: async (root, args, { injector }: ModuleContext<any>, { createdBy, ...info }: any) => {
		const TItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PARTNER);
		return {
			data: (
				await TItemModel.findActive(info.own)
					.populate(POPULATE.PARTNER(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TItemModel.countActive(info.own)
			}
		};
	}
};
