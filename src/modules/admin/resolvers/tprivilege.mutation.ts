import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TMember } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, FirebaseProvider, POPULATE, NOTIFICATINOS } from '@tclub/core';
import { IAdminModuleContext } from '../admin.module';
import { ApolloError } from 'apollo-server-core';

import shortid from 'shortid';

export const Mutation: any = {
	addPrivilege: async (root, args, { injector }: any, { createdBy }: any) => {
		const ITemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);

		let item = await ITemModel.create({
			...args.input,
			createdBy
		});
		return (await ITemModel.findById(item._id).populate(POPULATE.PRIVILEGE(injector))).toObject();
	},
	updatePrivilege: async (root, args, { injector }: any, { updatedBy }: any) => {
		const ITemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);

		await ITemModel.update({ _id: args.input._id }, { $set: { ...args.input, updatedBy } });
		return (await ITemModel.findById(args.input._id).populate(POPULATE.PRIVILEGE(injector))).toObject();
	},
	removePrivilege: async (root, { _id }: any, { injector }: any, { updatedBy }: any) => {
		const ITemModel: any = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);
		const deleted: any = await ITemModel.softDelete(_id, { updatedBy });
		if (!deleted) throw new ApolloError(`Cant find item with eventId = '${_id}'`);
		return { success: true };
	},
	applyMemberToPrivilege: async (root, args, { injector }: ModuleContext<IAdminModuleContext>) => {
		const firebaseProvider = injector.get(FirebaseProvider);
		const ITemModel: any = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);

		let item = await ITemModel.findOneActive(args.input);
		if (item) throw new ApolloError(`Already assigned`);
		if (!item) item = await ITemModel.create(args.input);
		item = (await ITemModel.findById(item._id).populate(POPULATE.BOOK_PRIVILEGE(injector))).toObject();
		firebaseProvider.notifyUsers(NOTIFICATINOS.ADD_PRIVILEGE_MEMBER({ object: item.privilegeId }), [
			args.input.memberId
		]);
		return item;
	},
	removeMemberFromPrivilege: async (root, args: any, { injector }: ModuleContext<IAdminModuleContext>) => {
		const firebaseProvider = injector.get(FirebaseProvider);
		const ITemModel: any = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);

		let item = await ITemModel.softDelete(args._id).populate(POPULATE.BOOK_PRIVILEGE(injector));
		firebaseProvider.notifyUsers(NOTIFICATINOS.REMOVE_PRIVILEGE_MEMBER({ object: item.privilegeId }), [
			args.input.memberId
		]);
		return { success: true };
	}
};
