import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, POPULATE } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

import { tMemberInfoProjection } from './constants';

export const Query: any = {
	privilegeList: async (root, args: any, { injector }: ModuleContext<any>, info: any) => {
		const TPrvModel: any = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);

		return {
			data: (
				await TPrvModel.findActive(info.own)
					.populate(POPULATE.PRIVILEGE(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TPrvModel.countActive(info.own)
			}
		};
	},
	privilegeMembersList: async (
		root,
		{ input: { privilegeId } }: any,
		{ injector }: ModuleContext<any>,
		info: any
	) => {
		const TPrivMemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);
		const TMemModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);
		const memberId = await TMemModel.distinctActive();
		const q = {
			memberId,
			privilegeId
		};
		return {
			data: (
				await TPrivMemModel.findActive(q)
					.populate(POPULATE.PRIVILEGE_MEMBER(injector))
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await TPrivMemModel.countActive(q)
			}
		};
	}
};
