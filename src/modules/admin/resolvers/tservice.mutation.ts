import { ModuleContext } from '@graphql-modules/core';
import { MutationResolvers, TMember } from '@tclub/types';

import {
	NamespaceEntityProvider,
	EntityEnum,
	TwofaServiceProvider,
	AuthyResponseType,
	getServiceBooking,
	getService
} from '@tclub/core';
import { IAdminModuleContext } from '../admin.module';
import { ApolloError } from 'apollo-server-core';

import shortid from 'shortid';

export const Mutation: any = {
	addService: async (root, args, { injector }: ModuleContext<IAdminModuleContext>, { createdBy }) => {
		const ITemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);

		const item = await ITemModel.create({
			...args.input,
			createdBy
		});

		return await getService(ITemModel, injector, item);
	},

	updateService: async (root, { _id, input }, { injector }: ModuleContext<any>, { updatedBy }) => {
		const ITemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		const el = await ITemModel.findById(_id);
		el.set({
			...input,
			updatedAt: Date.now()
		});
		await el.save();
		return await getService(ITemModel, injector, el);
	},
	removeService: async (root, { _id }, { injector }: ModuleContext<any>, { updatedBy }) => {
		const ITemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		const itemData = ITemModel.findById(_id);
		if (!itemData) throw new ApolloError(`Cant find item with memberId = '${_id}'`);

		await ITemModel.update({ _id }, { $set: { deletedAt: Date.now(), updatedBy } });
		return { success: true };
	},
	updateBookingService: async (root, { _id, input }, { injector }: ModuleContext<any>, { updatedBy }) => {
		const ITemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_BOOKING);
		const itemData = ITemModel.findById(_id);
		if (!itemData) throw new ApolloError(`Cant find item with id = '${_id}'`);

		await ITemModel.update(
			{ _id },
			{
				$set: {
					...input,
					updatedBy
				}
			}
		);
		return getServiceBooking(ITemModel, injector, { _id });
	}
};
