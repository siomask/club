import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, getServiceBooking, getService } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

import { tServiceInfoProjection } from './constants';

export const Query: any = {
	serviceById: async (root, args, { injector }: ModuleContext<IAdminModuleContext>) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		return await getService(ItemModel, injector, args);
	},

	serviceList: async (root, args, { injector }: ModuleContext<any>, info: any) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		return await getService(ItemModel, injector, null, info, info.own);
	},
	bookingServiceList: async (root, { serviceId }, { injector }: ModuleContext<any>) => {
		const ItemModelBooking = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_BOOKING);

		const find: any = {};
		if (serviceId) {
			find.serviceId = serviceId;
		}
		return await getServiceBooking(ItemModelBooking, injector, null, find);
	},
	serviceAssetsList: async (root, { serviceId }, { injector }: ModuleContext<any>) => {
		const ItemModelBooking = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);

		const find: any = {};
		if (serviceId) {
			find.serviceId = serviceId;
		}
		return await getServiceBooking(ItemModelBooking, injector, null, find);
	}
};
