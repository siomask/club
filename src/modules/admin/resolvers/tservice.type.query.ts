import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, getServiceBooking, getService } from '@tclub/core';

import { IAdminModuleContext } from '../admin.module';

import { tServiceInfoProjection } from './constants';

export const Query: any = {
	serviceTypeList: async (root, args, { injector }: ModuleContext<any>, info: any) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_TYPE);
		return {
			data: (
				await ItemModel.findActive({})
					.limit(info.limit)
					.skip(info.skip)
					.sort(info.sort)
			).map((el) => el.toObject()),
			pageInfo: {
				total: await ItemModel.countActive({})
			}
		};
	}
};
