export enum ROLES {
	USER = 'USER',
	ADMIN = 'ADMIN'
}

export const ROLE_VALUES = {
	USER: 1,
	ADMIN: 4
};

// ** move this maybe to mail provieder - need to discuss how it should work
export const msgForResetPassword = {
	text: 'resetPassword',
	subject: 'resetPassword'
};

export const msgForVerifyEmail = {
	text: 'VerifyEmail',
	subject: 'VerifyEmail'
};
