import { hash } from 'bcryptjs';

export const getHashedPassword = (password: string) => {
	return hash(password, 10);
};
