// ** AVAR::DONE
import { GraphQLModule } from '@graphql-modules/core';
import { createAdminModule } from '@tclub/admin';
import { createMobileModule } from '@tclub/mobile';

export const createAppModule = (moduleName: String): GraphQLModule => {
	let gModule: GraphQLModule = null;
 
  switch (moduleName.toLowerCase()) {
		case 'mobile':
			gModule = new GraphQLModule({
				imports: [createMobileModule()]
			});
			break;
		case 'admin':
			gModule = new GraphQLModule({
				imports: [createAdminModule()]
			});
			break;
		default: {
			gModule = new GraphQLModule({
				imports: [createMobileModule(), createAdminModule()]
			});
		}
	}

	return gModule;
};
