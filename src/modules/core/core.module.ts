import mongoose from 'mongoose';
import { GraphQLModule } from '@graphql-modules/core';

import {
	SchemaProvider,
	NamespaceEntityProvider,
	MongooseSDKProviderToken,
	DefaultLoggerProvider,
	DefaultEnvProvider,
	TokenProvider,
  MailProvider,
	FirebaseProvider,
	UtilsProvider
} from './providers';
import { createUploadModule } from '../upload';

let gm: GraphQLModule = null;

export const createCoreModule = (): GraphQLModule<{}, {}, {}> => {
	if (gm) return gm;

	console.log('createCoreModule');
	gm = new GraphQLModule({
		imports: [createUploadModule()],
		providers: [
			{
				provide: MongooseSDKProviderToken,
				useValue: mongoose
			},
      MailProvider,
			TokenProvider,
			DefaultEnvProvider,
			DefaultLoggerProvider,
			SchemaProvider,
			FirebaseProvider,
			NamespaceEntityProvider,
			UtilsProvider
		]
	});

	return gm;
};
