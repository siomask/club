import { createError } from 'apollo-errors';

export const TokenExpiredError = createError('ResetTokenExpiredError', {
	message: 'Token is expired.'
});

export const IncorrectTokenError = createError('IncorrectTokenError', {
	message: 'Something wrong with token.'
});
