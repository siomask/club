import { EventEmitter } from 'events';
import { OnInit } from '@graphql-modules/core';

export interface OnSyncInit extends OnInit {
	onInit: () => void;
	initializer: () => void;
}

export abstract class SyncInitProvider extends EventEmitter implements OnSyncInit {
	private p: Boolean;
	constructor(name?: String) {
		super();
		console.log(`TCLUB::Provider '${name}' constructor`);
	}

	public onInit() {
		if (this.p) return;
		this.p = true;
		this.initializer();
	}

	public abstract initializer(): void;
}

export interface OnAsyncInit extends OnInit {
	onInit: () => Promise<any>;
	initializer: () => Promise<any>;
}

export abstract class ASyncInitProvider extends EventEmitter implements OnAsyncInit {
	private p: Promise<any>;
	constructor(name?: String) {
		super();
		console.log(`TCLUB::Provider '${name}' constructor`);
	}

	public async onInit() {
		if (this.p) return this.p;

		this.p = new Promise(async (res, rej) => {
			await this.initializer();
			res();
		});

		return this.p;
	}

	public abstract initializer(): Promise<any>;
}
