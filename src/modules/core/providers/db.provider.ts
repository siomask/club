import { IEntityModel } from './namespace.entity.provider';

export const DBProviderToken = Symbol('DBProviderToken');

export interface IDBProvider {
	getEntityModel: (namespace: string, entityName: string) => IEntityModel;
}
