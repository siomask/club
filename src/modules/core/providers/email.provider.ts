import { Injectable, ProviderScope } from '@graphql-modules/di';
import sgMail from '@sendgrid/mail';

import { NamespaceEntityProvider, EntityEnum } from './namespace.entity.provider';
import { ASyncInitProvider } from './base.provider';
import { DefaultEnvProvider } from './env.default.provider';
import { TokenProvider } from './token.provider';
import fs from 'fs';
import path from 'path';
import moment from 'moment-timezone';
// ** constants
const SENDGRID_TEMPLATE_ID = '';

const MailSettings = {
	subject: '',
	from: 'admin@tclub.com',
	text: ''
};

const { subject, from, text } = MailSettings;

const time = (time) =>
	moment(time)
		.tz('Asia/Omsk')
		.format('DD-MM-YYYY HH:mm (UTC z)');

interface IMailData {
	[index: string]: string;

	email: string;
	name: string;
	info?: string;
	subject: string;
	text: string;
}

interface ITempate {
	path: string;
	name: string;
	text: string;
}

@Injectable({
	scope: ProviderScope.Application
})
export class MailProvider extends ASyncInitProvider {
	private emailHostURL: string;
	private templates: Array<any>;

	constructor(
		private envProvider: DefaultEnvProvider,
		private tokenProvider: TokenProvider,
		private nameSpaceProvider: NamespaceEntityProvider
	) {
		super('MailProvider');
		this.templates = [
			{
				name: 'BOOK_EVENT',
				path: 'notification.html'
			}
		];
		for (let i = 0; i < this.templates.length; i++) {
			const template: ITempate = this.templates[i];
			template.text = fs
				.readFileSync(path.resolve(__dirname, '../../../../resources/templates', template.path))
				.toString();
		}
	}
	private async emailActiveConcierges() {
		const TConciergeModel: any = this.nameSpaceProvider.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE);
		return await TConciergeModel.distinctActive('email', {
			isVerified: { $exists: true },
			$or: [{ isBlocked: { $exists: false } }, { isBlocked: null }]
		});
	}

	public async initializer() {
		this.envProvider.onInit();
		this.tokenProvider.onInit();
		this.emailHostURL = this.envProvider.TCLUB_EMAIL_HOST;
	}

	// ** we send this when concierge preregistered by admin
	public async sendPreRegisterEmail(user) {
		const { _id, email, firstname } = user;
		const token = this.tokenProvider.generatAuthenticationToken(_id, '24h');

		const html = `<a href='${this.emailHostURL}/verification?token=${token}'>Clik here</a>`;
		this.sendMail({ email, html, name: firstname });
	}

	public async sendPasswordResetEmail(user) {
		const { _id, email, firstname } = user;
		const token = this.tokenProvider.generatAuthenticationToken(_id, '24h');
		const html = `<a href='${this.emailHostURL}/password-reset?token=${token}'>Clik here</a>`;
		this.sendMail({ email, html, name: firstname });
	}

	public async sendBookServiceEmail(service) {
		const content = `Service Booking`;
		const help_data = `<table style="width:100%">
		<tbody>
      <tr>
        <td><b>Тип услуги</b></td>
        <td><b>Название услуги</b></td>
        <td><b>Дата и время бронирование</b></td>
        <td><b>Дата и время бронирование от и до</b></td>
        <td><b>ID клиента</b></td>
      </tr>
       <tr>
        <td>${service.serviceId.typeId.title}</td>
        <td>${service.serviceId.title}</td>
        <td>${time(service.createdAt)}</td>
        <td>${time(service.startDate)}:${time(service.endDate)}</td>
        <td>${service.memberId._id}</td>
      </tr>
		</tbody>
		</table>`;
		const html = this.templates[0].text.replace('@content@', content).replace('@help_data@', help_data);
		this.sendMail({ email: service.serviceId.createdBy.email, html, subject: 'Service Booking' });
	}

	public async sendBookEventEmail(event) {
		const content = `Event Booking`;
		const help_data = `<table style="width:100%">
		<tbody>
      <tr>
        <td><b>Название мероприятия</b></td>
        <td><b>Период мероприятия</b></td>
        <td><b>ID клиента</b></td>
        <td><b>Дата и время бронирования</b></td>
      </tr>
       <tr>
        <td>${event.eventId.title}</td>
        <td>${time(event.eventId.startDate)} : ${time(event.eventId.endDate)}</td>
        <td>${event.memberId._id}</td>
        <td>${time(event.createdAt)}</td>
      </tr>
		</tbody>
		</table>`;
		const html = this.templates[0].text.replace('@content@', content).replace('@help_data@', help_data);
		this.sendMail({ email: event.eventId.createdBy.email, html, subject: 'Event Booking' });
	}

	public async sendCancelBookEventEmail(event) {
		const content = `Event Booking Canceled`;
		const help_data = `<table style="width:100%">
		<tbody>
      <tr>
        <td><b>Название мероприятия</b></td>
        <td><b>Период мероприятия</b></td>
        <td><b>ID клиента</b></td>
        <td><b>Дата и время бронирования</b></td>
        <td><b>Дата и время отмены бронирования</b></td>
      </tr>
       <tr>
        <td>${event.eventId.title}</td>
        <td>${time(event.eventId.startDate)} : ${time(event.eventId.endDate)}</td>
        <td>${event.memberId._id}</td>
        <td>${time(event.createdAt)}</td>
        <td>${time(event.updatedAt)}</td>
      </tr>
		</tbody>
		</table>`;
		const html = this.templates[0].text.replace('@content@', content).replace('@help_data@', help_data);
		this.sendMail({ email: event.eventId.createdBy.email, html, subject: content });
	}

	public async sendBookPrivilegeEmail(item) {
		const content = `Privilege Booking`;
		const help_data = `<table style="width:100%">
		<tbody >
      <tr>
        <td><b>ID клиента</b></td>
        <td><b>Привилегия</b></td>
        <td><b>израсходовано</b></td>
        <td><b> осталось</b></td>
      </tr>
       <tr>
         <td>${item.memberId._id}</td>
         <td>${item.privilegeId.title}</td>
         <td>${item.privilegeId.used}</td>
         <td>${item.privilegeId.remain}</td>
      </tr>
		</tbody>
		</table>`;
		const html = this.templates[0].text.replace('@content@', content).replace('@help_data@', help_data);
		this.sendMail({ email: item.privilegeId.createdBy.email, html, subject: 'Privilege Booking' });
	}

	public async sendPasswordChangedEmail(user) {
		const { _id, email, firstname } = user;
		const html = `Has been succsessfuly reset`;
		this.sendMail({ email, html, name: firstname });
	}

	public async sendMail(data: any) {
		const { email, name, html, subject } = data;

		const { TCLUB_SENDGRID_KEY } = this.envProvider;
		sgMail.setApiKey(TCLUB_SENDGRID_KEY);
		const msg = (await this.emailActiveConcierges()).map((email: string) => ({
			to: email,
			from,
			subject,
			text: html,
			html
			// template_id: SENDGRID_TEMPLATE_ID,
			// dynamic_template_data: {
			// 	name
			// }
		}));
		console.log(
			'Mails to notify',
			msg.map((el) => el.email)
		);
		try {
			return sgMail.send(msg);
		} catch (err) {
			console.log(err);
			throw new Error(`error: ${err}`);
		}
	}
}
