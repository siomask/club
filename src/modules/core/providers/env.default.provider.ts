import { Mongoose } from 'mongoose';
import globMongoose from 'mongoose';
import { Injectable, ProviderScope, Inject } from '@graphql-modules/di';

import { ILogger, DefaultLoggerProvider } from './loger.default.provider';
import { MongooseSDKProviderToken } from './provider.tokens';
import { SyncInitProvider } from './base.provider';

export interface IEnvConfig {
	loger: ILogger;
	debug: boolean;
	// ** mongo

	MASTER_MONGO_URL: string;
	MASTER_MONGO_DATABASE: string;
	MASTER_MONGO_ARGS: string;

	PORT: number;
	WS_PORT: number;
	// portal & workspace  specific

	TCLUB_SENDGRID_KEY: string;
	JWT_SECRET: string;

	// twofa config
	TWOFA_CODE_EXPIRE_TIME: number;
	TWILIO_SID: string;
	TWILIO_TOKEN: string;
	TWILIO_PHONE_NUMBER: string;
	AUTHY_API_KEY: string;

	// ** in case we want to inject our own version of mongoose
	mongoose?: Mongoose;

	onInit: () => void;
}

@Injectable({
	scope: ProviderScope.Application
})
export class DefaultEnvProvider extends SyncInitProvider implements IEnvConfig {
	public NODE_ENV: string;
	public TCLUB_ENV: string;

	public debug: boolean = false;
	public MASTER_MONGO_URL: string;
	public MASTER_MONGO_DATABASE: string;
	public MASTER_MONGO_ARGS: string;

	public PORT: number;
	public WS_PORT: number;

	public TCLUB_EMAIL_HOST: string;
	public TCLUB_SENDGRID_KEY: string;
	public JWT_SECRET: string;
	public FIREBASE_CREDENTIALS: string;

	// ** some more twofa options
	public TWOFA_CODE_EXPIRE_TIME: number;
	public TWOFA_APP_SECRET: string;

	public TWILIO_SID: string;
	public TWILIO_TOKEN: string;
	public TWILIO_PHONE_NUMBER: string;
	public TWILIO_APP_KEY: string;
	public AUTHY_API_KEY: string;
	public APP_NAME: string;
	public AUTHY_APP_ID: string;

	public port: number;
	constructor(
		@Inject(DefaultLoggerProvider) public loger: DefaultLoggerProvider,
		@Inject(MongooseSDKProviderToken) public mongoose?: Mongoose
	) {
		super('DefaultEnvProvider');
	}

	public initializer() {
		console.log(`DefaultEnvProvider onInit - START`);
		this.loger.onInit();

		globMongoose.set('useNewUrlParser', true);
		globMongoose.set('useFindAndModify', false);
		// mongoose.set('useCreateIndex', true);
		// mongoose.set('useUnifiedTopology', true);

		this.mongoose = globMongoose;

		this.debug = !!process.env.debug;

		this.NODE_ENV = process.env.NODE_ENV || 'dev';
		this.TCLUB_ENV = process.env.TCLUB_ENV;

		this.MASTER_MONGO_URL = process.env.MASTER_MONGO_URL || 'mongodb://localhost:27017';
		this.MASTER_MONGO_DATABASE = process.env.MASTER_MONGO_DATABASE || 'tclub';
		this.MASTER_MONGO_ARGS = process.env.MASTER_MONGO_ARGS || '';

		this.PORT = +process.env.PORT || 4000;

		this.WS_PORT = +process.env.PORT || 4001;

		this.JWT_SECRET = process.env.JWT_SECRET;
		this.FIREBASE_CREDENTIALS = process.env.FIREBASE_CREDENTIALS;

		this.TCLUB_EMAIL_HOST = process.env.TCLUB_EMAIL_HOST;

		this.TCLUB_SENDGRID_KEY = process.env.TCLUB_SENDGRID_KEY;

		// ** Twillio and Authy
		this.TWOFA_CODE_EXPIRE_TIME = parseInt(process.env.TWOFA_CODE_EXPIRE_TIME, 10);

		this.TWILIO_SID = process.env.TWILIO_SID;
		this.TWILIO_TOKEN = process.env.TWILIO_TOKEN;
		this.TWILIO_PHONE_NUMBER = process.env.TWILIO_PHONE_NUMBER;
		this.TWILIO_APP_KEY = process.env.TWILIO_APP_KEY;
		this.AUTHY_API_KEY = process.env.AUTHY_API_KEY;
		this.APP_NAME = process.env.APP_NAME;
		this.AUTHY_APP_ID = process.env.AUTHY_APP_ID;

		console.log(`DefaultEnvProvider onInit - END`);
	}

	public isProduction(): boolean {
		return this.NODE_ENV === 'prod';
	}
}
