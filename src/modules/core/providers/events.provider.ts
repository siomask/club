import { Injectable, ProviderScope } from '@graphql-modules/di';
import { map, range } from 'lodash';

import { ASyncInitProvider } from './base.provider';
import { DefaultEnvProvider } from './env.default.provider';
import { ApolloError } from 'apollo-server-core';
import { NamespaceEntityProvider } from './namespace.entity.provider';

@Injectable({
	scope: ProviderScope.Application
})
export class EventsProvider extends ASyncInitProvider {
	constructor(private envProvider: DefaultEnvProvider, private nsp: NamespaceEntityProvider) {
		super('EventsProvider');
	}

	public async initializer() {
		this.envProvider.onInit();
		await this.nsp.onInit();
	}

	public assertEvent(eventId: string) {}

	public assertPublicEvent(eventId: string) {}

	public assertCurrentPublicEvent(eventId: string) {}

	public requestEventBookingByMember(eventId: string, memberId: string) {}

	public cancelEventBookingByMember(eventId: string, memberId: string) {}

	public markEventByMember(eventId: string, favourite: boolean) {}

	// ** accept event booking by concierge
	public acceptEventBookingByConcierge(conciergeId: string, eventId: string, memberId: string) {}

	// ** cancel event booking by concierge
	public cancelEventBookingByConcierge(conciergeId: string, eventId: string, memberId: string) {}
}
