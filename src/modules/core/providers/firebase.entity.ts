const admin = require('firebase-admin');
import fs from 'fs';

interface PushNotification {
	title: string;
	description: string;
	body: any;
}

export class FirebaseEntity {
	private db: any;
	private messaging: any;

	constructor(FIREBASE_CREDENTIALS) {
		const cred: string = fs.readFileSync(FIREBASE_CREDENTIALS, { encoding: 'utf8' }).toString();
		admin.initializeApp({
			credential: admin.credential.cert(JSON.parse(cred))
		});
		this.db = admin.firestore();
		this.messaging = admin.messaging();
	}

	private async getPushTokens(users: any = null) {
		return new Promise((resolve, reject) => {
			try {
				const tokens = [];

				this.db
					.collection('users')
					.get()
					.then((snapshot) => {
						const tokens = [];
						const usersId = [];
						const avusersId = [];
						snapshot.forEach((doc) => {
							const userId: string = doc.id.toString();
							avusersId.push(userId);
							if (users && users.indexOf(userId) < 0) {
								return;
							}
							usersId.push(userId);
							const d = doc.data();
							if (d.pushMode === false) return;
							if (Array.isArray(d.tokens)) tokens.push(...d.tokens);
						});
						console.log('++++++++++++++Will be notified ', usersId, tokens.length);
						console.log('++++++++++++++Users shoul be notified ', users);
						console.log('++++++++++++++Available user tokens ', avusersId);
						console.log('++++++++++++++Available user tokens ', tokens);
						resolve(tokens);
					})
					.catch((err) => {
						console.log('Error getting documents', err);
						reject(err);
					});
			} catch (e) {
				console.log(e);
				reject(e);
			}
		});
	}

	async updateUser(update: any, userId: string) {
		let user = this.db.collection('users').doc(userId);
		await user.update(update);
	}

	async notifyUsers(data: any, users: Array<string> | any = null) {
		return new Promise(async (resolve, reject) => {
			try {
				const tokens: any = await this.getPushTokens(
					users ? users.map((el: any) => (el._id ? el._id.toString() : el.toString())) : null
				);
				const message: any = {
					notification: {
						title: data.title,
						body: data.body.description,
						clickAction: 'FLUTTER_NOTIFICATION_CLICK',
						sound: "default"
					},
					data: { ...data, body: JSON.stringify(data.body) }
				};
				console.log(message);
				if (tokens.length) {
					this.messaging
						.sendToDevice(tokens, message)
						.then(function(response) {
							console.log('Successfully sent notifications to :', message );
              console.log(JSON.stringify(response));
            })
						.catch(function(error) {
							console.log('Error sending message:', error);
						});
				}
				resolve();
			} catch (e) {
				reject(e);
			}
		});
	}
}
