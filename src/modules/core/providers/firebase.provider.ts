import { Injectable, ProviderScope } from '@graphql-modules/di';
import { SyncInitProvider } from './base.provider';

import { ASyncInitProvider } from './base.provider';
import { DefaultEnvProvider } from './env.default.provider';
import { TokenProvider } from './token.provider';
import { FirebaseEntity } from './firebase.entity';

/**
 * This is the defalt logger that could be changed
 */

@Injectable({
	scope: ProviderScope.Application
})
export class FirebaseProvider extends ASyncInitProvider {
	private db: FirebaseEntity;

	constructor(private envProvider: DefaultEnvProvider, private tokenProvider: TokenProvider) {
		super('FirebaseProvider');
	}

	public async initializer() {
		this.envProvider.onInit();
		this.tokenProvider.onInit();
		this.db = new FirebaseEntity(this.envProvider.FIREBASE_CREDENTIALS);
	}

	async notifyUsers(notification: any, users: Array<string> | any = null) {
		return await this.db.notifyUsers(notification, users);
	}
	async updateUser(update: any, userId: string) {
		return await this.db.updateUser(update, userId);
	}
}
