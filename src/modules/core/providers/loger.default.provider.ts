import { Injectable, ProviderScope } from '@graphql-modules/di';
import { SyncInitProvider } from './base.provider';

export interface ILogger {
	log: (message: string | any) => void;
	info: (message: string | any) => void;
	error: (error: string | Error) => void;
}

/**
 * This is the defalt logger that could be changed
 */

@Injectable({
	scope: ProviderScope.Application
})
export class DefaultLoggerProvider extends SyncInitProvider implements ILogger {
	constructor() {
		super('DefaultLoggerProvider');
	}

	public initializer() {
		// **
	}

	public log(message: string | any): void {
		console.log(message);
	}

	public info(message: string | any): void {
		console.info(message);
	}

	public error(message: string | Error): void {
		console.error(message);
	}
}
