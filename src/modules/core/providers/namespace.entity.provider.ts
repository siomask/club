import { reduce } from 'lodash';
import { ConnectionOptions, Model } from 'mongoose';
import bCrypt from 'bcrypt-nodejs';
import { Injectable, ProviderScope, Inject } from '@graphql-modules/di';

import { IEntityDefinition } from '@sfast/gql-schema-parser';
import { createMongooseSchema } from '@sfast/gql-schema-mongoose';

import shortid from 'shortid';
import {
	MongoManager,
	ConnectArgsType,
	IDocument,
	RegisterSchemaArgsType,
	MongoManagerCtrOpts
} from '@sfast/mongomanager';

import { DefaultEnvProvider } from './env.default.provider';
import { ASyncInitProvider } from './base.provider';
import moment from 'moment';

// ** initialy we'll provide mongoose model like client interface
export type IEntityModel = Model<IDocument>;

export type INamespace = string[];

export interface INamespaceEntityProvider {
	registerEntities: (namespace: string, entities: IEntityDefinition[]) => Promise<void>;
	registerNamespace: (namespaceConfig: NamespaceConfig) => Promise<void>;
	getEntityModel: (namespace: string, entityName: string) => IEntityModel;
}

const UPLOAD_DIR_PUBLIC = 'uploads';
const UPLOAD_DIR = `./resources/${UPLOAD_DIR_PUBLIC}`;
const REMOTE_UPLOAD_HOST = process.env.REMOTE_UPLOAD_HOST;

export enum EntityEnum {
	DB_NAME = 'tclub',
	T_MEMBER = 'TMember',
	T_CONCIERGE = 'TConcierge',
	T_PARTNER = 'TPartners',
	T_EVENT = 'TEvent',
	T_EVENT_AGENDA = 'TEventAgendaItem',
	T_EVENT_SPEAKER = 'TEventSpeaker',
	T_EVENT_TYPE = 'TEventType',
	T_EVENT_FAVOURITES = 'TEventFavourites',
	T_MEMBER_EVENT_INFO = 'TMemberEventInfo',
	T_ASSETS = 'TAsset',
	T_SERVICE = 'TService',
	T_SERVICE_TYPE = 'TServiceType',
	T_ASSET_PERIOD = 'TAssetPeriod',
	T_CONTACT = 'TContacts',
	T_PRVILEGE = 'TPrivilege',
	T_PRVILEGE_BOOKING = 'TPrivilegeBooking',
	T_PRVILEGE_MEMBER = 'TPrivilegeMembers',
	T_SERVICE_BOOKING = 'TServiceBooking'
}

export interface StorageConfig<T extends 'mongo' | 'mysql'> {
	type?: T;
	url: string;
	config?: T extends 'mongo' ? ConnectionOptions : Record<string, any>;
	label?: string | string[];
}

export interface NamespaceConfig extends StorageConfig<any> {
	namespace: string;
}

// ** internal event
const NAMESPACE_READY = 'NAMESPACE_READY';

@Injectable({
	scope: ProviderScope.Application
})
export class NamespaceEntityProvider extends ASyncInitProvider implements INamespaceEntityProvider {
	private namespaceMap: Record<string, NamespaceConfig> = {};
	private mongoManager: MongoManager;

	constructor(@Inject(DefaultEnvProvider) private envProvider: DefaultEnvProvider) {
		super('NamespaceEntityProvider');
	}

	public async initializer() {
		return new Promise((resolve, reject) => {
			console.log('NamespaceEntityProvider onInit - START');
			this.envProvider.onInit();

			const mongoManagerOptions: MongoManagerCtrOpts = {
				debug: this.envProvider.debug,
				mongoose: this.envProvider.mongoose
			};

			this.mongoManager = new MongoManager(mongoManagerOptions);
			console.log('NamespaceEntityProvider onInit - END');

			this.once(NAMESPACE_READY, resolve);
		});
	}

	/**
	 * This should be called from the DB providers and by calling this we are allowing nsp to finish onInit
	 */
	public ready() {
		this.emit(NAMESPACE_READY);
	}

	/**
	 * By initialize-ing storage it'll try to create models for every entity registered for that namespace
	 * @param storageConfig
	 */
	public async registerNamespace(namespaceConfig: NamespaceConfig): Promise<void> {
		console.log('NamespaceEntityProvider registerNamespace', namespaceConfig);
		const { type, namespace, url, label, config } = namespaceConfig;
		switch (type) {
			case 'mongo':
				const storageConnectionOptions: ConnectArgsType = {
					id: namespace,
					url,
					label,
					config,
					autoindex: true
				};
				await this.mongoManager.connect(storageConnectionOptions);
				this.namespaceMap[namespace] = namespaceConfig;

				break;
			default:
				throw new Error(`Unknown storage ${namespaceConfig.type} is provided.`);
		}
	}

	/**
	 *
	 * @param entities
	 */
	public registerEntities(label: string, entities: IEntityDefinition[], dbType: string = 'mongo'): Promise<void> {
		console.log(`NamespaceEntityProvider registerEntities under label = ${label}`);
		return reduce(
			entities,
			async (prevPromise: Promise<void>, entityDef: any) => {
				await prevPromise;
				const { name } = entityDef;

				switch (dbType) {
					case 'mongo':
						const schema = createMongooseSchema(entityDef);
						schema.set('timestamps', true);

						schema.statics.findOneActive = function(query = {}) {
							return this.findOne({ ...query, deletedAt: { $exists: false } });
						};
						schema.statics.findActive = function(query = {}) {
							return this.find({ ...query, deletedAt: { $exists: false } });
						};
						schema.statics.distinctActive = function(key = '_id', query = {}) {
							return this.distinct(key, { ...query, deletedAt: { $exists: false } });
						};
						schema.statics.softDelete = function(_id, setts) {
							return this.findByIdAndUpdate({ _id }, { $set: { deletedAt: Date.now(), ...setts } });
						};
						schema.statics.softDeleteOne = function(query, setts) {
							return this.findOneAndUpdate(query, { $set: { deletedAt: Date.now(), ...setts } });
						};
						schema.statics.countActive = function(query = {}) {
							return this.count({ ...query, deletedAt: { $exists: false } });
						};
						// schema.methods.countActive = function countActive (cb) {
						//   return this.model('Animal').find({ type: this.type }, cb);
						// };
						if (name === EntityEnum.T_CONCIERGE) {
							schema.statics.generateHash = (password) => {
								if (!password) throw 'Password is empty';
								return bCrypt.hashSync('admin', bCrypt.genSaltSync(10), null);
							};
							schema.statics.isValidPassword = function(userpass, password) {
								return bCrypt.compareSync(password, userpass);
							};
							schema.set('toJSON', {
								transform(doc, record) {
									record.password = null;
									delete record.password;
									return record;
								}
							});
							schema.set('toObject', {
								transform(doc, record) {
									record.password = null;
									delete record.password;
									return record;
								}
							});
						} else if (name === EntityEnum.T_PRVILEGE) {
							schema.statics.activationsByRule = function(
								model,
								TPrivilegePeriodRuleRepeat,
								activations,
								startActivation = new Date()
							) {
								const today = moment(startActivation).valueOf();
								return activations.filter((activation) => {
									const actStart = new Date(activation.start);
									const actEnd = new Date(activation.end);
									if (typeof model.rule.start !== 'undefined') {
										//limited rules are in priorety
										return (
											actStart.getTime() >= new Date(model.rule.start).getTime() &&
											actEnd.getTime() <= new Date(model.rule.end).getTime()
										);
									} else {
										switch (model.rule.repeat) {
											case TPrivilegePeriodRuleRepeat.Day: {
												return (
													moment(actStart).format('YYYY/MM/DD') ===
													moment(startActivation).format('YYYY/MM/DD')
												);
											}
											case TPrivilegePeriodRuleRepeat.Week: {
												return (
													today >=
														moment(activation.start)
															.startOf('week')
															.valueOf() &&
													today <=
														moment(activation.start)
															.endOf('week')
															.valueOf()
												);
											}
											case TPrivilegePeriodRuleRepeat.Month: {
												return (
													moment(startActivation).format('YYYY/MM') ===
													moment(actStart).format('YYYY/MM')
												);
											}
											case TPrivilegePeriodRuleRepeat.Year: {
												return (
													moment(startActivation).format('YYYY') ===
													moment(actStart).format('YYYY')
												);
											}
										}
									}
								});
							};
							schema.statics.activeRule = function(
								model,
								TPrivilegePeriodRuleRepeat,
								startT = Date.now(),
								endT = Date.now()
							) {
								if (startT < new Date(model.start).getTime() || endT > new Date(model.end).getTime()) {
									return null;
								}
								return model.rules.filter((el: any) => {
									if (typeof el.start !== 'undefined') {
										//limited rules are in priorety
										return (
											startT >= new Date(el.start).getTime() && endT <= new Date(el.end).getTime()
										);
									} else {
										switch (el.repeat) {
											case TPrivilegePeriodRuleRepeat.Day: {
												return (
													moment(new Date(startT)).format('YYYY/MM/DD') ===
													moment().format('YYYY/MM/DD')
												);
											}
											case TPrivilegePeriodRuleRepeat.Month: {
												return (
													moment(new Date(startT)).format('YYYY/MM') ===
													moment().format('YYYY/MM')
												);
											}
											case TPrivilegePeriodRuleRepeat.Year: {
												return (
													moment(new Date(startT)).format('YYYY') === moment().format('YYYY')
												);
											}
										}
									}
								})[0];
							};
						} else if (name === EntityEnum.T_ASSETS) {
							schema.set('toJSON', {
								transform(doc, record) {
									if (!record.publicPath)
										record.publicPath = record.filename
											? `${REMOTE_UPLOAD_HOST}/${UPLOAD_DIR_PUBLIC}/${record.filename}`
											: null;
									return record;
								}
							});
							schema.set('toObject', {
								transform(doc, record) {
									if (!record.publicPath)
										record.publicPath = record.filename
											? `${REMOTE_UPLOAD_HOST}/${UPLOAD_DIR_PUBLIC}/${record.filename}`
											: null;
									return record;
								}
							});
						} else if (name === EntityEnum.T_MEMBER) {
							schema.set('toJSON', {
								transform(doc, record) {
									if (process.env.TCLUB_ENV === 'mobile' && record.cardInfo.card) {
										record.cardInfo.card = record.cardInfo.card.substr(-4);
									}
									return record;
								}
							});
							schema.set('toObject', {
								transform(doc, record) {
                  if (process.env.TCLUB_ENV === 'mobile' && record.cardInfo.code) {
										record.cardInfo.code = record.cardInfo.code.substr(-4);
									}
                  return record;
								}
							});
						}

						const entitySchema: RegisterSchemaArgsType = {
							name,
							schema,
							// pk?: string;
							// parent?: string;
							options: {
								collection: entityDef.collection
							},
							label
						};
						// schema.pre('validate', function() {
						// 	console.log('this gets printed first');
						// });
						// schema.post('validate', function() {
						// 	console.log('this gets printed second');
						// });
						// schema.pre('create', function() {
						// 	const self: any = this;
						// 	self.createdAt = Date.now();
						// 	self.updatedAt = Date.now();
						// 	self.uuid = name + '-' + shortid.generate();
						//
						// 	console.log('before create');
						// });
						// schema.pre('save', function() {
						// 	const self: any = this;
						// 	if (!self.createdAt) self.createdAt = Date.now();
						// 	if (!self.uuid) self.uuid = name + '-' + shortid.generate();
						// 	self.updatedAt = Date.now();
						// 	console.log('before save',self,schema);
						// });
						// schema.pre('findOneAndUpdate', function() {
						// 	const self: any = this;
						// 	self.updatedAt = Date.now();
						// });
						// schema.pre('findByIdAndUpdate', function() {
						// 	const self: any = this;
						// 	self.updatedAt = Date.now();
						//   console.log('before save',self,schema);
						// });
						// schema.pre('updateOne', function() {
						// 	const self: any = this;
						// 	self.updatedAt = Date.now();
						// });
						// schema.pre('updateMany', function() {
						// 	const self: any = this;
						// 	self.updatedAt = Date.now();
						// });
						// schema.pre('update', function() {
						// 	const self: any = this;
						// 	self.updatedAt = Date.now();
						//   // console.log('before save',self,schema);
						// });
						// schema.set('timestamps', {
						//   createdAt: true,
						//   updatedAt: { path: 'updatedAt', setOnInsert: false }
						// });
						this.mongoManager.registerSchema(entitySchema);
						break;
					default:
						throw new Error('Unknown storage type');
				}
			},
			Promise.resolve()
		);
	}

	// ** this is the model of the db con
	public getEntityModel(namespace: string, entityName: string): IEntityModel {
		const { type } = this.getNamespace(namespace);
		switch (type) {
			case 'mongo':
				return this.mongoManager.getModel(namespace, entityName) as IEntityModel;
			default:
				throw new Error('Unknown type of storage');
		}
	}

	public getNamespace(namespace: string): NamespaceConfig {
		return this.namespaceMap[namespace];
	}
}
