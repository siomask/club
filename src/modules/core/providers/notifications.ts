const objectTitle = (object: any) => {
	if (object.title) return object.title;
	if (object.name) return object.name;
	if (object._id) return object._id.toString();
	return 'PLEASE REPORT IF YOU SEE THIS';
};
const simpleObject = (object: any) => {
	if (object.event) {
		return {
			event: {
				title: object.event.title,
				_id: object.event._id
			},
			booking: {
				_id: object.booking._id,
        bookingStatus: object.booking.bookingStatus
			}
		};
	}
	return {
		_id: object._id,
		title: object.title
	};
};
export const NOTIFICATINOS: any = {
	CLIENT_BOOK_EVENT: ({ object }: any) => ({
		//only booked client will receive push message
		title: `Бронирование мероприятия ${objectTitle(object)}`,
		body: {
			description: `Мероприятие ${objectTitle(object)} забронировано`,
			type: 'event',
			category: 'CLIENT_BOOK_EVENT',
			action: 'BOOKING',
			object: simpleObject(object)
		}
	}),
	CLIENT_CANCEL_BOOK_EVENT: ({ object }: any) => ({
		//only booked client will receive push message
		title: `Бронирование мероприятия ${objectTitle(object)}`,
		body: {
			description: `Бронирование мероприятия ${objectTitle(object)} отменено`,
			category: 'CLIENT_CANCEL_BOOK_EVENT',
			type: 'event',
			action: 'BOOKING',
			object: simpleObject(object)
		}
	}),
	ADMIN_REJECT_BOOKED_EVENT: ({ object }: any) => ({
		//only booked client will receive push message
		title: `Бронирование мероприятия ${objectTitle(object.event)}`,
		body: {
			description: `Бронирование мероприятия ${objectTitle(object.event)} отменено консьержем`,
			category: 'ADMIN_REJECT_BOOKED_EVENT',
			type: 'event',
			action: 'BOOKING',
			object: simpleObject(object)
		}
	}),
	ADMIN_CONFIRM_BOOKED_EVENT: ({ object }: any) => ({
		//only booked client will receive push message
		title: `Booking event ${objectTitle(object.event)}`,
		body: {
			description: `Бронирование мероприятия ${objectTitle(object.event)} подтверждено`,
			category: 'ADMIN_CONFIRM_BOOKED_EVENT',
			type: 'event',
			action: 'BOOKING',
			object: simpleObject(object)
		}
	}),
	NEW_EVENT: ({ object }: any) => ({
		//all clients will receive push messages
		title: `Новое мероприятие ${objectTitle(object)}`,
		body: {
			description: `Новое мероприятие ${objectTitle(object)}`,
			category: 'NEW_EVENT',
			type: 'event',
			action: 'CREATE',
			object: simpleObject(object)
		}
	}),
	UPDATE_EVENT: ({ object }: any) => ({
		//all clients will receive push messages
		title: `Обновление мероприятие ${objectTitle(object)}`,
		body: {
			description: `Мероприятие ${objectTitle(object)} обновлено`,
			category: 'UPDATE_EVENT',
			type: 'event',
			action: 'Update',
			object: simpleObject(object)
		}
	}),
	DELETE_EVENT: ({ object }: any) => ({
		//only booked event clients will receive push messages
		title: `Мероприятие ${objectTitle(object)} удалено`,
		body: {
			description: `Мероприятие ${objectTitle(object)} удалено`,
			category: 'UPDATE_EVENT',
			type: 'event',
			action: 'Update',
			object: simpleObject(object)
		}
	}),
	EVENT_WILL_START: ({ object }: any) => ({
		//all clients will receive push messages
		title: `Мероприятие ${objectTitle(object)} скоро начнется`,
		body: {
			description: `Мероприятие ${objectTitle(object)} запустится в течении дня`,
			category: 'EVENT_WILL_START',
			type: 'event',
			action: 'Update',
			object: simpleObject(object)
		}
	}),
	ADD_PRIVILEGE_MEMBER: ({ object }: any) => ({
		//only booked client will receive push message
		title: `Привилегия ${objectTitle(object)} доступна`,
		body: {
			description: `Новая Привилегия ${objectTitle(object)} доступна`,
			category: 'ADD_PRIVILEGE_MEMBER',
			type: 'privilege',
			action: 'Update',
			object: simpleObject(object)
		}
	}),
	REMOVE_PRIVILEGE_MEMBER: ({ object }: any) => ({
		//only booked client will receive push message
		title: `Привилегия ${objectTitle(object)} не доступна`,
		body: {
			description: `Привилегия ${objectTitle(object)} не доступна`,
			category: 'REMOVE_PRIVILEGE_MEMBER',
			type: 'privilege',
			action: 'Update',
			object: simpleObject(object)
		}
	}),
	ADMIN_NOTIFY_ALL_USERS: ({ object }: any) => ({
		//only booked client will receive push message
		title: object.title,
		body: {
			description:  object.text,
			category: 'notify_all_members',
			type: 'all',
			action: 'notify_all_members',
			object: simpleObject(object)
		}
	})
};
