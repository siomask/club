import path from 'path';
import { reduce } from 'lodash';
import { loadSchemaFiles } from 'graphql-toolkit';
import { DocumentNode } from 'graphql';
import gql from 'graphql-tag';

import { Injectable, ProviderScope } from '@graphql-modules/di';

import { DIRECTIVES } from '@sfast/gql-schema-mongoose';
import { GraphEntityParser, IEntityDefinition } from '@sfast/gql-schema-parser';

import { SyncInitProvider } from './base.provider';

@Injectable({
	scope: ProviderScope.Application
})
export class SchemaProvider extends SyncInitProvider {
	public typeDefs: String;
	private graphParser: GraphEntityParser;

	constructor() {
		super('SchemaProvider');
	}

	public initializer() {
		try {
			console.log('SchemaProvider onInit - START');

			// ** those are all the models we have in a system
			const coreSchemasPath = path.resolve(__dirname, '../schema/');

			this.typeDefs = [DIRECTIVES, ...loadSchemaFiles(coreSchemasPath)].join('');
			const dn: DocumentNode = gql(this.typeDefs);
			this.graphParser = new GraphEntityParser(dn);
			console.log('SchemaProvider onInit - END');
		} catch (err) {
			throw err;
		}
	}

	public get entities(): Record<string, IEntityDefinition> {
		return this.graphParser.entities;
	}

	public getNamespaceEntities(namespace: string): IEntityDefinition[] {
		return reduce(
			this.graphParser.entities,
			(acc: IEntityDefinition[], entityItem: IEntityDefinition) => {
				if (entityItem.namespace.indexOf(namespace) > -1) acc.push(entityItem);
				return acc;
			},
			[]
		);
	}
}
