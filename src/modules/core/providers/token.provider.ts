import fs from 'fs';
import { DecodeOptions, sign, verify, decode } from 'jsonwebtoken';
import { Injectable, ProviderScope } from '@graphql-modules/di';

import { DefaultEnvProvider } from './env.default.provider';
import { SyncInitProvider } from './base.provider';

import { TokenExpiredError, IncorrectTokenError } from '../errors';

export enum TokenTypeEnum {
	AUTHENTICATION = 'AUTHENTICATION',
	AUTHORIZATION = 'AUTHORIZATION'
}

export type TokenInfo = {
	memberId: string;
	type: TokenTypeEnum;
};

@Injectable({
	scope: ProviderScope.Application
})
export class TokenProvider extends SyncInitProvider {
	private privateKey: string;
	private publicKey: string;
	firebaseCredentials: Record<string, string>;

	constructor(private envProvider: DefaultEnvProvider) {
		super('TokenProvider');
	}

	public initializer() {
		this.envProvider.onInit();
		if (this.envProvider.FIREBASE_CREDENTIALS) {
			const fileContext = fs.readFileSync(this.envProvider.FIREBASE_CREDENTIALS, { encoding: 'utf8' }).toString();
			this.firebaseCredentials = JSON.parse(fileContext);
			this.privateKey = this.firebaseCredentials.private_key;
    }
	}

	// ** expires in 1 hour
	public generatAuthenticationToken(userId: string, expire: string = '1h'): string {
		return this.generateToken(userId, TokenTypeEnum.AUTHENTICATION, expire);
	}

	// ** expires in 10 years
	public generatAuthorizationToken(userId: string, expire: string = '10y'): string {
		return this.generateToken(userId, TokenTypeEnum.AUTHORIZATION, expire);
	}

	public generatFirebaseToken(userId: string): string {
		// ** Firebase custom token based on this https://firebase.google.com/docs/auth/admin/create-custom-tokens

		const now = Math.floor(Date.now() / 1000);

		const payload = {
			alg: 'RS256',
			iss: this.firebaseCredentials.client_email,
			sub: this.firebaseCredentials.client_email,
			aud: 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
			iat: now,
			uid: userId,
			exp: now + 3600
		};

		const firebaseToken = sign(payload, this.privateKey, { algorithm: 'RS256' });
		return firebaseToken;
	}

	public decodeToken(token: string, tokenType?: string): TokenInfo {
		const { JWT_SECRET } = this.envProvider;
		const verifyStatus = verify(token, JWT_SECRET);
		if (!verifyStatus) throw new TokenExpiredError();

		const payload = decode(token, JWT_SECRET as DecodeOptions);

		if (!payload) throw new IncorrectTokenError();

		console.log(payload);
		// tslint:disable-next-line:curly
		if (tokenType && tokenType !== payload.type) {
			throw new IncorrectTokenError();
		}

		return {
			memberId: payload.userId,
			type: payload.type
		};
	}

	private generateToken(userId: string, type: string = 'tclub', expiresIn: string = '48h'): string {
		return sign({ userId, type }, this.envProvider.JWT_SECRET, { expiresIn });
	}
}
