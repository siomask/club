import { Twilio } from 'twilio';
import Authy from 'authy';
import crypto from 'crypto';
import { Injectable, ProviderScope } from '@graphql-modules/di';
import { map, range } from 'lodash';

import { ASyncInitProvider } from './base.provider';
import { DefaultEnvProvider } from './env.default.provider';
import { ApolloError } from 'apollo-server-core';

export type RegisterUserResponseType = {
	message: string;
	user: any;
	success: boolean;
};

export type AuthyResponseType = {
	message: string;
	user: any;
	success: boolean;
	cellphone?: string;
	device?: any;
	ignored?: boolean;
};

@Injectable({
	scope: ProviderScope.Application
})
export class TwofaServiceProvider extends ASyncInitProvider {
	private appName: string;
	private twilio: Twilio;
	private authy: any;

	constructor(private envProvider: DefaultEnvProvider) {
		super('TwofaServiceProvider');
		const { TWILIO_SID, TWILIO_TOKEN, AUTHY_API_KEY, APP_NAME, AUTHY_APP_ID } = this.envProvider;
		this.twilio = new Twilio(TWILIO_SID, TWILIO_TOKEN);

		this.authy = Authy(AUTHY_API_KEY);
		this.appName = APP_NAME;
	}

	public async initializer() {
		this.envProvider.onInit();

		const { TWILIO_SID, TWILIO_TOKEN, AUTHY_API_KEY, APP_NAME } = this.envProvider;
		this.twilio = new Twilio(TWILIO_SID, TWILIO_TOKEN);

		this.appName = APP_NAME;
		this.authy = Authy(AUTHY_API_KEY);
	}

	public async verificationSms(phoneNumber: string, countryCode: string): Promise<string> {
		try {
			if (!phoneNumber || !countryCode) throw new ApolloError('not correct value');
			const { TWILIO_PHONE_NUMBER } = this.envProvider;
			const code = Math.random()
				.toString()
				.substring(2, 9);
			const twillioRequest = {
				from: TWILIO_PHONE_NUMBER,
				to: `+${countryCode.replace('+', '')}${phoneNumber.replace('+', '')}`,
				body: `Your ${this.appName || 'APPNAME'} verification code is: ${code}`
			};

			console.log(`AVAR::TWILLIO::verificationSms`, twillioRequest);
			await this.twilio.messages.create(twillioRequest);

			return code;
		} catch (error) {
			console.log('AVAR::ERROR::verificationSms', error);
			throw new Error(error.message);
		}
	}

	public async checkVerification(phoneNumber: string, countryCode: string, code: string): Promise<any> {
		try {
			console.log(`AVAR::checkVerification  from (${countryCode})${phoneNumber} -> code= ${code}`);
			return new Promise((resolve, reject) => {
				this.authy.phones().verification_check(phoneNumber, countryCode, code, (err, res) => {
					if (err) return reject(err);
					resolve(res);
				});
			});
		} catch (error) {
			console.log('AVAR::ERROR::checkVerification', error);
			throw new Error(error.message);
		}
	}

	public async registerUser(
		email: string,
		phoneNumber: string,
		countryCode: string
	): Promise<RegisterUserResponseType> {
		try {
			return new Promise((resolve, reject) => {
				this.authy.register_user(email, phoneNumber, countryCode, (err, res) => {
					if (err) return reject(err);
					resolve(res);
				});
			});
		} catch (error) {
			console.log('AVAR::ERROR::registerUser', error);
			throw new Error(error.message);
		}
	}

	public generateBackupKeys(count?: number) {
		return map(range(count || 10), () => {
			return {
				code: crypto.randomBytes(5).toString('hex'),
				isUsed: false
			};
		});
	}

	public async deleteUser(authyId: string): Promise<any> {
		try {
			return new Promise((resolve, reject) => {
				this.authy.delete_user(authyId, (err, res) => {
					if (err) return reject(err);
					resolve(res);
				});
			});
		} catch (error) {
			throw new Error(error.message);
		}
	}

	public async sendCode(authyId: string): Promise<any> {
		try {
			return new Promise((resolve, reject) => {
				this.authy.request_sms(authyId, true, (err, res) => {
					if (err) return reject(err);
					resolve(res);
				});
			});
		} catch (error) {
			throw new Error(error.message);
		}
	}

	public async verifyCode(id, token): Promise<any> {
		try {
			return new Promise((resolve, reject) => {
				this.authy.verify(id, token, (err, res) => {
					if (err) return reject(err);
					resolve(res);
				});
			});
		} catch (error) {
			throw new Error(error.message);
		}
	}
}
