import fs from 'fs';
import { map } from 'lodash';
import moment from 'moment';

import { Injectable, ProviderScope } from '@graphql-modules/di';
import { SyncInitProvider } from './base.provider';

@Injectable({
	scope: ProviderScope.Application
})
export class UtilsProvider extends SyncInitProvider {
	constructor() {
		super('UtilsProvider');
	}

	public initializer() {
		// **
	}

	public getSchemaStructure(coreSchemasPath): any {
		const res: any = [];
		const files = fs.readdirSync(coreSchemasPath);
		// tslint:disable-next-line:prefer-for-of
		for (let i = 0; i < files.length; i++) {
			if (files[i].match('.spec')) continue;
			res.push(fs.readFileSync(coreSchemasPath + '/' + files[i]));
		}
		return res;
	}

	public parseDailyRangeUnix(rangeArg: string) {
		const rangeTerms: string[] = map(rangeArg.split(':'), (elem) => elem.trim());
		if (rangeTerms.length !== 2) {
			throw new Error('rangeArg should conform moment js');
		}

		const from = moment()
			.subtract(rangeTerms[0], 'd')
			.unix();
		const to = moment()
			.add(rangeTerms[1], 'd')
			.unix();

		return { from, to };
	}

	public parseDailyRangeDate(rangeArg: string) {
		const rangeTerms: string[] = map(rangeArg.split(':'), (elem) => elem.trim());
		if (rangeTerms.length !== 2) {
			throw new Error('rangeArg should conform moment js');
		}

		const from = moment()
			.subtract(rangeTerms[0], 'd')
			.unix();
		const to = moment()
			.add(rangeTerms[1], 'd')
			.unix();

		return { from: new Date(from * 1000), to: new Date(to * 1000) };
	}
}
