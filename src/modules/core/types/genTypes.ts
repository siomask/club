import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
export type Maybe<T> = T | null;
export type RequireFields<T, K extends keyof T> = { [X in Exclude<keyof T, K>]?: T[X] } & { [P in K]-?: NonNullable<T[P]> };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string,
  String: string,
  Boolean: boolean,
  Int: number,
  Float: number,
  Date: any,
  /** upload file type */
  Upload: any,
};























export type AddConciergeInput = {
  firstname: Scalars['String'],
  lastname: Scalars['String'],
  email: Scalars['String'],
  phone: Scalars['String'],
  countryCode: Scalars['String'],
  isBlocked?: Maybe<Scalars['Date']>,
  avatar?: Maybe<Scalars['ID']>,
  password: Scalars['String'],
};

export type AddEventAgendaInput = {
  /** required title of event agenda */
  title: Scalars['String'],
  /** description of event agenda */
  description?: Maybe<Scalars['String']>,
  /** required start datetime of event agenda */
  startDate: Scalars['Date'],
  /** required end datetime of event agenda */
  endDate: Scalars['Date'],
  /** required event which belong agenda */
  eventId: Scalars['ID'],
  /** speakers of the agenda */
  speakers?: Maybe<Array<Maybe<Scalars['ID']>>>,
};

export type AddEventInput = {
  /** required event title */
  title?: Maybe<Scalars['String']>,
  /** event start Date to be available */
  startDate?: Maybe<Scalars['Date']>,
  /** event end Date to be available */
  endDate?: Maybe<Scalars['Date']>,
  /** event location with 1 min char and 256 max xhars */
  location?: Maybe<Scalars['String']>,
  /** description of event */
  description1?: Maybe<Scalars['String']>,
  /** more description of event */
  description2?: Maybe<Scalars['String']>,
  /** file assets for event */
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** total available places to book */
  capacity?: Maybe<Scalars['Int']>,
  /** total booked places */
  bookedCapacity?: Maybe<Scalars['Int']>,
  /** event type */
  typeId?: Maybe<Scalars['ID']>,
  /** event image preview */
  preview?: Maybe<Scalars['ID']>,
  /** event since when member can register */
  canRegisterBefore?: Maybe<Scalars['Date']>,
  /** event since when member can cancel booking */
  canCancelBefore?: Maybe<Scalars['Date']>,
  /** event speakers */
  speakers?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** event status ability */
  status?: Maybe<TEventStatusEnum>,
};

export type AddEventSpeakerInput = {
  /** name of user-speaker */
  name?: Maybe<Scalars['String']>,
  /** description of user-speaker like who is he/she */
  description?: Maybe<Scalars['String']>,
  /** avatar image preview of user-speaker */
  preview?: Maybe<Scalars['ID']>,
};

export type AddMemberInput = {
  /** member first name */
  firstname: Scalars['String'],
  /** member last name */
  lastname: Scalars['String'],
  /** member phone country code */
  countryCode: Scalars['String'],
  /** member phone */
  phoneNumber: Scalars['String'],
  /** member email */
  email?: Maybe<Scalars['String']>,
  /** member is blocked */
  isBlocked?: Maybe<Scalars['Date']>,
  /** profile url */
  preview?: Maybe<Scalars['ID']>,
  cardInfo?: Maybe<CardInfoInput>,
  appSettings?: Maybe<AppSettingsInput>,
};

/** service input params */
export type AddServiceInput = {
  title?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  subtitle1?: Maybe<Scalars['String']>,
  subtitle2?: Maybe<Scalars['String']>,
  typeId: Scalars['ID'],
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
  preview?: Maybe<Scalars['ID']>,
  price?: Maybe<Scalars['Float']>,
};

export type AgendaItemInput = {
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** required title of event agenda */
  title: Scalars['String'],
  /** description of event agenda */
  description?: Maybe<Scalars['String']>,
  /** required start datetime of event agenda */
  startDate: Scalars['Date'],
  /** required end datetime of event agenda */
  endDate: Scalars['Date'],
  /** required event which belong agenda */
  eventId: Scalars['ID'],
  /** speakers of the agenda */
  speakers?: Maybe<Array<Maybe<Scalars['ID']>>>,
};

export type ApplyMemberToPrivilege = {
  memberId: Scalars['ID'],
  privilegeId: Scalars['ID'],
};

/** member settings respose structure */
export type AppSettings = {
   __typename?: 'AppSettings',
  /** member available mode */
  publicMode?: Maybe<Scalars['Boolean']>,
  /** can member recieve push notifications */
  pushMode?: Maybe<Scalars['Boolean']>,
  secureLoginMode?: Maybe<Scalars['Boolean']>,
  /** member language */
  language?: Maybe<Scalars['String']>,
};

export type AppSettingsInput = {
  publicMode?: Maybe<Scalars['Boolean']>,
  pushMode?: Maybe<Scalars['Boolean']>,
  secureLoginMode?: Maybe<Scalars['Boolean']>,
  language?: Maybe<Scalars['String']>,
};

export type AssetPeiodInputFields = {
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
};

export type AuthInfo = {
   __typename?: 'AuthInfo',
  /** description of error */
  error?: Maybe<Scalars['String']>,
  /** status */
  success?: Maybe<Scalars['Boolean']>,
  /** token to access */
  token: Scalars['String'],
  /** user details */
  user?: Maybe<TConciergeInfoResponse>,
};

export type AuthToken = {
   __typename?: 'AuthToken',
  /** description of error */
  error?: Maybe<Scalars['String']>,
  /** status */
  success?: Maybe<Scalars['Boolean']>,
  token: Scalars['String'],
};

/** member configs BackupKeys respose structure */
export type BackupKeyType = {
   __typename?: 'BackupKeyType',
  code?: Maybe<Scalars['String']>,
  isUsed?: Maybe<Scalars['Boolean']>,
};

export type Base = {
   __typename?: 'Base',
  _id?: Maybe<Scalars['ID']>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
};

/** base structure for all admin */
export type BaseAdminResponse = {
   __typename?: 'BaseAdminResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** first name with min chars 1 and max chars is 128 */
  firstname?: Maybe<Scalars['String']>,
  /** last name with min chars 1 and max chars is 128 */
  lastname?: Maybe<Scalars['String']>,
  /** unique email with 5 min chars and 128 max chars */
  email?: Maybe<Scalars['String']>,
  /** phone unmber */
  phone?: Maybe<Scalars['String']>,
  /** phone countryCodeIndex */
  countryCode?: Maybe<Scalars['String']>,
  /** avatar preview image */
  avatar?: Maybe<TAsset>,
};

/** base structure for all types of user */
export type BaseAdminSchema = {
   __typename?: 'BaseAdminSchema',
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** first name with min chars 1 and max chars is 128 */
  firstname?: Maybe<Scalars['String']>,
  /** last name with min chars 1 and max chars is 128 */
  lastname?: Maybe<Scalars['String']>,
  /** unique email with 5 min chars and 128 max chars */
  email: Scalars['String'],
  /** phone unmber */
  phone?: Maybe<Scalars['String']>,
  /** countryCodeIndex */
  countryCode?: Maybe<Scalars['String']>,
  /** avatar preview image */
  avatar?: Maybe<Scalars['ID']>,
};

/** Base model structure for all types */
export type BaseModel = {
   __typename?: 'BaseModel',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
};

/** base structure for all entites */
export type BaseModelSchema = {
   __typename?: 'BaseModelSchema',
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
};

/** member card respose structure */
export type CardInfo = {
   __typename?: 'CardInfo',
  /** member card code */
  code?: Maybe<Scalars['String']>,
  /** member card expire date */
  expire?: Maybe<Scalars['Date']>,
  /** member card details */
  cardHolder?: Maybe<Scalars['String']>,
};

export type CardInfoInput = {
  code?: Maybe<Scalars['String']>,
  expire?: Maybe<Scalars['Date']>,
  cardHolder?: Maybe<Scalars['String']>,
};

export type ConciergeInput = {
  firstname: Scalars['String'],
  lastname: Scalars['String'],
  email: Scalars['String'],
  phone: Scalars['String'],
  countryCode: Scalars['String'],
  isBlocked?: Maybe<Scalars['Date']>,
  avatar?: Maybe<Scalars['ID']>,
};


export type DateRange = {
   __typename?: 'DateRange',
  startDate?: Maybe<Scalars['Date']>,
  endDate?: Maybe<Scalars['Date']>,
};

/** service booking input params */
export type EditBookinServiceInput = {
  status?: Maybe<TServiceBookingStatusEnum>,
  startDate?: Maybe<Scalars['Date']>,
  endDate?: Maybe<Scalars['Date']>,
};

export type FetchEventsFilterInput = {
  /** search string */
  search?: Maybe<Scalars['String']>,
  /** search by columns, if no provided or empty will search by all available keys */
  columns?: Maybe<Array<Maybe<Scalars['String']>>>,
  /** limit items to return, defauly is 100 */
  limit?: Maybe<Scalars['Int']>,
  /** skip on get limited items, default is 0 */
  skip?: Maybe<Scalars['Int']>,
  /** sort items */
  sort?: Maybe<InputBaseFilterSort>,
  userId?: Maybe<Scalars['ID']>,
  from?: Maybe<Scalars['Date']>,
  to?: Maybe<Scalars['Date']>,
  range?: Maybe<Scalars['String']>,
};

/** base filter on fetch data */
export type InputBaseFilter = {
  /** search string */
  search?: Maybe<Scalars['String']>,
  /** search by columns, if no provided or empty will search by all available keys */
  columns?: Maybe<Array<Maybe<Scalars['String']>>>,
  /** limit items to return, defauly is 100 */
  limit?: Maybe<Scalars['Int']>,
  /** skip on get limited items, default is 0 */
  skip?: Maybe<Scalars['Int']>,
  /** sort items */
  sort?: Maybe<InputBaseFilterSort>,
};

export type InputBaseFilterSort = {
  /** field name to sort, default is createdAt */
  field?: Maybe<Scalars['String']>,
  /** 1 - ASC, -1 - DESC, default is -1 */
  dir?: Maybe<Scalars['Int']>,
};

/** member langs structure */
export enum LanguageEnum {
  English = 'ENGLISH',
  Russian = 'RUSSIAN'
}

export type MediaAsset = {
   __typename?: 'MediaAsset',
  name?: Maybe<Scalars['String']>,
  type?: Maybe<Scalars['String']>,
};

export type MemberListInput = {
  /** search string */
  search?: Maybe<Scalars['String']>,
  /** search by columns, if no provided or empty will search by all available keys */
  columns?: Maybe<Array<Maybe<Scalars['String']>>>,
  /** limit items to return, defauly is 100 */
  limit?: Maybe<Scalars['Int']>,
  /** skip on get limited items, default is 0 */
  skip?: Maybe<Scalars['Int']>,
  /** sort items */
  sort?: Maybe<InputBaseFilterSort>,
  memberId: Scalars['String'],
};

export type MemberPrivilegeActivationsListInput = {
  /** search string */
  search?: Maybe<Scalars['String']>,
  /** search by columns, if no provided or empty will search by all available keys */
  columns?: Maybe<Array<Maybe<Scalars['String']>>>,
  /** limit items to return, defauly is 100 */
  limit?: Maybe<Scalars['Int']>,
  /** skip on get limited items, default is 0 */
  skip?: Maybe<Scalars['Int']>,
  /** sort items */
  sort?: Maybe<InputBaseFilterSort>,
  memberId: Scalars['String'],
  privilegeId: Scalars['String'],
};

export type Mutation = {
   __typename?: 'Mutation',
  login?: Maybe<AuthInfo>,
  resetPsw?: Maybe<OperationStatus>,
  notifyAllMembers?: Maybe<OperationStatus>,
  addAssetPeriod?: Maybe<TAssetPeriodInfo>,
  removeAssetPeriod?: Maybe<OperationStatus>,
  editAssetPeriod?: Maybe<TAssetPeriodInfo>,
  addAsset?: Maybe<TAsset>,
  removeAsset?: Maybe<OperationStatus>,
  addConcierge?: Maybe<TConciergeInfoResponse>,
  updateConcierge?: Maybe<TConciergeInfoResponse>,
  removeConcierge?: Maybe<OperationStatus>,
  addEventAgenda?: Maybe<TEventAgendaItemResponse>,
  updateEventAgenda?: Maybe<TEventAgendaItemResponse>,
  removeEventAgenda?: Maybe<OperationStatus>,
  addEvent?: Maybe<TEventInfo>,
  removeEvent?: Maybe<OperationStatus>,
  updateEvent?: Maybe<TEventInfo>,
  confirmEventBookingForUser?: Maybe<TMemberBookingEventInfo>,
  rejectEventBookingForUser?: Maybe<TMemberBookingEventInfo>,
  addMemberToEvent?: Maybe<OperationStatus>,
  addEventSpeaker?: Maybe<TEventSpeakerInfo>,
  updateEventSpeaker?: Maybe<TEventSpeakerInfo>,
  removeEventSpeaker?: Maybe<OperationStatus>,
  addMember?: Maybe<TMemberInfo>,
  updateMember?: Maybe<TMemberInfo>,
  removeMember?: Maybe<OperationStatus>,
  blockMember?: Maybe<OperationStatus>,
  addPrivilege?: Maybe<TPrivilegeResponse>,
  updatePrivilege?: Maybe<TPrivilegeResponse>,
  removePrivilege?: Maybe<OperationStatus>,
  applyMemberToPrivilege?: Maybe<TPrivilegeMembersRespose>,
  removeMemberFromPrivilege?: Maybe<OperationStatus>,
  addService?: Maybe<TServiceInfo>,
  updateService?: Maybe<TServiceInfo>,
  removeService?: Maybe<OperationStatus>,
  updateBookingService?: Maybe<TServiceBookingInfo>,
};


export type MutationLoginArgs = {
  email: Scalars['String'],
  password: Scalars['String']
};


export type MutationResetPswArgs = {
  email: Scalars['String']
};


export type MutationNotifyAllMembersArgs = {
  title: Scalars['String'],
  text: Scalars['String'],
  eventId?: Maybe<Scalars['ID']>
};


export type MutationAddAssetPeriodArgs = {
  input?: Maybe<AssetPeiodInputFields>
};


export type MutationRemoveAssetPeriodArgs = {
  id: Scalars['String']
};


export type MutationEditAssetPeriodArgs = {
  id: Scalars['ID'],
  input?: Maybe<AssetPeiodInputFields>
};


export type MutationAddAssetArgs = {
  file: Scalars['Upload']
};


export type MutationRemoveAssetArgs = {
  id: Scalars['String']
};


export type MutationAddConciergeArgs = {
  input: AddConciergeInput
};


export type MutationUpdateConciergeArgs = {
  conciergeId: Scalars['String'],
  input: UpdateConciergeInput
};


export type MutationRemoveConciergeArgs = {
  conciergeId: Scalars['String']
};


export type MutationAddEventAgendaArgs = {
  input?: Maybe<AddEventAgendaInput>
};


export type MutationUpdateEventAgendaArgs = {
  input?: Maybe<UpdateEventAgendaInput>
};


export type MutationRemoveEventAgendaArgs = {
  _id: Scalars['ID']
};


export type MutationAddEventArgs = {
  input?: Maybe<AddEventInput>
};


export type MutationRemoveEventArgs = {
  _id?: Maybe<Scalars['String']>
};


export type MutationUpdateEventArgs = {
  eventId?: Maybe<Scalars['String']>,
  input?: Maybe<UpdateEventInput>
};


export type MutationConfirmEventBookingForUserArgs = {
  _id: Scalars['ID'],
  eventId: Scalars['ID'],
  userId: Scalars['ID']
};


export type MutationRejectEventBookingForUserArgs = {
  _id: Scalars['ID'],
  eventId: Scalars['ID'],
  userId: Scalars['ID']
};


export type MutationAddMemberToEventArgs = {
  eventId: Scalars['ID'],
  memberId: Scalars['ID']
};


export type MutationAddEventSpeakerArgs = {
  input?: Maybe<AddEventSpeakerInput>
};


export type MutationUpdateEventSpeakerArgs = {
  input?: Maybe<UpdateEventSpeakerInput>
};


export type MutationRemoveEventSpeakerArgs = {
  _id: Scalars['ID']
};


export type MutationAddMemberArgs = {
  input: AddMemberInput
};


export type MutationUpdateMemberArgs = {
  memberId: Scalars['String'],
  input: AddMemberInput
};


export type MutationRemoveMemberArgs = {
  memberId: Scalars['String']
};


export type MutationBlockMemberArgs = {
  memberId: Scalars['String']
};


export type MutationAddPrivilegeArgs = {
  input: TPrivilegeInput
};


export type MutationUpdatePrivilegeArgs = {
  input: TPrivilegeInput
};


export type MutationRemovePrivilegeArgs = {
  _id: Scalars['ID']
};


export type MutationApplyMemberToPrivilegeArgs = {
  input: ApplyMemberToPrivilege
};


export type MutationRemoveMemberFromPrivilegeArgs = {
  _id: Scalars['ID'],
  input: ApplyMemberToPrivilege
};


export type MutationAddServiceArgs = {
  input: AddServiceInput
};


export type MutationUpdateServiceArgs = {
  _id: Scalars['String'],
  input: AddServiceInput
};


export type MutationRemoveServiceArgs = {
  _id: Scalars['String']
};


export type MutationUpdateBookingServiceArgs = {
  _id: Scalars['String'],
  input: EditBookinServiceInput
};

/** service dates */
export type OcupationDateService = {
   __typename?: 'OcupationDateService',
  startDate?: Maybe<Scalars['Date']>,
  endDate?: Maybe<Scalars['Date']>,
};

/** response status */
export type OperationStatus = {
   __typename?: 'OperationStatus',
  /** description of error */
  error?: Maybe<Scalars['String']>,
  /** status */
  success?: Maybe<Scalars['Boolean']>,
};

export type Owner = {
   __typename?: 'Owner',
  _id?: Maybe<Scalars['ID']>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  owner?: Maybe<Scalars['ID']>,
};

/** page info */
export type Pageinfo = {
   __typename?: 'Pageinfo',
  /** total count of available items */
  total?: Maybe<Scalars['Int']>,
};

export type PrivilegeListInput = {
  /** search string */
  search?: Maybe<Scalars['String']>,
  /** search by columns, if no provided or empty will search by all available keys */
  columns?: Maybe<Array<Maybe<Scalars['String']>>>,
  /** limit items to return, defauly is 100 */
  limit?: Maybe<Scalars['Int']>,
  /** skip on get limited items, default is 0 */
  skip?: Maybe<Scalars['Int']>,
  /** sort items */
  sort?: Maybe<InputBaseFilterSort>,
  privilegeId: Scalars['ID'],
};

export type Query = {
   __typename?: 'Query',
  isAuth?: Maybe<AuthInfo>,
  assetById?: Maybe<TAsset>,
  assetsList?: Maybe<Array<Maybe<TAsset>>>,
  conciergeById?: Maybe<TConciergeInfoResponse>,
  conciergeList?: Maybe<TotalTConcierge>,
  fetchEventAgenda?: Maybe<TotalTEventAgendaItemResponse>,
  fetchEvents?: Maybe<TotalTEventInfo>,
  fetchEventById?: Maybe<TEventInfo>,
  fetchEventBookings?: Maybe<TotalTMemberEventInfoResponse>,
  fetchAllEventSpeakers?: Maybe<TotalTEventSpeakerInfo>,
  memberById?: Maybe<TMemberInfo>,
  memberList?: Maybe<TotalTMemberInfo>,
  memberPrivilegeList?: Maybe<TotalTPrivilegeMembersRespose>,
  memberPrivilegeActivationsList?: Maybe<TotalTPrivilegeBookingResponse>,
  memberServicesBookingList?: Maybe<TotalTServiceBookingInfo>,
  memberEventsFavourites?: Maybe<TotalTEventFavouritesResponse>,
  partnersList?: Maybe<TotalTPartnersResponse>,
  privilegeList?: Maybe<TotalTPrivilegeResponse>,
  privilegeMembersList?: Maybe<TotalTPrivilegesMembersRespose>,
  serviceById?: Maybe<TServiceInfo>,
  serviceList?: Maybe<Array<Maybe<TServiceInfo>>>,
  bookingServiceList?: Maybe<Array<Maybe<TServiceBookingInfo>>>,
  serviceAssetsList?: Maybe<TotalTServiceMenuAssetsResponse>,
  serviceTypeList?: Maybe<TotalTServiceTypeResponse>,
};


export type QueryAssetByIdArgs = {
  id: Scalars['String']
};


export type QueryConciergeByIdArgs = {
  id: Scalars['String']
};


export type QueryConciergeListArgs = {
  input?: Maybe<InputBaseFilter>
};


export type QueryFetchEventAgendaArgs = {
  eventId: Scalars['ID'],
  input?: Maybe<InputBaseFilter>
};


export type QueryFetchEventsArgs = {
  input?: Maybe<FetchEventsFilterInput>
};


export type QueryFetchEventByIdArgs = {
  eventId: Scalars['ID']
};


export type QueryFetchEventBookingsArgs = {
  eventId: Scalars['ID'],
  input?: Maybe<InputBaseFilter>
};


export type QueryFetchAllEventSpeakersArgs = {
  input?: Maybe<InputBaseFilter>
};


export type QueryMemberByIdArgs = {
  id: Scalars['String']
};


export type QueryMemberListArgs = {
  input?: Maybe<InputBaseFilter>
};


export type QueryMemberPrivilegeListArgs = {
  input?: Maybe<MemberListInput>
};


export type QueryMemberPrivilegeActivationsListArgs = {
  input?: Maybe<MemberPrivilegeActivationsListInput>
};


export type QueryMemberServicesBookingListArgs = {
  input?: Maybe<MemberListInput>
};


export type QueryMemberEventsFavouritesArgs = {
  input?: Maybe<MemberListInput>
};


export type QueryPartnersListArgs = {
  input?: Maybe<InputBaseFilter>
};


export type QueryPrivilegeListArgs = {
  input?: Maybe<InputBaseFilter>
};


export type QueryPrivilegeMembersListArgs = {
  input?: Maybe<PrivilegeListInput>
};


export type QueryServiceByIdArgs = {
  _id: Scalars['String']
};


export type QueryServiceListArgs = {
  input?: Maybe<InputBaseFilter>
};


export type QueryBookingServiceListArgs = {
  serviceId?: Maybe<Scalars['String']>
};


export type QueryServiceAssetsListArgs = {
  serviceId?: Maybe<Scalars['String']>,
  input?: Maybe<InputBaseFilter>
};


export type QueryServiceTypeListArgs = {
  input?: Maybe<InputBaseFilter>
};

export enum Roles {
  Member = 'MEMBER',
  Concierge = 'CONCIERGE',
  Admin = 'ADMIN'
}

/** asset collection to keep all uploads */
export type TAsset = {
   __typename?: 'TAsset',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** absolute path to access file */
  publicPath?: Maybe<Scalars['String']>,
  /** relative path to access file */
  path?: Maybe<Scalars['String']>,
  /** name of file */
  filename?: Maybe<Scalars['String']>,
  /** extension of file */
  mimetype?: Maybe<Scalars['String']>,
  /** file encode type */
  encoding?: Maybe<Scalars['String']>,
};

/** asset collection to keep all uploads in specific period */
export type TAssetPeriod = {
   __typename?: 'TAssetPeriod',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** when asset should start available */
  start?: Maybe<Scalars['Date']>,
  /** when asset should end available */
  end?: Maybe<Scalars['Date']>,
  /** list of assets */
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
};

/** asset period respose structure */
export type TAssetPeriodInfo = {
   __typename?: 'TAssetPeriodInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** when asset should start available */
  start?: Maybe<Scalars['Date']>,
  /** when asset should end available */
  end?: Maybe<Scalars['Date']>,
  /** list of assets */
  assets?: Maybe<Array<Maybe<TAsset>>>,
};

/** Concierge collection */
export type TConcierge = {
   __typename?: 'TConcierge',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** first name with min chars 1 and max chars is 128 */
  firstname?: Maybe<Scalars['String']>,
  /** last name with min chars 1 and max chars is 128 */
  lastname?: Maybe<Scalars['String']>,
  /** unique email with 5 min chars and 128 max chars */
  email: Scalars['String'],
  /** phone unmber */
  phone?: Maybe<Scalars['String']>,
  /** countryCodeIndex */
  countryCode?: Maybe<Scalars['String']>,
  /** avatar preview image */
  avatar?: Maybe<Scalars['ID']>,
  /** username with 128 max xhars */
  username?: Maybe<Scalars['String']>,
  /** password with required string */
  password: Scalars['String'],
  /** type of concirge */
  type: TConciergeType,
  /** when user was verified, it restrict user to access admin panel if user was not verified */
  isVerified?: Maybe<Scalars['Date']>,
  /** when user was blocked, it restrict user to access admin panel if user is blocked */
  isBlocked?: Maybe<Scalars['Date']>,
};

/** Concierge Response structure */
export type TConciergeInfoResponse = {
   __typename?: 'TConciergeInfoResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** first name with min chars 1 and max chars is 128 */
  firstname?: Maybe<Scalars['String']>,
  /** last name with min chars 1 and max chars is 128 */
  lastname?: Maybe<Scalars['String']>,
  /** unique email with 5 min chars and 128 max chars */
  email?: Maybe<Scalars['String']>,
  /** phone unmber */
  phone?: Maybe<Scalars['String']>,
  /** phone countryCodeIndex */
  countryCode?: Maybe<Scalars['String']>,
  /** avatar preview image */
  avatar?: Maybe<TAsset>,
  /** username with 128 max xhars */
  username?: Maybe<Scalars['String']>,
  /** when user was verified, it restrict user to access admin panel if user was not verified */
  isVerified?: Maybe<Scalars['Date']>,
  /** when user was blocked, it restrict user to access admin panel if user is blocked */
  isBlocked?: Maybe<Scalars['Date']>,
  /** type of concirge */
  type?: Maybe<TConciergeType>,
};

/** type of admin users */
export enum TConciergeType {
  /** concierge type */
  Concierge = 'CONCIERGE',
  /** admin */
  Admin = 'ADMIN'
}

/** event collection(мероприятия) */
export type TEvent = {
   __typename?: 'TEvent',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** required event title */
  title?: Maybe<Scalars['String']>,
  /** event start Date to be available */
  startDate?: Maybe<Scalars['Date']>,
  /** event end Date to be available */
  endDate?: Maybe<Scalars['Date']>,
  /** event location with 1 min char and 256 max xhars */
  location?: Maybe<Scalars['String']>,
  /** description of event */
  description1?: Maybe<Scalars['String']>,
  /** more description of event */
  description2?: Maybe<Scalars['String']>,
  /** file assets for event */
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** total available places to book */
  capacity?: Maybe<Scalars['Int']>,
  /** total booked places */
  bookedCapacity?: Maybe<Scalars['Int']>,
  /** event type */
  typeId?: Maybe<Scalars['ID']>,
  /** event image preview */
  preview?: Maybe<Scalars['ID']>,
  /** event since when member can register */
  canRegisterBefore?: Maybe<Scalars['Date']>,
  /** event since when member can cancel booking */
  canCancelBefore?: Maybe<Scalars['Date']>,
  /** event agenads */
  agenda?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** event speakers */
  speakers?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** event status ability */
  status?: Maybe<TEventStatusEnum>,
};

/** event agenda collection */
export type TEventAgendaItem = {
   __typename?: 'TEventAgendaItem',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** required title of event agenda */
  title: Scalars['String'],
  /** description of event agenda */
  description?: Maybe<Scalars['String']>,
  /** required start datetime of event agenda */
  startDate: Scalars['Date'],
  /** required end datetime of event agenda */
  endDate: Scalars['Date'],
  /** required event which belong agenda */
  eventId: Scalars['ID'],
  /** speakers of the agenda */
  speakers?: Maybe<Array<Maybe<Scalars['ID']>>>,
};

/** event agenda response structure */
export type TEventAgendaItemResponse = {
   __typename?: 'TEventAgendaItemResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** required title of event agenda */
  title?: Maybe<Scalars['String']>,
  /** description of event agenda */
  description?: Maybe<Scalars['String']>,
  /** required start datetime of event agenda */
  startDate?: Maybe<Scalars['Date']>,
  /** required end datetime of event agenda */
  endDate?: Maybe<Scalars['Date']>,
  /** required event which belong agenda */
  eventId?: Maybe<Scalars['ID']>,
  /** speakers of the agenda */
  speakers?: Maybe<Array<Maybe<TEventSpeakerInfo>>>,
};

/** event booking status */
export enum TEventBookingStatusEnum {
  /** event booking was created by member */
  MemberRequested = 'MEMBER_REQUESTED',
  /** event booking was canceled by member */
  MemberCanceled = 'MEMBER_CANCELED',
  /** event booking was confirmed by concierge */
  ConciergeConfirmed = 'CONCIERGE_CONFIRMED',
  /** event booking was rejected by concierge */
  ConciergeRejected = 'CONCIERGE_REJECTED'
}

/** event collection(мероприятия) */
export type TEventFavourites = {
   __typename?: 'TEventFavourites',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** required member who is keep favourite event */
  memberId: Scalars['ID'],
  /** required event to favourite */
  eventId: Scalars['ID'],
  /** is Favourite, default is false */
  isFavourite?: Maybe<Scalars['Boolean']>,
};

/** event collection(мероприятия) */
export type TEventFavouritesResponse = {
   __typename?: 'TEventFavouritesResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** required member who is keep favourite event */
  memberId?: Maybe<TMemberInfo>,
  /** required event to favourite */
  eventId?: Maybe<TEventInfo>,
  /** is Favourite, default is false */
  isFavourite?: Maybe<Scalars['Boolean']>,
};

/** event response structure */
export type TEventInfo = {
   __typename?: 'TEventInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** required event title */
  title?: Maybe<Scalars['String']>,
  /** event start Date to be available */
  startDate?: Maybe<Scalars['Date']>,
  /** event end Date to be available */
  endDate?: Maybe<Scalars['Date']>,
  /** event location with 1 min char and 256 max xhars */
  location?: Maybe<Scalars['String']>,
  /** description of event */
  description1?: Maybe<Scalars['String']>,
  /** more description of event */
  description2?: Maybe<Scalars['String']>,
  /** file assets for event */
  assets?: Maybe<Array<Maybe<TAsset>>>,
  /** capacity of the event, which is a hard limitation for allowing event bookings. */
  capacity?: Maybe<Scalars['Int']>,
  /** total booked places */
  bookedCapacity?: Maybe<Scalars['Int']>,
  /** event type */
  typeId?: Maybe<TEventTypeResponse>,
  /** event image preview */
  preview?: Maybe<TAsset>,
  /** event since when member can register */
  canRegisterBefore?: Maybe<Scalars['Date']>,
  /** event since when member can cancel booking */
  canCancelBefore?: Maybe<Scalars['Date']>,
  /** event agenads */
  agenda?: Maybe<Array<Maybe<TEventAgendaItemResponse>>>,
  /** event speakers */
  speakers?: Maybe<Array<Maybe<TEventSpeakerInfo>>>,
  /** event status ability */
  status?: Maybe<TEventStatusEnum>,
};

/** event speaker collection */
export type TEventSpeaker = {
   __typename?: 'TEventSpeaker',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** name of user-speaker */
  name?: Maybe<Scalars['String']>,
  /** description of user-speaker like who is he/she */
  description?: Maybe<Scalars['String']>,
  /** avatar image preview of user-speaker */
  preview?: Maybe<Scalars['ID']>,
};

/** event speaker collection */
export type TEventSpeakerInfo = {
   __typename?: 'TEventSpeakerInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** name of user-speaker */
  name?: Maybe<Scalars['String']>,
  /** describe about speaker */
  description?: Maybe<Scalars['String']>,
  /** avatar image preview of user-speaker */
  preview?: Maybe<TAsset>,
};

/** event status */
export enum TEventStatusEnum {
  /** no one can access it */
  Private = 'PRIVATE',
  /** anyone can see it */
  Open = 'OPEN',
  /** was closed and not available anymore */
  Closed = 'CLOSED'
}

/** 
 * event types, since we can have restaurant, or IT meeting or another, we shold keep the type of event in case we will
 * need to filter events
 */
export type TEventType = {
   __typename?: 'TEventType',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** title of event type */
  title?: Maybe<Scalars['String']>,
  /** description of type event */
  description?: Maybe<Scalars['String']>,
};

export type TEventTypeResponse = {
   __typename?: 'TEventTypeResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** description of type event */
  description?: Maybe<Scalars['String']>,
  /** title of event type */
  title?: Maybe<Scalars['String']>,
};

/** member collection */
export type TMember = {
   __typename?: 'TMember',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** member first name */
  firstname: Scalars['String'],
  /** member last name */
  lastname: Scalars['String'],
  /** member phone country code */
  countryCode: Scalars['String'],
  /** member phone */
  phoneNumber: Scalars['String'],
  /** member email */
  email?: Maybe<Scalars['String']>,
  /** member is verifed */
  isVerified?: Maybe<Scalars['Date']>,
  /** member is blocked */
  isBlocked?: Maybe<Scalars['Date']>,
  twofa?: Maybe<TwofaConfig>,
  cardInfo?: Maybe<CardInfo>,
  /** member setings */
  appSettings?: Maybe<AppSettings>,
  /** profile url */
  preview?: Maybe<Scalars['ID']>,
  test?: Maybe<Scalars['Boolean']>,
};

/** event booking member collection */
export type TMemberBookingEventInfo = {
   __typename?: 'TMemberBookingEventInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  memberId?: Maybe<TMemberInfo>,
  eventId?: Maybe<TEventInfo>,
  bookingStatus?: Maybe<TEventBookingStatusEnum>,
};

/** event booking member collection */
export type TMemberEventInfo = {
   __typename?: 'TMemberEventInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** which member was request the booking for event */
  memberId: Scalars['ID'],
  /** on which event member was request the booking */
  eventId: Scalars['ID'],
  /** booking status */
  bookingStatus?: Maybe<TEventBookingStatusEnum>,
};

/** event booking member response structure */
export type TMemberEventInfoResponse = {
   __typename?: 'TMemberEventInfoResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** event title */
  title?: Maybe<Scalars['String']>,
  startDate?: Maybe<Scalars['Date']>,
  endDate?: Maybe<Scalars['Date']>,
  location?: Maybe<Scalars['String']>,
  description1?: Maybe<Scalars['String']>,
  description2?: Maybe<Scalars['String']>,
  /** total available places */
  capacity?: Maybe<Scalars['Int']>,
  /** total booked places */
  bookedCapacity?: Maybe<Scalars['Int']>,
  typeId?: Maybe<TEventTypeResponse>,
  canRegisterBefore?: Maybe<Scalars['Date']>,
  canCancelBefore?: Maybe<Scalars['Date']>,
  agenda?: Maybe<Array<Maybe<TEventAgendaItemResponse>>>,
  status?: Maybe<TEventStatusEnum>,
  preview?: Maybe<TAsset>,
  speakers?: Maybe<Array<Maybe<TEventSpeakerInfo>>>,
  assets?: Maybe<Array<Maybe<TAsset>>>,
  isFavourite?: Maybe<Scalars['Boolean']>,
  /** current event is booked for current user */
  info?: Maybe<TMemberEventInfo>,
};

/** event booking member response structure */
export type TMemberEventResponse = {
   __typename?: 'TMemberEventResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  memberId?: Maybe<TMemberInfo>,
  eventId?: Maybe<TEventInfo>,
  bookingStatus?: Maybe<TEventBookingStatusEnum>,
};

/** member respose structure */
export type TMemberInfo = {
   __typename?: 'TMemberInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  firstname?: Maybe<Scalars['String']>,
  lastname?: Maybe<Scalars['String']>,
  email?: Maybe<Scalars['String']>,
  countryCode?: Maybe<Scalars['String']>,
  phoneNumber?: Maybe<Scalars['String']>,
  preview?: Maybe<TAsset>,
  twofa?: Maybe<TwofaConfig>,
  cardInfo?: Maybe<CardInfo>,
  appSettings?: Maybe<AppSettings>,
  isVerified?: Maybe<Scalars['Date']>,
  isBlocked?: Maybe<Scalars['Date']>,
  test?: Maybe<Scalars['Boolean']>,
};

export type TotalTConcierge = {
   __typename?: 'TotalTConcierge',
  data?: Maybe<Array<Maybe<TConciergeInfoResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTEventAgendaItemResponse = {
   __typename?: 'TotalTEventAgendaItemResponse',
  data?: Maybe<Array<Maybe<TEventAgendaItemResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTEventFavouritesResponse = {
   __typename?: 'TotalTEventFavouritesResponse',
  data?: Maybe<Array<Maybe<TEventFavouritesResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTEventInfo = {
   __typename?: 'TotalTEventInfo',
  data?: Maybe<Array<Maybe<TEventInfo>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTEventSpeakerInfo = {
   __typename?: 'TotalTEventSpeakerInfo',
  data?: Maybe<Array<Maybe<TEventSpeakerInfo>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTMemberEventInfo = {
   __typename?: 'TotalTMemberEventInfo',
  data?: Maybe<Array<Maybe<TMemberEventResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTMemberEventInfoResponse = {
   __typename?: 'TotalTMemberEventInfoResponse',
  data?: Maybe<Array<Maybe<TMemberEventResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTMemberInfo = {
   __typename?: 'TotalTMemberInfo',
  data?: Maybe<Array<Maybe<TMemberInfo>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTPartnersResponse = {
   __typename?: 'TotalTPartnersResponse',
  data?: Maybe<Array<Maybe<TPartnersResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTPrivilegeBookingResponse = {
   __typename?: 'TotalTPrivilegeBookingResponse',
  data?: Maybe<Array<Maybe<TPrivilegeBookingResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTPrivilegeMembersRespose = {
   __typename?: 'TotalTPrivilegeMembersRespose',
  data?: Maybe<Array<Maybe<TPrivilegeMembersRespose>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTPrivilegeResponse = {
   __typename?: 'TotalTPrivilegeResponse',
  data?: Maybe<Array<Maybe<TPrivilegeResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTPrivilegesMembersRespose = {
   __typename?: 'TotalTPrivilegesMembersRespose',
  data?: Maybe<Array<Maybe<TPrivilegeMembersRespose>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTServiceBookingInfo = {
   __typename?: 'TotalTServiceBookingInfo',
  data?: Maybe<Array<Maybe<TServiceBookingInfo>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTServiceMenuAssetsResponse = {
   __typename?: 'TotalTServiceMenuAssetsResponse',
  data?: Maybe<Array<Maybe<TServiceMenuAssetsResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

export type TotalTServiceTypeResponse = {
   __typename?: 'TotalTServiceTypeResponse',
  data?: Maybe<Array<Maybe<TServiceTypeResponse>>>,
  pageInfo?: Maybe<Pageinfo>,
};

/** Partners collection */
export type TPartners = {
   __typename?: 'TPartners',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** first name with min chars 1 and max chars is 128 */
  firstname?: Maybe<Scalars['String']>,
  /** last name with min chars 1 and max chars is 128 */
  lastname?: Maybe<Scalars['String']>,
  /** unique email with 5 min chars and 128 max chars */
  email: Scalars['String'],
  /** phone unmber */
  phone?: Maybe<Scalars['String']>,
  /** countryCodeIndex */
  countryCode?: Maybe<Scalars['String']>,
  /** avatar preview image */
  avatar?: Maybe<Scalars['ID']>,
  /** partner user name with 1 min char and 128 max chars */
  name?: Maybe<Scalars['String']>,
  /** partner url link with 1 min char and 128 max xhars */
  site_link?: Maybe<Scalars['String']>,
};

/** Partners collection */
export type TPartnersResponse = {
   __typename?: 'TPartnersResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** first name with min chars 1 and max chars is 128 */
  firstname?: Maybe<Scalars['String']>,
  /** last name with min chars 1 and max chars is 128 */
  lastname?: Maybe<Scalars['String']>,
  /** unique email with 5 min chars and 128 max chars */
  email?: Maybe<Scalars['String']>,
  /** phone unmber */
  phone?: Maybe<Scalars['String']>,
  /** phone countryCodeIndex */
  countryCode?: Maybe<Scalars['String']>,
  /** avatar preview image */
  avatar?: Maybe<TAsset>,
  /** partner user name with 1 min char and 128 max chars */
  name?: Maybe<Scalars['String']>,
  /** partner url link with 1 min char and 128 max xhars */
  site_link?: Maybe<Scalars['String']>,
};

/** Privilege coolection */
export type TPrivilege = {
   __typename?: 'TPrivilege',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** Required Privilege title */
  title: Scalars['String'],
  /** Privilege description */
  description?: Maybe<Scalars['String']>,
  /** Privilege status ability */
  status: TPrivilegeStatus,
  /** when Privilege is able */
  start: Scalars['Date'],
  /** when Privilege is able */
  end: Scalars['Date'],
  /** Privilege rule of activation */
  rule: TPrivilegePeriodRule,
  /** Privilege partner */
  partner: Scalars['ID'],
  /** Privilege image preview */
  preview?: Maybe<Scalars['ID']>,
};

/** PrivilegeBooking collection */
export type TPrivilegeBooking = {
   __typename?: 'TPrivilegeBooking',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  memberId: Scalars['ID'],
  privilegeId: Scalars['ID'],
  start: Scalars['Date'],
  end: Scalars['Date'],
};

/** Privilege Booking response structure */
export type TPrivilegeBookingResponse = {
   __typename?: 'TPrivilegeBookingResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  memberId?: Maybe<TMemberInfo>,
  privilegeId?: Maybe<TPrivilegeResponse>,
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
};

/** Privilege Booking response structure */
export type TPrivilegeDetailsResponse = {
   __typename?: 'TPrivilegeDetailsResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  title?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  rule?: Maybe<TPrivilegePeriodRule>,
  status?: Maybe<TPrivilegeStatus>,
  partner?: Maybe<TPartnersResponse>,
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
  preview?: Maybe<TAsset>,
  last_activation?: Maybe<TPrivilegeBookingResponse>,
  last_activations?: Maybe<Array<Maybe<TPrivilegeBookingResponse>>>,
  all_activations?: Maybe<Array<Maybe<TPrivilegeBookingResponse>>>,
  /** count of used depend from rules and activations */
  used?: Maybe<Scalars['Int']>,
  /** count of remain depend from rules, activations and used */
  remain?: Maybe<Scalars['Int']>,
};

export type TPrivilegeInput = {
  _id?: Maybe<Scalars['ID']>,
  /** Required Privilege title */
  title: Scalars['String'],
  /** Privilege description */
  description?: Maybe<Scalars['String']>,
  /** Privilege status ability */
  status: TPrivilegeStatus,
  /** when Privilege is able */
  start: Scalars['Date'],
  /** when Privilege is able */
  end: Scalars['Date'],
  /** Privilege partner */
  partner: Scalars['ID'],
  /** Privilege image preview */
  preview?: Maybe<Scalars['ID']>,
  rule: TPrivilegePeriodRuleInput,
};

/** Privilege coolection */
export type TPrivilegeMembers = {
   __typename?: 'TPrivilegeMembers',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  memberId: Scalars['ID'],
  privilegeId: Scalars['ID'],
};

/** Privilege coolection */
export type TPrivilegeMembersRespose = {
   __typename?: 'TPrivilegeMembersRespose',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  memberId?: Maybe<TMemberInfo>,
  privilegeId?: Maybe<TPrivilegeResponse>,
};

/** Privilege PeriodRule, can be limited require start and end date, or can repeat */
export type TPrivilegePeriodRule = {
   __typename?: 'TPrivilegePeriodRule',
  repeat?: Maybe<TPrivilegePeriodRuleRepeat>,
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
  count?: Maybe<Scalars['Int']>,
};

/** Privilege PeriodRule, can be limited require start and end date, or can repeat */
export type TPrivilegePeriodRuleInput = {
  repeat?: Maybe<TPrivilegePeriodRuleRepeat>,
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
  count: Scalars['Int'],
};

/** Privilege PeriodRule repeat */
export enum TPrivilegePeriodRuleRepeat {
  /** require activation only today */
  Day = 'DAY',
  /** require activation only today */
  Week = 'WEEK',
  /** require activation only in current month */
  Month = 'MONTH',
  /** require activation only in current year */
  Year = 'YEAR'
}

/** Privilege Response structure */
export type TPrivilegeResponse = {
   __typename?: 'TPrivilegeResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  title?: Maybe<Scalars['String']>,
  description?: Maybe<Scalars['String']>,
  rule?: Maybe<TPrivilegePeriodRule>,
  status?: Maybe<TPrivilegeStatus>,
  partner?: Maybe<TPartnersResponse>,
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
  preview?: Maybe<TAsset>,
};

/** Privilege status */
export enum TPrivilegeStatus {
  Active = 'ACTIVE',
  Canceled = 'CANCELED'
}

/** service collection */
export type TService = {
   __typename?: 'TService',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** Service title */
  title?: Maybe<Scalars['String']>,
  /** Service description */
  description?: Maybe<Scalars['String']>,
  subtitle1?: Maybe<Scalars['String']>,
  subtitle2?: Maybe<Scalars['String']>,
  /** Service assets files */
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** Service image preivew */
  preview?: Maybe<Scalars['ID']>,
  typeId: Scalars['ID'],
  /** restricted, will be removed */
  price?: Maybe<Scalars['Int']>,
};

/** service booking collection */
export type TServiceBooking = {
   __typename?: 'TServiceBooking',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** start occupation when service is reserverd for using */
  startDate?: Maybe<Scalars['Date']>,
  /** end occupation when service is reserverd for using */
  endDate?: Maybe<Scalars['Date']>,
  status?: Maybe<TServiceBookingStatusEnum>,
  memberId?: Maybe<Scalars['ID']>,
  serviceId?: Maybe<Scalars['ID']>,
};

/** service booking response structure */
export type TServiceBookingInfo = {
   __typename?: 'TServiceBookingInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  startDate?: Maybe<Scalars['Date']>,
  endDate?: Maybe<Scalars['Date']>,
  status?: Maybe<TServiceBookingStatusEnum>,
  memberId?: Maybe<TMemberInfo>,
  serviceId?: Maybe<TServiceInfo>,
};

/** service booking status */
export enum TServiceBookingStatusEnum {
  Requested = 'REQUESTED',
  Active = 'ACTIVE',
  Rejected = 'REJECTED',
  Closed = 'CLOSED'
}

/** service response structure */
export type TServiceInfo = {
   __typename?: 'TServiceInfo',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** Service required title */
  title: Scalars['String'],
  /** Service description */
  description?: Maybe<Scalars['String']>,
  subtitle1?: Maybe<Scalars['String']>,
  subtitle2?: Maybe<Scalars['String']>,
  /** Service assets */
  assets?: Maybe<Array<Maybe<TServiceMenuAssetsResponse>>>,
  /** Service image preview */
  preview?: Maybe<TAsset>,
  /** Service type */
  typeId?: Maybe<TServiceTypeResponse>,
  /** restricted, will be removed */
  price?: Maybe<Scalars['Int']>,
};

/** service menu assets input params */
export type TServiceMenuAssetsInput = {
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
};

/** service menu assets response structure */
export type TServiceMenuAssetsResponse = {
   __typename?: 'TServiceMenuAssetsResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  assets?: Maybe<Array<Maybe<TAsset>>>,
  start?: Maybe<Scalars['Date']>,
  end?: Maybe<Scalars['Date']>,
};

/** service types */
export type TServiceType = {
   __typename?: 'TServiceType',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  /** who was update the entity */
  updatedBy?: Maybe<Scalars['ID']>,
  /** who was create the entity */
  createdBy?: Maybe<Scalars['ID']>,
  /** timestamp when entity was created */
  createdAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was updated */
  updatedAt?: Maybe<Scalars['Date']>,
  /** timestamp when entity was deleted */
  deletedAt?: Maybe<Scalars['Date']>,
  /** Service type title */
  title: Scalars['String'],
  /** Service type description */
  description?: Maybe<Scalars['String']>,
};

/** service types response */
export type TServiceTypeResponse = {
   __typename?: 'TServiceTypeResponse',
  _id?: Maybe<Scalars['ID']>,
  uuid?: Maybe<Scalars['String']>,
  updatedBy?: Maybe<TConciergeInfoResponse>,
  createdBy?: Maybe<TConciergeInfoResponse>,
  createdAt?: Maybe<Scalars['Date']>,
  updatedAt?: Maybe<Scalars['Date']>,
  deletedAt?: Maybe<Scalars['Date']>,
  /** Service type description */
  description?: Maybe<Scalars['String']>,
  /** Service type title */
  title?: Maybe<Scalars['String']>,
};

/** member configs respose structure */
export type TwofaConfig = {
   __typename?: 'TwofaConfig',
  authyid?: Maybe<Scalars['String']>,
  enabled?: Maybe<Scalars['Boolean']>,
  countryCode?: Maybe<Scalars['String']>,
  phoneNumber?: Maybe<Scalars['String']>,
  backupKeys?: Maybe<Array<Maybe<BackupKeyType>>>,
  backupTimeStamp?: Maybe<Scalars['Date']>,
  code?: Maybe<Scalars['String']>,
  codeTime?: Maybe<Scalars['Date']>,
  sendMessagesTimes?: Maybe<Array<Maybe<Scalars['Date']>>>,
};

export type UpdateConciergeInput = {
  firstname: Scalars['String'],
  lastname: Scalars['String'],
  email: Scalars['String'],
  phone: Scalars['String'],
  countryCode: Scalars['String'],
  isBlocked?: Maybe<Scalars['Date']>,
  avatar?: Maybe<Scalars['ID']>,
  password?: Maybe<Scalars['String']>,
};

export type UpdateEventAgendaInput = {
  _id?: Maybe<Scalars['ID']>,
  /** required title of event agenda */
  title: Scalars['String'],
  /** description of event agenda */
  description?: Maybe<Scalars['String']>,
  /** required start datetime of event agenda */
  startDate: Scalars['Date'],
  /** required end datetime of event agenda */
  endDate: Scalars['Date'],
  /** required event which belong agenda */
  eventId: Scalars['ID'],
  /** speakers of the agenda */
  speakers?: Maybe<Array<Maybe<Scalars['ID']>>>,
};

export type UpdateEventInput = {
  /** required event title */
  title?: Maybe<Scalars['String']>,
  /** event start Date to be available */
  startDate?: Maybe<Scalars['Date']>,
  /** event end Date to be available */
  endDate?: Maybe<Scalars['Date']>,
  /** event location with 1 min char and 256 max xhars */
  location?: Maybe<Scalars['String']>,
  /** description of event */
  description1?: Maybe<Scalars['String']>,
  /** more description of event */
  description2?: Maybe<Scalars['String']>,
  /** file assets for event */
  assets?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** total available places to book */
  capacity?: Maybe<Scalars['Int']>,
  /** total booked places */
  bookedCapacity?: Maybe<Scalars['Int']>,
  /** event type */
  typeId?: Maybe<Scalars['ID']>,
  /** event image preview */
  preview?: Maybe<Scalars['ID']>,
  /** event since when member can register */
  canRegisterBefore?: Maybe<Scalars['Date']>,
  /** event since when member can cancel booking */
  canCancelBefore?: Maybe<Scalars['Date']>,
  /** event speakers */
  speakers?: Maybe<Array<Maybe<Scalars['ID']>>>,
  /** event status ability */
  status?: Maybe<TEventStatusEnum>,
};

export type UpdateEventSpeakerInput = {
  _id?: Maybe<Scalars['ID']>,
  /** name of user-speaker */
  name?: Maybe<Scalars['String']>,
  /** description of user-speaker like who is he/she */
  description?: Maybe<Scalars['String']>,
  /** avatar image preview of user-speaker */
  preview?: Maybe<Scalars['ID']>,
};




export type ResolverTypeWrapper<T> = Promise<T> | T;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;


export type StitchingResolver<TResult, TParent, TContext, TArgs> = {
  fragment: string;
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};

export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> =
  | ResolverFn<TResult, TParent, TContext, TArgs>
  | StitchingResolver<TResult, TParent, TContext, TArgs>;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterator<TResult> | Promise<AsyncIterator<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Query: ResolverTypeWrapper<{}>,
  AuthInfo: ResolverTypeWrapper<AuthInfo>,
  String: ResolverTypeWrapper<Scalars['String']>,
  Boolean: ResolverTypeWrapper<Scalars['Boolean']>,
  TConciergeInfoResponse: ResolverTypeWrapper<TConciergeInfoResponse>,
  ID: ResolverTypeWrapper<Scalars['ID']>,
  Date: ResolverTypeWrapper<Scalars['Date']>,
  TAsset: ResolverTypeWrapper<TAsset>,
  TConciergeType: TConciergeType,
  InputBaseFilter: InputBaseFilter,
  Int: ResolverTypeWrapper<Scalars['Int']>,
  InputBaseFilterSort: InputBaseFilterSort,
  TotalTConcierge: ResolverTypeWrapper<TotalTConcierge>,
  Pageinfo: ResolverTypeWrapper<Pageinfo>,
  TotalTEventAgendaItemResponse: ResolverTypeWrapper<TotalTEventAgendaItemResponse>,
  TEventAgendaItemResponse: ResolverTypeWrapper<TEventAgendaItemResponse>,
  TEventSpeakerInfo: ResolverTypeWrapper<TEventSpeakerInfo>,
  FetchEventsFilterInput: FetchEventsFilterInput,
  TotalTEventInfo: ResolverTypeWrapper<TotalTEventInfo>,
  TEventInfo: ResolverTypeWrapper<TEventInfo>,
  TEventTypeResponse: ResolverTypeWrapper<TEventTypeResponse>,
  TEventStatusEnum: TEventStatusEnum,
  TotalTMemberEventInfoResponse: ResolverTypeWrapper<TotalTMemberEventInfoResponse>,
  TMemberEventResponse: ResolverTypeWrapper<TMemberEventResponse>,
  TMemberInfo: ResolverTypeWrapper<TMemberInfo>,
  TwofaConfig: ResolverTypeWrapper<TwofaConfig>,
  BackupKeyType: ResolverTypeWrapper<BackupKeyType>,
  CardInfo: ResolverTypeWrapper<CardInfo>,
  AppSettings: ResolverTypeWrapper<AppSettings>,
  TEventBookingStatusEnum: TEventBookingStatusEnum,
  TotalTEventSpeakerInfo: ResolverTypeWrapper<TotalTEventSpeakerInfo>,
  TotalTMemberInfo: ResolverTypeWrapper<TotalTMemberInfo>,
  MemberListInput: MemberListInput,
  TotalTPrivilegeMembersRespose: ResolverTypeWrapper<TotalTPrivilegeMembersRespose>,
  TPrivilegeMembersRespose: ResolverTypeWrapper<TPrivilegeMembersRespose>,
  TPrivilegeResponse: ResolverTypeWrapper<TPrivilegeResponse>,
  TPrivilegePeriodRule: ResolverTypeWrapper<TPrivilegePeriodRule>,
  TPrivilegePeriodRuleRepeat: TPrivilegePeriodRuleRepeat,
  TPrivilegeStatus: TPrivilegeStatus,
  TPartnersResponse: ResolverTypeWrapper<TPartnersResponse>,
  MemberPrivilegeActivationsListInput: MemberPrivilegeActivationsListInput,
  TotalTPrivilegeBookingResponse: ResolverTypeWrapper<TotalTPrivilegeBookingResponse>,
  TPrivilegeBookingResponse: ResolverTypeWrapper<TPrivilegeBookingResponse>,
  TotalTServiceBookingInfo: ResolverTypeWrapper<TotalTServiceBookingInfo>,
  TServiceBookingInfo: ResolverTypeWrapper<TServiceBookingInfo>,
  TServiceBookingStatusEnum: TServiceBookingStatusEnum,
  TServiceInfo: ResolverTypeWrapper<TServiceInfo>,
  TServiceMenuAssetsResponse: ResolverTypeWrapper<TServiceMenuAssetsResponse>,
  TServiceTypeResponse: ResolverTypeWrapper<TServiceTypeResponse>,
  TotalTEventFavouritesResponse: ResolverTypeWrapper<TotalTEventFavouritesResponse>,
  TEventFavouritesResponse: ResolverTypeWrapper<TEventFavouritesResponse>,
  TotalTPartnersResponse: ResolverTypeWrapper<TotalTPartnersResponse>,
  TotalTPrivilegeResponse: ResolverTypeWrapper<TotalTPrivilegeResponse>,
  PrivilegeListInput: PrivilegeListInput,
  TotalTPrivilegesMembersRespose: ResolverTypeWrapper<TotalTPrivilegesMembersRespose>,
  TotalTServiceMenuAssetsResponse: ResolverTypeWrapper<TotalTServiceMenuAssetsResponse>,
  TotalTServiceTypeResponse: ResolverTypeWrapper<TotalTServiceTypeResponse>,
  Mutation: ResolverTypeWrapper<{}>,
  OperationStatus: ResolverTypeWrapper<OperationStatus>,
  AssetPeiodInputFields: AssetPeiodInputFields,
  TAssetPeriodInfo: ResolverTypeWrapper<TAssetPeriodInfo>,
  Upload: ResolverTypeWrapper<Scalars['Upload']>,
  AddConciergeInput: AddConciergeInput,
  UpdateConciergeInput: UpdateConciergeInput,
  AddEventAgendaInput: AddEventAgendaInput,
  UpdateEventAgendaInput: UpdateEventAgendaInput,
  AddEventInput: AddEventInput,
  UpdateEventInput: UpdateEventInput,
  TMemberBookingEventInfo: ResolverTypeWrapper<TMemberBookingEventInfo>,
  AddEventSpeakerInput: AddEventSpeakerInput,
  UpdateEventSpeakerInput: UpdateEventSpeakerInput,
  AddMemberInput: AddMemberInput,
  CardInfoInput: CardInfoInput,
  AppSettingsInput: AppSettingsInput,
  TPrivilegeInput: TPrivilegeInput,
  TPrivilegePeriodRuleInput: TPrivilegePeriodRuleInput,
  ApplyMemberToPrivilege: ApplyMemberToPrivilege,
  AddServiceInput: AddServiceInput,
  Float: ResolverTypeWrapper<Scalars['Float']>,
  EditBookinServiceInput: EditBookinServiceInput,
  ConciergeInput: ConciergeInput,
  AgendaItemInput: AgendaItemInput,
  TotalTMemberEventInfo: ResolverTypeWrapper<TotalTMemberEventInfo>,
  AuthToken: ResolverTypeWrapper<AuthToken>,
  Base: ResolverTypeWrapper<Base>,
  BaseModelSchema: ResolverTypeWrapper<BaseModelSchema>,
  BaseModel: ResolverTypeWrapper<BaseModel>,
  DateRange: ResolverTypeWrapper<DateRange>,
  MediaAsset: ResolverTypeWrapper<MediaAsset>,
  ROLES: Roles,
  Owner: ResolverTypeWrapper<Owner>,
  TAssetPeriod: ResolverTypeWrapper<TAssetPeriod>,
  BaseAdminSchema: ResolverTypeWrapper<BaseAdminSchema>,
  BaseAdminResponse: ResolverTypeWrapper<BaseAdminResponse>,
  TConcierge: ResolverTypeWrapper<TConcierge>,
  TPartners: ResolverTypeWrapper<TPartners>,
  TMemberEventInfo: ResolverTypeWrapper<TMemberEventInfo>,
  TEventSpeaker: ResolverTypeWrapper<TEventSpeaker>,
  TEventType: ResolverTypeWrapper<TEventType>,
  TEventAgendaItem: ResolverTypeWrapper<TEventAgendaItem>,
  TEventFavourites: ResolverTypeWrapper<TEventFavourites>,
  TEvent: ResolverTypeWrapper<TEvent>,
  TMemberEventInfoResponse: ResolverTypeWrapper<TMemberEventInfoResponse>,
  LanguageEnum: LanguageEnum,
  TMember: ResolverTypeWrapper<TMember>,
  TPrivilegeMembers: ResolverTypeWrapper<TPrivilegeMembers>,
  TPrivilege: ResolverTypeWrapper<TPrivilege>,
  TPrivilegeBooking: ResolverTypeWrapper<TPrivilegeBooking>,
  TPrivilegeDetailsResponse: ResolverTypeWrapper<TPrivilegeDetailsResponse>,
  TServiceType: ResolverTypeWrapper<TServiceType>,
  OcupationDateService: ResolverTypeWrapper<OcupationDateService>,
  TService: ResolverTypeWrapper<TService>,
  TServiceBooking: ResolverTypeWrapper<TServiceBooking>,
  TServiceMenuAssetsInput: TServiceMenuAssetsInput,
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Query: {},
  AuthInfo: AuthInfo,
  String: Scalars['String'],
  Boolean: Scalars['Boolean'],
  TConciergeInfoResponse: TConciergeInfoResponse,
  ID: Scalars['ID'],
  Date: Scalars['Date'],
  TAsset: TAsset,
  TConciergeType: TConciergeType,
  InputBaseFilter: InputBaseFilter,
  Int: Scalars['Int'],
  InputBaseFilterSort: InputBaseFilterSort,
  TotalTConcierge: TotalTConcierge,
  Pageinfo: Pageinfo,
  TotalTEventAgendaItemResponse: TotalTEventAgendaItemResponse,
  TEventAgendaItemResponse: TEventAgendaItemResponse,
  TEventSpeakerInfo: TEventSpeakerInfo,
  FetchEventsFilterInput: FetchEventsFilterInput,
  TotalTEventInfo: TotalTEventInfo,
  TEventInfo: TEventInfo,
  TEventTypeResponse: TEventTypeResponse,
  TEventStatusEnum: TEventStatusEnum,
  TotalTMemberEventInfoResponse: TotalTMemberEventInfoResponse,
  TMemberEventResponse: TMemberEventResponse,
  TMemberInfo: TMemberInfo,
  TwofaConfig: TwofaConfig,
  BackupKeyType: BackupKeyType,
  CardInfo: CardInfo,
  AppSettings: AppSettings,
  TEventBookingStatusEnum: TEventBookingStatusEnum,
  TotalTEventSpeakerInfo: TotalTEventSpeakerInfo,
  TotalTMemberInfo: TotalTMemberInfo,
  MemberListInput: MemberListInput,
  TotalTPrivilegeMembersRespose: TotalTPrivilegeMembersRespose,
  TPrivilegeMembersRespose: TPrivilegeMembersRespose,
  TPrivilegeResponse: TPrivilegeResponse,
  TPrivilegePeriodRule: TPrivilegePeriodRule,
  TPrivilegePeriodRuleRepeat: TPrivilegePeriodRuleRepeat,
  TPrivilegeStatus: TPrivilegeStatus,
  TPartnersResponse: TPartnersResponse,
  MemberPrivilegeActivationsListInput: MemberPrivilegeActivationsListInput,
  TotalTPrivilegeBookingResponse: TotalTPrivilegeBookingResponse,
  TPrivilegeBookingResponse: TPrivilegeBookingResponse,
  TotalTServiceBookingInfo: TotalTServiceBookingInfo,
  TServiceBookingInfo: TServiceBookingInfo,
  TServiceBookingStatusEnum: TServiceBookingStatusEnum,
  TServiceInfo: TServiceInfo,
  TServiceMenuAssetsResponse: TServiceMenuAssetsResponse,
  TServiceTypeResponse: TServiceTypeResponse,
  TotalTEventFavouritesResponse: TotalTEventFavouritesResponse,
  TEventFavouritesResponse: TEventFavouritesResponse,
  TotalTPartnersResponse: TotalTPartnersResponse,
  TotalTPrivilegeResponse: TotalTPrivilegeResponse,
  PrivilegeListInput: PrivilegeListInput,
  TotalTPrivilegesMembersRespose: TotalTPrivilegesMembersRespose,
  TotalTServiceMenuAssetsResponse: TotalTServiceMenuAssetsResponse,
  TotalTServiceTypeResponse: TotalTServiceTypeResponse,
  Mutation: {},
  OperationStatus: OperationStatus,
  AssetPeiodInputFields: AssetPeiodInputFields,
  TAssetPeriodInfo: TAssetPeriodInfo,
  Upload: Scalars['Upload'],
  AddConciergeInput: AddConciergeInput,
  UpdateConciergeInput: UpdateConciergeInput,
  AddEventAgendaInput: AddEventAgendaInput,
  UpdateEventAgendaInput: UpdateEventAgendaInput,
  AddEventInput: AddEventInput,
  UpdateEventInput: UpdateEventInput,
  TMemberBookingEventInfo: TMemberBookingEventInfo,
  AddEventSpeakerInput: AddEventSpeakerInput,
  UpdateEventSpeakerInput: UpdateEventSpeakerInput,
  AddMemberInput: AddMemberInput,
  CardInfoInput: CardInfoInput,
  AppSettingsInput: AppSettingsInput,
  TPrivilegeInput: TPrivilegeInput,
  TPrivilegePeriodRuleInput: TPrivilegePeriodRuleInput,
  ApplyMemberToPrivilege: ApplyMemberToPrivilege,
  AddServiceInput: AddServiceInput,
  Float: Scalars['Float'],
  EditBookinServiceInput: EditBookinServiceInput,
  ConciergeInput: ConciergeInput,
  AgendaItemInput: AgendaItemInput,
  TotalTMemberEventInfo: TotalTMemberEventInfo,
  AuthToken: AuthToken,
  Base: Base,
  BaseModelSchema: BaseModelSchema,
  BaseModel: BaseModel,
  DateRange: DateRange,
  MediaAsset: MediaAsset,
  ROLES: Roles,
  Owner: Owner,
  TAssetPeriod: TAssetPeriod,
  BaseAdminSchema: BaseAdminSchema,
  BaseAdminResponse: BaseAdminResponse,
  TConcierge: TConcierge,
  TPartners: TPartners,
  TMemberEventInfo: TMemberEventInfo,
  TEventSpeaker: TEventSpeaker,
  TEventType: TEventType,
  TEventAgendaItem: TEventAgendaItem,
  TEventFavourites: TEventFavourites,
  TEvent: TEvent,
  TMemberEventInfoResponse: TMemberEventInfoResponse,
  LanguageEnum: LanguageEnum,
  TMember: TMember,
  TPrivilegeMembers: TPrivilegeMembers,
  TPrivilege: TPrivilege,
  TPrivilegeBooking: TPrivilegeBooking,
  TPrivilegeDetailsResponse: TPrivilegeDetailsResponse,
  TServiceType: TServiceType,
  OcupationDateService: OcupationDateService,
  TService: TService,
  TServiceBooking: TServiceBooking,
  TServiceMenuAssetsInput: TServiceMenuAssetsInput,
};

export type ObjectIdDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type NamespaceDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type CollectionDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type VersionDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type IgnoreDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type DefaultDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type RequiredDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type UniqueDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type SetonceDirectiveResolver<Result, Parent, ContextType = any, Args = {  }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type UuidDirectiveResolver<Result, Parent, ContextType = any, Args = {  }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type OptionalDirectiveResolver<Result, Parent, ContextType = any, Args = {  }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type MinDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['Int']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type MaxDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['Int']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type MinlengthDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['Int']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type MaxlengthDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['Int']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type TrimDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type UppercaseDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['Int']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type LowercaseDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['Int']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type HashDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type ExtendsDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Scalars['String']>>,
  select?: Maybe<Maybe<Array<Maybe<Scalars['String']>>>>,
  reject?: Maybe<Maybe<Array<Maybe<Scalars['String']>>>>,
  fragment?: Maybe<Maybe<Array<Maybe<Scalars['String']>>>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type MongoDirectiveResolver<Result, Parent, ContextType = any, Args = {  }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type FragmentDirectiveResolver<Result, Parent, ContextType = any, Args = {   value?: Maybe<Maybe<Array<Maybe<Scalars['String']>>>> }> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type AppSettingsResolvers<ContextType = any, ParentType extends ResolversParentTypes['AppSettings'] = ResolversParentTypes['AppSettings']> = {
  publicMode?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
  pushMode?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
  secureLoginMode?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
  language?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type AuthInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['AuthInfo'] = ResolversParentTypes['AuthInfo']> = {
  error?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  success?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
  token?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  user?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
};

export type AuthTokenResolvers<ContextType = any, ParentType extends ResolversParentTypes['AuthToken'] = ResolversParentTypes['AuthToken']> = {
  error?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  success?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
  token?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
};

export type BackupKeyTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['BackupKeyType'] = ResolversParentTypes['BackupKeyType']> = {
  code?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  isUsed?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
};

export type BaseResolvers<ContextType = any, ParentType extends ResolversParentTypes['Base'] = ResolversParentTypes['Base']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type BaseAdminResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['BaseAdminResponse'] = ResolversParentTypes['BaseAdminResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  lastname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  phone?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  avatar?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
};

export type BaseAdminSchemaResolvers<ContextType = any, ParentType extends ResolversParentTypes['BaseAdminSchema'] = ResolversParentTypes['BaseAdminSchema']> = {
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  lastname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  email?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  phone?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  avatar?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
};

export type BaseModelResolvers<ContextType = any, ParentType extends ResolversParentTypes['BaseModel'] = ResolversParentTypes['BaseModel']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type BaseModelSchemaResolvers<ContextType = any, ParentType extends ResolversParentTypes['BaseModelSchema'] = ResolversParentTypes['BaseModelSchema']> = {
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type CardInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['CardInfo'] = ResolversParentTypes['CardInfo']> = {
  code?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  expire?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  cardHolder?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export interface DateScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Date'], any> {
  name: 'Date'
}

export type DateRangeResolvers<ContextType = any, ParentType extends ResolversParentTypes['DateRange'] = ResolversParentTypes['DateRange']> = {
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type MediaAssetResolvers<ContextType = any, ParentType extends ResolversParentTypes['MediaAsset'] = ResolversParentTypes['MediaAsset']> = {
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  login?: Resolver<Maybe<ResolversTypes['AuthInfo']>, ParentType, ContextType, RequireFields<MutationLoginArgs, 'email' | 'password'>>,
  resetPsw?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationResetPswArgs, 'email'>>,
  notifyAllMembers?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationNotifyAllMembersArgs, 'title' | 'text'>>,
  addAssetPeriod?: Resolver<Maybe<ResolversTypes['TAssetPeriodInfo']>, ParentType, ContextType, MutationAddAssetPeriodArgs>,
  removeAssetPeriod?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveAssetPeriodArgs, 'id'>>,
  editAssetPeriod?: Resolver<Maybe<ResolversTypes['TAssetPeriodInfo']>, ParentType, ContextType, RequireFields<MutationEditAssetPeriodArgs, 'id'>>,
  addAsset?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType, RequireFields<MutationAddAssetArgs, 'file'>>,
  removeAsset?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveAssetArgs, 'id'>>,
  addConcierge?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType, RequireFields<MutationAddConciergeArgs, 'input'>>,
  updateConcierge?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType, RequireFields<MutationUpdateConciergeArgs, 'conciergeId' | 'input'>>,
  removeConcierge?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveConciergeArgs, 'conciergeId'>>,
  addEventAgenda?: Resolver<Maybe<ResolversTypes['TEventAgendaItemResponse']>, ParentType, ContextType, MutationAddEventAgendaArgs>,
  updateEventAgenda?: Resolver<Maybe<ResolversTypes['TEventAgendaItemResponse']>, ParentType, ContextType, MutationUpdateEventAgendaArgs>,
  removeEventAgenda?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveEventAgendaArgs, '_id'>>,
  addEvent?: Resolver<Maybe<ResolversTypes['TEventInfo']>, ParentType, ContextType, MutationAddEventArgs>,
  removeEvent?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, MutationRemoveEventArgs>,
  updateEvent?: Resolver<Maybe<ResolversTypes['TEventInfo']>, ParentType, ContextType, MutationUpdateEventArgs>,
  confirmEventBookingForUser?: Resolver<Maybe<ResolversTypes['TMemberBookingEventInfo']>, ParentType, ContextType, RequireFields<MutationConfirmEventBookingForUserArgs, '_id' | 'eventId' | 'userId'>>,
  rejectEventBookingForUser?: Resolver<Maybe<ResolversTypes['TMemberBookingEventInfo']>, ParentType, ContextType, RequireFields<MutationRejectEventBookingForUserArgs, '_id' | 'eventId' | 'userId'>>,
  addMemberToEvent?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationAddMemberToEventArgs, 'eventId' | 'memberId'>>,
  addEventSpeaker?: Resolver<Maybe<ResolversTypes['TEventSpeakerInfo']>, ParentType, ContextType, MutationAddEventSpeakerArgs>,
  updateEventSpeaker?: Resolver<Maybe<ResolversTypes['TEventSpeakerInfo']>, ParentType, ContextType, MutationUpdateEventSpeakerArgs>,
  removeEventSpeaker?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveEventSpeakerArgs, '_id'>>,
  addMember?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType, RequireFields<MutationAddMemberArgs, 'input'>>,
  updateMember?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType, RequireFields<MutationUpdateMemberArgs, 'memberId' | 'input'>>,
  removeMember?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveMemberArgs, 'memberId'>>,
  blockMember?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationBlockMemberArgs, 'memberId'>>,
  addPrivilege?: Resolver<Maybe<ResolversTypes['TPrivilegeResponse']>, ParentType, ContextType, RequireFields<MutationAddPrivilegeArgs, 'input'>>,
  updatePrivilege?: Resolver<Maybe<ResolversTypes['TPrivilegeResponse']>, ParentType, ContextType, RequireFields<MutationUpdatePrivilegeArgs, 'input'>>,
  removePrivilege?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemovePrivilegeArgs, '_id'>>,
  applyMemberToPrivilege?: Resolver<Maybe<ResolversTypes['TPrivilegeMembersRespose']>, ParentType, ContextType, RequireFields<MutationApplyMemberToPrivilegeArgs, 'input'>>,
  removeMemberFromPrivilege?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveMemberFromPrivilegeArgs, '_id' | 'input'>>,
  addService?: Resolver<Maybe<ResolversTypes['TServiceInfo']>, ParentType, ContextType, RequireFields<MutationAddServiceArgs, 'input'>>,
  updateService?: Resolver<Maybe<ResolversTypes['TServiceInfo']>, ParentType, ContextType, RequireFields<MutationUpdateServiceArgs, '_id' | 'input'>>,
  removeService?: Resolver<Maybe<ResolversTypes['OperationStatus']>, ParentType, ContextType, RequireFields<MutationRemoveServiceArgs, '_id'>>,
  updateBookingService?: Resolver<Maybe<ResolversTypes['TServiceBookingInfo']>, ParentType, ContextType, RequireFields<MutationUpdateBookingServiceArgs, '_id' | 'input'>>,
};

export type OcupationDateServiceResolvers<ContextType = any, ParentType extends ResolversParentTypes['OcupationDateService'] = ResolversParentTypes['OcupationDateService']> = {
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type OperationStatusResolvers<ContextType = any, ParentType extends ResolversParentTypes['OperationStatus'] = ResolversParentTypes['OperationStatus']> = {
  error?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  success?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
};

export type OwnerResolvers<ContextType = any, ParentType extends ResolversParentTypes['Owner'] = ResolversParentTypes['Owner']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  owner?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
};

export type PageinfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['Pageinfo'] = ResolversParentTypes['Pageinfo']> = {
  total?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  isAuth?: Resolver<Maybe<ResolversTypes['AuthInfo']>, ParentType, ContextType>,
  assetById?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType, RequireFields<QueryAssetByIdArgs, 'id'>>,
  assetsList?: Resolver<Maybe<Array<Maybe<ResolversTypes['TAsset']>>>, ParentType, ContextType>,
  conciergeById?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType, RequireFields<QueryConciergeByIdArgs, 'id'>>,
  conciergeList?: Resolver<Maybe<ResolversTypes['TotalTConcierge']>, ParentType, ContextType, QueryConciergeListArgs>,
  fetchEventAgenda?: Resolver<Maybe<ResolversTypes['TotalTEventAgendaItemResponse']>, ParentType, ContextType, RequireFields<QueryFetchEventAgendaArgs, 'eventId'>>,
  fetchEvents?: Resolver<Maybe<ResolversTypes['TotalTEventInfo']>, ParentType, ContextType, QueryFetchEventsArgs>,
  fetchEventById?: Resolver<Maybe<ResolversTypes['TEventInfo']>, ParentType, ContextType, RequireFields<QueryFetchEventByIdArgs, 'eventId'>>,
  fetchEventBookings?: Resolver<Maybe<ResolversTypes['TotalTMemberEventInfoResponse']>, ParentType, ContextType, RequireFields<QueryFetchEventBookingsArgs, 'eventId'>>,
  fetchAllEventSpeakers?: Resolver<Maybe<ResolversTypes['TotalTEventSpeakerInfo']>, ParentType, ContextType, QueryFetchAllEventSpeakersArgs>,
  memberById?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType, RequireFields<QueryMemberByIdArgs, 'id'>>,
  memberList?: Resolver<Maybe<ResolversTypes['TotalTMemberInfo']>, ParentType, ContextType, QueryMemberListArgs>,
  memberPrivilegeList?: Resolver<Maybe<ResolversTypes['TotalTPrivilegeMembersRespose']>, ParentType, ContextType, QueryMemberPrivilegeListArgs>,
  memberPrivilegeActivationsList?: Resolver<Maybe<ResolversTypes['TotalTPrivilegeBookingResponse']>, ParentType, ContextType, QueryMemberPrivilegeActivationsListArgs>,
  memberServicesBookingList?: Resolver<Maybe<ResolversTypes['TotalTServiceBookingInfo']>, ParentType, ContextType, QueryMemberServicesBookingListArgs>,
  memberEventsFavourites?: Resolver<Maybe<ResolversTypes['TotalTEventFavouritesResponse']>, ParentType, ContextType, QueryMemberEventsFavouritesArgs>,
  partnersList?: Resolver<Maybe<ResolversTypes['TotalTPartnersResponse']>, ParentType, ContextType, QueryPartnersListArgs>,
  privilegeList?: Resolver<Maybe<ResolversTypes['TotalTPrivilegeResponse']>, ParentType, ContextType, QueryPrivilegeListArgs>,
  privilegeMembersList?: Resolver<Maybe<ResolversTypes['TotalTPrivilegesMembersRespose']>, ParentType, ContextType, QueryPrivilegeMembersListArgs>,
  serviceById?: Resolver<Maybe<ResolversTypes['TServiceInfo']>, ParentType, ContextType, RequireFields<QueryServiceByIdArgs, '_id'>>,
  serviceList?: Resolver<Maybe<Array<Maybe<ResolversTypes['TServiceInfo']>>>, ParentType, ContextType, QueryServiceListArgs>,
  bookingServiceList?: Resolver<Maybe<Array<Maybe<ResolversTypes['TServiceBookingInfo']>>>, ParentType, ContextType, QueryBookingServiceListArgs>,
  serviceAssetsList?: Resolver<Maybe<ResolversTypes['TotalTServiceMenuAssetsResponse']>, ParentType, ContextType, QueryServiceAssetsListArgs>,
  serviceTypeList?: Resolver<Maybe<ResolversTypes['TotalTServiceTypeResponse']>, ParentType, ContextType, QueryServiceTypeListArgs>,
};

export type TAssetResolvers<ContextType = any, ParentType extends ResolversParentTypes['TAsset'] = ResolversParentTypes['TAsset']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  publicPath?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  path?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  filename?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  mimetype?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  encoding?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type TAssetPeriodResolvers<ContextType = any, ParentType extends ResolversParentTypes['TAssetPeriod'] = ResolversParentTypes['TAssetPeriod']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['ID']>>>, ParentType, ContextType>,
};

export type TAssetPeriodInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TAssetPeriodInfo'] = ResolversParentTypes['TAssetPeriodInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['TAsset']>>>, ParentType, ContextType>,
};

export type TConciergeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TConcierge'] = ResolversParentTypes['TConcierge']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  lastname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  email?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  phone?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  avatar?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  username?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  password?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  type?: Resolver<ResolversTypes['TConciergeType'], ParentType, ContextType>,
  isVerified?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  isBlocked?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type TConciergeInfoResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TConciergeInfoResponse'] = ResolversParentTypes['TConciergeInfoResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  lastname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  phone?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  avatar?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
  username?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  isVerified?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  isBlocked?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  type?: Resolver<Maybe<ResolversTypes['TConciergeType']>, ParentType, ContextType>,
};

export type TEventResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEvent'] = ResolversParentTypes['TEvent']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  location?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['ID']>>>, ParentType, ContextType>,
  capacity?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  bookedCapacity?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  typeId?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  canRegisterBefore?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  canCancelBefore?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  agenda?: Resolver<Maybe<Array<Maybe<ResolversTypes['ID']>>>, ParentType, ContextType>,
  speakers?: Resolver<Maybe<Array<Maybe<ResolversTypes['ID']>>>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['TEventStatusEnum']>, ParentType, ContextType>,
};

export type TEventAgendaItemResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventAgendaItem'] = ResolversParentTypes['TEventAgendaItem']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  startDate?: Resolver<ResolversTypes['Date'], ParentType, ContextType>,
  endDate?: Resolver<ResolversTypes['Date'], ParentType, ContextType>,
  eventId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  speakers?: Resolver<Maybe<Array<Maybe<ResolversTypes['ID']>>>, ParentType, ContextType>,
};

export type TEventAgendaItemResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventAgendaItemResponse'] = ResolversParentTypes['TEventAgendaItemResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  eventId?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  speakers?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventSpeakerInfo']>>>, ParentType, ContextType>,
};

export type TEventFavouritesResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventFavourites'] = ResolversParentTypes['TEventFavourites']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  eventId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  isFavourite?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
};

export type TEventFavouritesResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventFavouritesResponse'] = ResolversParentTypes['TEventFavouritesResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType>,
  eventId?: Resolver<Maybe<ResolversTypes['TEventInfo']>, ParentType, ContextType>,
  isFavourite?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
};

export type TEventInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventInfo'] = ResolversParentTypes['TEventInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  location?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['TAsset']>>>, ParentType, ContextType>,
  capacity?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  bookedCapacity?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  typeId?: Resolver<Maybe<ResolversTypes['TEventTypeResponse']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
  canRegisterBefore?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  canCancelBefore?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  agenda?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventAgendaItemResponse']>>>, ParentType, ContextType>,
  speakers?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventSpeakerInfo']>>>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['TEventStatusEnum']>, ParentType, ContextType>,
};

export type TEventSpeakerResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventSpeaker'] = ResolversParentTypes['TEventSpeaker']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
};

export type TEventSpeakerInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventSpeakerInfo'] = ResolversParentTypes['TEventSpeakerInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
};

export type TEventTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventType'] = ResolversParentTypes['TEventType']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type TEventTypeResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TEventTypeResponse'] = ResolversParentTypes['TEventTypeResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type TMemberResolvers<ContextType = any, ParentType extends ResolversParentTypes['TMember'] = ResolversParentTypes['TMember']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  lastname?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  countryCode?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  phoneNumber?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  isVerified?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  isBlocked?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  twofa?: Resolver<Maybe<ResolversTypes['TwofaConfig']>, ParentType, ContextType>,
  cardInfo?: Resolver<Maybe<ResolversTypes['CardInfo']>, ParentType, ContextType>,
  appSettings?: Resolver<Maybe<ResolversTypes['AppSettings']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  test?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
};

export type TMemberBookingEventInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TMemberBookingEventInfo'] = ResolversParentTypes['TMemberBookingEventInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType>,
  eventId?: Resolver<Maybe<ResolversTypes['TEventInfo']>, ParentType, ContextType>,
  bookingStatus?: Resolver<Maybe<ResolversTypes['TEventBookingStatusEnum']>, ParentType, ContextType>,
};

export type TMemberEventInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TMemberEventInfo'] = ResolversParentTypes['TMemberEventInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  eventId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  bookingStatus?: Resolver<Maybe<ResolversTypes['TEventBookingStatusEnum']>, ParentType, ContextType>,
};

export type TMemberEventInfoResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TMemberEventInfoResponse'] = ResolversParentTypes['TMemberEventInfoResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  location?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  capacity?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  bookedCapacity?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  typeId?: Resolver<Maybe<ResolversTypes['TEventTypeResponse']>, ParentType, ContextType>,
  canRegisterBefore?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  canCancelBefore?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  agenda?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventAgendaItemResponse']>>>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['TEventStatusEnum']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
  speakers?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventSpeakerInfo']>>>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['TAsset']>>>, ParentType, ContextType>,
  isFavourite?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
  info?: Resolver<Maybe<ResolversTypes['TMemberEventInfo']>, ParentType, ContextType>,
};

export type TMemberEventResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TMemberEventResponse'] = ResolversParentTypes['TMemberEventResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType>,
  eventId?: Resolver<Maybe<ResolversTypes['TEventInfo']>, ParentType, ContextType>,
  bookingStatus?: Resolver<Maybe<ResolversTypes['TEventBookingStatusEnum']>, ParentType, ContextType>,
};

export type TMemberInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TMemberInfo'] = ResolversParentTypes['TMemberInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  lastname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  phoneNumber?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
  twofa?: Resolver<Maybe<ResolversTypes['TwofaConfig']>, ParentType, ContextType>,
  cardInfo?: Resolver<Maybe<ResolversTypes['CardInfo']>, ParentType, ContextType>,
  appSettings?: Resolver<Maybe<ResolversTypes['AppSettings']>, ParentType, ContextType>,
  isVerified?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  isBlocked?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  test?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
};

export type TotalTConciergeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTConcierge'] = ResolversParentTypes['TotalTConcierge']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TConciergeInfoResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTEventAgendaItemResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTEventAgendaItemResponse'] = ResolversParentTypes['TotalTEventAgendaItemResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventAgendaItemResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTEventFavouritesResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTEventFavouritesResponse'] = ResolversParentTypes['TotalTEventFavouritesResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventFavouritesResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTEventInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTEventInfo'] = ResolversParentTypes['TotalTEventInfo']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventInfo']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTEventSpeakerInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTEventSpeakerInfo'] = ResolversParentTypes['TotalTEventSpeakerInfo']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TEventSpeakerInfo']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTMemberEventInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTMemberEventInfo'] = ResolversParentTypes['TotalTMemberEventInfo']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TMemberEventResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTMemberEventInfoResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTMemberEventInfoResponse'] = ResolversParentTypes['TotalTMemberEventInfoResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TMemberEventResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTMemberInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTMemberInfo'] = ResolversParentTypes['TotalTMemberInfo']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TMemberInfo']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTPartnersResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTPartnersResponse'] = ResolversParentTypes['TotalTPartnersResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TPartnersResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTPrivilegeBookingResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTPrivilegeBookingResponse'] = ResolversParentTypes['TotalTPrivilegeBookingResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TPrivilegeBookingResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTPrivilegeMembersResposeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTPrivilegeMembersRespose'] = ResolversParentTypes['TotalTPrivilegeMembersRespose']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TPrivilegeMembersRespose']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTPrivilegeResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTPrivilegeResponse'] = ResolversParentTypes['TotalTPrivilegeResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TPrivilegeResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTPrivilegesMembersResposeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTPrivilegesMembersRespose'] = ResolversParentTypes['TotalTPrivilegesMembersRespose']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TPrivilegeMembersRespose']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTServiceBookingInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTServiceBookingInfo'] = ResolversParentTypes['TotalTServiceBookingInfo']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TServiceBookingInfo']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTServiceMenuAssetsResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTServiceMenuAssetsResponse'] = ResolversParentTypes['TotalTServiceMenuAssetsResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TServiceMenuAssetsResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TotalTServiceTypeResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TotalTServiceTypeResponse'] = ResolversParentTypes['TotalTServiceTypeResponse']> = {
  data?: Resolver<Maybe<Array<Maybe<ResolversTypes['TServiceTypeResponse']>>>, ParentType, ContextType>,
  pageInfo?: Resolver<Maybe<ResolversTypes['Pageinfo']>, ParentType, ContextType>,
};

export type TPartnersResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPartners'] = ResolversParentTypes['TPartners']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  lastname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  email?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  phone?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  avatar?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  site_link?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type TPartnersResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPartnersResponse'] = ResolversParentTypes['TPartnersResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  firstname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  lastname?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  email?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  phone?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  avatar?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  site_link?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type TPrivilegeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilege'] = ResolversParentTypes['TPrivilege']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  status?: Resolver<ResolversTypes['TPrivilegeStatus'], ParentType, ContextType>,
  start?: Resolver<ResolversTypes['Date'], ParentType, ContextType>,
  end?: Resolver<ResolversTypes['Date'], ParentType, ContextType>,
  rule?: Resolver<ResolversTypes['TPrivilegePeriodRule'], ParentType, ContextType>,
  partner?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
};

export type TPrivilegeBookingResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilegeBooking'] = ResolversParentTypes['TPrivilegeBooking']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  privilegeId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  start?: Resolver<ResolversTypes['Date'], ParentType, ContextType>,
  end?: Resolver<ResolversTypes['Date'], ParentType, ContextType>,
};

export type TPrivilegeBookingResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilegeBookingResponse'] = ResolversParentTypes['TPrivilegeBookingResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType>,
  privilegeId?: Resolver<Maybe<ResolversTypes['TPrivilegeResponse']>, ParentType, ContextType>,
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type TPrivilegeDetailsResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilegeDetailsResponse'] = ResolversParentTypes['TPrivilegeDetailsResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  rule?: Resolver<Maybe<ResolversTypes['TPrivilegePeriodRule']>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['TPrivilegeStatus']>, ParentType, ContextType>,
  partner?: Resolver<Maybe<ResolversTypes['TPartnersResponse']>, ParentType, ContextType>,
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
  last_activation?: Resolver<Maybe<ResolversTypes['TPrivilegeBookingResponse']>, ParentType, ContextType>,
  last_activations?: Resolver<Maybe<Array<Maybe<ResolversTypes['TPrivilegeBookingResponse']>>>, ParentType, ContextType>,
  all_activations?: Resolver<Maybe<Array<Maybe<ResolversTypes['TPrivilegeBookingResponse']>>>, ParentType, ContextType>,
  used?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
  remain?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
};

export type TPrivilegeMembersResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilegeMembers'] = ResolversParentTypes['TPrivilegeMembers']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  privilegeId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
};

export type TPrivilegeMembersResposeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilegeMembersRespose'] = ResolversParentTypes['TPrivilegeMembersRespose']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  memberId?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType>,
  privilegeId?: Resolver<Maybe<ResolversTypes['TPrivilegeResponse']>, ParentType, ContextType>,
};

export type TPrivilegePeriodRuleResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilegePeriodRule'] = ResolversParentTypes['TPrivilegePeriodRule']> = {
  repeat?: Resolver<Maybe<ResolversTypes['TPrivilegePeriodRuleRepeat']>, ParentType, ContextType>,
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
};

export type TPrivilegeResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TPrivilegeResponse'] = ResolversParentTypes['TPrivilegeResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  rule?: Resolver<Maybe<ResolversTypes['TPrivilegePeriodRule']>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['TPrivilegeStatus']>, ParentType, ContextType>,
  partner?: Resolver<Maybe<ResolversTypes['TPartnersResponse']>, ParentType, ContextType>,
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
};

export type TServiceResolvers<ContextType = any, ParentType extends ResolversParentTypes['TService'] = ResolversParentTypes['TService']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  subtitle1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  subtitle2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['ID']>>>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  typeId?: Resolver<ResolversTypes['ID'], ParentType, ContextType>,
  price?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
};

export type TServiceBookingResolvers<ContextType = any, ParentType extends ResolversParentTypes['TServiceBooking'] = ResolversParentTypes['TServiceBooking']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['TServiceBookingStatusEnum']>, ParentType, ContextType>,
  memberId?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  serviceId?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
};

export type TServiceBookingInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TServiceBookingInfo'] = ResolversParentTypes['TServiceBookingInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  startDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  endDate?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  status?: Resolver<Maybe<ResolversTypes['TServiceBookingStatusEnum']>, ParentType, ContextType>,
  memberId?: Resolver<Maybe<ResolversTypes['TMemberInfo']>, ParentType, ContextType>,
  serviceId?: Resolver<Maybe<ResolversTypes['TServiceInfo']>, ParentType, ContextType>,
};

export type TServiceInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['TServiceInfo'] = ResolversParentTypes['TServiceInfo']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  subtitle1?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  subtitle2?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['TServiceMenuAssetsResponse']>>>, ParentType, ContextType>,
  preview?: Resolver<Maybe<ResolversTypes['TAsset']>, ParentType, ContextType>,
  typeId?: Resolver<Maybe<ResolversTypes['TServiceTypeResponse']>, ParentType, ContextType>,
  price?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>,
};

export type TServiceMenuAssetsResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TServiceMenuAssetsResponse'] = ResolversParentTypes['TServiceMenuAssetsResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  assets?: Resolver<Maybe<Array<Maybe<ResolversTypes['TAsset']>>>, ParentType, ContextType>,
  start?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  end?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
};

export type TServiceTypeResolvers<ContextType = any, ParentType extends ResolversParentTypes['TServiceType'] = ResolversParentTypes['TServiceType']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type TServiceTypeResponseResolvers<ContextType = any, ParentType extends ResolversParentTypes['TServiceTypeResponse'] = ResolversParentTypes['TServiceTypeResponse']> = {
  _id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>,
  uuid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  updatedBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdBy?: Resolver<Maybe<ResolversTypes['TConciergeInfoResponse']>, ParentType, ContextType>,
  createdAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  updatedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  deletedAt?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  description?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  title?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
};

export type TwofaConfigResolvers<ContextType = any, ParentType extends ResolversParentTypes['TwofaConfig'] = ResolversParentTypes['TwofaConfig']> = {
  authyid?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  enabled?: Resolver<Maybe<ResolversTypes['Boolean']>, ParentType, ContextType>,
  countryCode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  phoneNumber?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  backupKeys?: Resolver<Maybe<Array<Maybe<ResolversTypes['BackupKeyType']>>>, ParentType, ContextType>,
  backupTimeStamp?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  code?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>,
  codeTime?: Resolver<Maybe<ResolversTypes['Date']>, ParentType, ContextType>,
  sendMessagesTimes?: Resolver<Maybe<Array<Maybe<ResolversTypes['Date']>>>, ParentType, ContextType>,
};

export interface UploadScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Upload'], any> {
  name: 'Upload'
}

export type Resolvers<ContextType = any> = {
  AppSettings?: AppSettingsResolvers<ContextType>,
  AuthInfo?: AuthInfoResolvers<ContextType>,
  AuthToken?: AuthTokenResolvers<ContextType>,
  BackupKeyType?: BackupKeyTypeResolvers<ContextType>,
  Base?: BaseResolvers<ContextType>,
  BaseAdminResponse?: BaseAdminResponseResolvers<ContextType>,
  BaseAdminSchema?: BaseAdminSchemaResolvers<ContextType>,
  BaseModel?: BaseModelResolvers<ContextType>,
  BaseModelSchema?: BaseModelSchemaResolvers<ContextType>,
  CardInfo?: CardInfoResolvers<ContextType>,
  Date?: GraphQLScalarType,
  DateRange?: DateRangeResolvers<ContextType>,
  MediaAsset?: MediaAssetResolvers<ContextType>,
  Mutation?: MutationResolvers<ContextType>,
  OcupationDateService?: OcupationDateServiceResolvers<ContextType>,
  OperationStatus?: OperationStatusResolvers<ContextType>,
  Owner?: OwnerResolvers<ContextType>,
  Pageinfo?: PageinfoResolvers<ContextType>,
  Query?: QueryResolvers<ContextType>,
  TAsset?: TAssetResolvers<ContextType>,
  TAssetPeriod?: TAssetPeriodResolvers<ContextType>,
  TAssetPeriodInfo?: TAssetPeriodInfoResolvers<ContextType>,
  TConcierge?: TConciergeResolvers<ContextType>,
  TConciergeInfoResponse?: TConciergeInfoResponseResolvers<ContextType>,
  TEvent?: TEventResolvers<ContextType>,
  TEventAgendaItem?: TEventAgendaItemResolvers<ContextType>,
  TEventAgendaItemResponse?: TEventAgendaItemResponseResolvers<ContextType>,
  TEventFavourites?: TEventFavouritesResolvers<ContextType>,
  TEventFavouritesResponse?: TEventFavouritesResponseResolvers<ContextType>,
  TEventInfo?: TEventInfoResolvers<ContextType>,
  TEventSpeaker?: TEventSpeakerResolvers<ContextType>,
  TEventSpeakerInfo?: TEventSpeakerInfoResolvers<ContextType>,
  TEventType?: TEventTypeResolvers<ContextType>,
  TEventTypeResponse?: TEventTypeResponseResolvers<ContextType>,
  TMember?: TMemberResolvers<ContextType>,
  TMemberBookingEventInfo?: TMemberBookingEventInfoResolvers<ContextType>,
  TMemberEventInfo?: TMemberEventInfoResolvers<ContextType>,
  TMemberEventInfoResponse?: TMemberEventInfoResponseResolvers<ContextType>,
  TMemberEventResponse?: TMemberEventResponseResolvers<ContextType>,
  TMemberInfo?: TMemberInfoResolvers<ContextType>,
  TotalTConcierge?: TotalTConciergeResolvers<ContextType>,
  TotalTEventAgendaItemResponse?: TotalTEventAgendaItemResponseResolvers<ContextType>,
  TotalTEventFavouritesResponse?: TotalTEventFavouritesResponseResolvers<ContextType>,
  TotalTEventInfo?: TotalTEventInfoResolvers<ContextType>,
  TotalTEventSpeakerInfo?: TotalTEventSpeakerInfoResolvers<ContextType>,
  TotalTMemberEventInfo?: TotalTMemberEventInfoResolvers<ContextType>,
  TotalTMemberEventInfoResponse?: TotalTMemberEventInfoResponseResolvers<ContextType>,
  TotalTMemberInfo?: TotalTMemberInfoResolvers<ContextType>,
  TotalTPartnersResponse?: TotalTPartnersResponseResolvers<ContextType>,
  TotalTPrivilegeBookingResponse?: TotalTPrivilegeBookingResponseResolvers<ContextType>,
  TotalTPrivilegeMembersRespose?: TotalTPrivilegeMembersResposeResolvers<ContextType>,
  TotalTPrivilegeResponse?: TotalTPrivilegeResponseResolvers<ContextType>,
  TotalTPrivilegesMembersRespose?: TotalTPrivilegesMembersResposeResolvers<ContextType>,
  TotalTServiceBookingInfo?: TotalTServiceBookingInfoResolvers<ContextType>,
  TotalTServiceMenuAssetsResponse?: TotalTServiceMenuAssetsResponseResolvers<ContextType>,
  TotalTServiceTypeResponse?: TotalTServiceTypeResponseResolvers<ContextType>,
  TPartners?: TPartnersResolvers<ContextType>,
  TPartnersResponse?: TPartnersResponseResolvers<ContextType>,
  TPrivilege?: TPrivilegeResolvers<ContextType>,
  TPrivilegeBooking?: TPrivilegeBookingResolvers<ContextType>,
  TPrivilegeBookingResponse?: TPrivilegeBookingResponseResolvers<ContextType>,
  TPrivilegeDetailsResponse?: TPrivilegeDetailsResponseResolvers<ContextType>,
  TPrivilegeMembers?: TPrivilegeMembersResolvers<ContextType>,
  TPrivilegeMembersRespose?: TPrivilegeMembersResposeResolvers<ContextType>,
  TPrivilegePeriodRule?: TPrivilegePeriodRuleResolvers<ContextType>,
  TPrivilegeResponse?: TPrivilegeResponseResolvers<ContextType>,
  TService?: TServiceResolvers<ContextType>,
  TServiceBooking?: TServiceBookingResolvers<ContextType>,
  TServiceBookingInfo?: TServiceBookingInfoResolvers<ContextType>,
  TServiceInfo?: TServiceInfoResolvers<ContextType>,
  TServiceMenuAssetsResponse?: TServiceMenuAssetsResponseResolvers<ContextType>,
  TServiceType?: TServiceTypeResolvers<ContextType>,
  TServiceTypeResponse?: TServiceTypeResponseResolvers<ContextType>,
  TwofaConfig?: TwofaConfigResolvers<ContextType>,
  Upload?: GraphQLScalarType,
};


/**
 * @deprecated
 * Use "Resolvers" root object instead. If you wish to get "IResolvers", add "typesPrefix: I" to your config.
*/
export type IResolvers<ContextType = any> = Resolvers<ContextType>;
export type DirectiveResolvers<ContextType = any> = {
  ObjectId?: ObjectIdDirectiveResolver<any, any, ContextType>,
  namespace?: NamespaceDirectiveResolver<any, any, ContextType>,
  collection?: CollectionDirectiveResolver<any, any, ContextType>,
  version?: VersionDirectiveResolver<any, any, ContextType>,
  ignore?: IgnoreDirectiveResolver<any, any, ContextType>,
  default?: DefaultDirectiveResolver<any, any, ContextType>,
  required?: RequiredDirectiveResolver<any, any, ContextType>,
  unique?: UniqueDirectiveResolver<any, any, ContextType>,
  setonce?: SetonceDirectiveResolver<any, any, ContextType>,
  uuid?: UuidDirectiveResolver<any, any, ContextType>,
  optional?: OptionalDirectiveResolver<any, any, ContextType>,
  min?: MinDirectiveResolver<any, any, ContextType>,
  max?: MaxDirectiveResolver<any, any, ContextType>,
  minlength?: MinlengthDirectiveResolver<any, any, ContextType>,
  maxlength?: MaxlengthDirectiveResolver<any, any, ContextType>,
  trim?: TrimDirectiveResolver<any, any, ContextType>,
  uppercase?: UppercaseDirectiveResolver<any, any, ContextType>,
  lowercase?: LowercaseDirectiveResolver<any, any, ContextType>,
  hash?: HashDirectiveResolver<any, any, ContextType>,
  extends?: ExtendsDirectiveResolver<any, any, ContextType>,
  mongo?: MongoDirectiveResolver<any, any, ContextType>,
  fragment?: FragmentDirectiveResolver<any, any, ContextType>,
};


/**
* @deprecated
* Use "DirectiveResolvers" root object instead. If you wish to get "IDirectiveResolvers", add "typesPrefix: I" to your config.
*/
export type IDirectiveResolvers<ContextType = any> = DirectiveResolvers<ContextType>;