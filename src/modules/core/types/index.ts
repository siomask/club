import { Request, Response } from 'express';
export * from './genTypes';

export interface ISession {
	req: Request;
	res: Response;
}
