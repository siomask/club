import fs from 'fs';

const { createWriteStream, unlink } = require('fs');
const UPLOAD_DIR_PUBLIC = 'uploads';
const UPLOAD_DIR = `./resources/${UPLOAD_DIR_PUBLIC}`;
const REMOTE_HOST = `${process.env.REMOTE_HOST}:${process.env.PORT}`;
const mkdirp = require('mkdirp');
const shortid = require('shortid');

const removeUpload = async (upload) => {
	fs.unlink(`${UPLOAD_DIR}/${upload.filename}`, (err) => {
		if (err) {
			console.log(err);
		} else {
			console.log(upload.filename + ' was removed');
		}
	});
};
const storeUpload = async (upload) => {
	const { createReadStream, filename, mimetype, encoding } = await upload;
	const stream = createReadStream();
	const id = shortid.generate();
	const fileExt = filename.split('.')[filename.split('.').length - 1];
	const newFileName = `${Date.now()}${id}.${fileExt}`;
	const path = `${UPLOAD_DIR}/${newFileName}`;
	const file = { id, filename: newFileName, mimetype, path, encoding };
	await new Promise((resolve, reject) => {
		const writeStream = createWriteStream(path);
		writeStream.on('finish', resolve);

		writeStream.on('error', (error) => {
			unlink(path, () => {
				reject(error);
			});
		});
		stream.on('error', (error) => writeStream.destroy(error));
		stream.pipe(writeStream);
	});

	return file;
};

mkdirp.sync(UPLOAD_DIR);

export { storeUpload, removeUpload };
