import {
	NamespaceEntityProvider,
	EntityEnum,
	MailProvider,
	TEventBookingStatusEnum,
	TwofaServiceProvider,
	AuthyResponseType,
	NOTIFICATINOS,
	FirebaseProvider
} from '../';
import { MutationResolvers, TEvent, TEventStatusEnum } from '../';
import { ApolloError } from 'apollo-server-core';
import { POPULATE } from './populates';

async function addMemberToEvent(
	{ eventId, memberId, status = TEventBookingStatusEnum.MemberRequested },
	{ injector, token }: any
) {
	const mailProvider = injector.get(MailProvider);
	const firebaseProvider = injector.get(FirebaseProvider);
	const TEventModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
	const TEventBookingModel = injector
		.get(NamespaceEntityProvider)
		.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);

	// ** registration is open and event is public one
	const tEvent = await TEventModel.findById(eventId);

	// tslint:disable-next-line:curly
	if (!tEvent) {
		throw new ApolloError(`No such event with id ${eventId}`);
	}
	if (
		!tEvent.capacity ||
		(await TEventBookingModel.count({
			eventId: tEvent._id,
			bookingStatus: {
				$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
			}
		})) >= tEvent.capacity
	) {
		throw new ApolloError(`No more avaialable places`);
	}

	const tEventData: TEvent = tEvent.toObject() as TEvent;

	// tslint:disable-next-line:curly
	if (tEventData.status !== TEventStatusEnum.Open) {
		throw new ApolloError(`Can't book a private event`);
	}

	if (Date.now() > new Date(tEventData.canRegisterBefore).getTime()) {
		throw new ApolloError(`This event not allow to register anymore`);
	}

	const query = {
		memberId,
		eventId: tEvent._id
	};

	let tEventBooking = await TEventBookingModel.findOne({
		...query,
		bookingStatus: {
			$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
		}
	});

	// tslint:disable-next-line:curly
	if (!tEventBooking) {
		tEventBooking = await TEventBookingModel.create({
			...query,
			bookingStatus: status
		});
	} else {
		throw new ApolloError(`Already have active booking event`);
	}

	const eventBokk = (
		await TEventBookingModel.findById(tEventBooking._id).populate(POPULATE.EVENT_BOOK(injector))
	).toObject();
	firebaseProvider.notifyUsers(NOTIFICATINOS.CLIENT_BOOK_EVENT({ object: tEvent }), [memberId]);
	mailProvider.sendBookEventEmail(eventBokk);
	return { success: true };
}
async function getServiceBooking(TServiceBoolinkModel, injector, item, find = null, opt = null) {
	let list;
	if (find) {
		console.log(find);
		list = TServiceBoolinkModel.findActive(find);
		if (opt) {
			const { limit, skip, sort }: any = opt;
			list = list
				.sort(sort)
				.limit(limit)
				.skip(skip);
		}
	} else {
		list = TServiceBoolinkModel.findById(item._id);
	}
	let res = await list.populate(POPULATE.SERVICE_BOOKINGS(injector));
	if (!find) {
		return res.toObject();
	} else {
		return res.map((el) => el.toObject());
	}
}

async function getService(ITemModel, injector, item = null, opt = null, query = {}) {
	let list;
	if (item) {
		list = ITemModel.findOne({ _id: item._id });
	} else {
		list = ITemModel.findActive({ ...query });
		if (opt) {
			const { limit, skip, sort }: any = opt;
			list = list
				.sort(sort)
				.limit(limit)
				.skip(skip);
		}
	}
	let res = await list.populate(POPULATE.SERVICE(injector));

	if (item) {
		return res.toObject();
	} else {
		return res.map((el) => el.toObject());
	}
}

const QUERY: any = {
	LIMIT: 10,
	SORT: {
		['createdAt']: -1
	},
	SKIP: 0
};
export { getServiceBooking, getService, POPULATE, QUERY, addMemberToEvent };
