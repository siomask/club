import { NamespaceEntityProvider, EntityEnum } from '../';
const NOT_DELETED = { deletedAt: { $exists: false } };
const POPULATE: any = {
	EVENT_BOOK: (injector) => [
		{
			path: 'memberId',
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER),
			match: NOT_DELETED,
			populate: POPULATE.MEMBER(injector)
		},
		{
			path: 'eventId',
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT),
			match: NOT_DELETED,
			populate: POPULATE.EVENTS(injector)
		}
	],
	EVENT_FAVOURITE: (injector) => [
		{
			path: 'memberId',
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER),
			match: NOT_DELETED,
			populate: POPULATE.MEMBER(injector)
		},
		{
			path: 'eventId',
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT),
			match: NOT_DELETED,
			populate: POPULATE.EVENTS(injector)
		}
	],
	EVENTS: (injector) => [
		{
			path: 'createdBy',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE)
		},
		{
			path: 'assets',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		},
		{
			path: 'typeId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_TYPE)
		},
		{
			path: 'preview',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		},
		{
			path: 'speakers',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_SPEAKER),
			populate: POPULATE.EVENT_SPEAKER(injector)
		},
		{
			path: 'agenda',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_AGENDA),
			populate: POPULATE.EVENT_AGENDA(injector)
		}
	],
	EVENT_AGENDA: (injector) => [
		{
			path: 'speakers',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_SPEAKER),
			populate: POPULATE.EVENT_SPEAKER(injector)
		}
	],
	EVENT_SPEAKER: (injector) => [
		{
			path: 'preview',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		}
	],
	SERVICE: (injector) => [
		{
			path: 'createdBy',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE)
		},
		{
			path: 'preview',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		},
		{
			path: 'typeId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_TYPE)
		},
		{
			path: 'assets',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSET_PERIOD),
			populate: {
				path: 'assets',
				match: NOT_DELETED,
				model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
			}
		}
	],
	SERVICE_BOOKINGS: (injector) => [
		{
			path: 'memberId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER)
		},
		{
			path: 'serviceId',
			match: NOT_DELETED,
			populate: POPULATE.SERVICE(injector),
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE)
		}
	],
	MEMBER: (injector) => [
		{
			path: 'createdBy',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE)
		},
		{
			path: 'preview',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		}
	],
	SPEAKER: (injector) => [
		{
			path: 'createdBy',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE)
		},
		{
			path: 'preview',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		}
	],
	CONCIERGE: (injector) => [
		{
			path: 'createdBy',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE)
		},
		{
			path: 'avatar',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		}
	],
	PARTNER: (injector) => [
		{
			path: 'createdBy',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE)
		},
		{
			path: 'avatar',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		}
	],
	BOOK_PRIVILEGE: (injector) => [
		{
			path: 'memberId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER),
			populate: POPULATE.MEMBER(injector)
		},
		{
			path: 'privilegeId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE),
			populate: POPULATE.PRIVILEGE(injector)
		}
	],
	PRIVILEGE: (injector) => [
		{
			path: 'preview',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		},
		// {
		// 	path: 'assets',
		// 	model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
		// },
		{
			path: 'createdBy',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONCIERGE)
		},
		{
			path: 'partner',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PARTNER),
			populate: POPULATE.PARTNER(injector)
		},
		{
			path: 'contacts',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_CONTACT),
			populate: [
				{
					path: 'preview',
					match: NOT_DELETED,
					model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_ASSETS)
				}
			]
		}
	],

	PRIVILEGE_MEMBER: (injector) => [
		{
			path: 'memberId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER),
			populate: POPULATE.MEMBER(injector)
		},
		{
			path: 'privilegeId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE),
			populate: POPULATE.PRIVILEGE(injector)
		}
	],
	BOOKING_PRIVILEGE: (injector) => [
		{
			path: 'memberId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER),
			populate: POPULATE.MEMBER(injector)
		},
		{
			path: 'privilegeId',
			match: NOT_DELETED,
			model: injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE),
			populate: POPULATE.PRIVILEGE(injector)
		}
	]
};
export { POPULATE };
