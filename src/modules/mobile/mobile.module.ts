import path from 'path';
import { GraphQLModule } from '@graphql-modules/core';
import { loadSchemaFiles, loadResolversFiles } from 'graphql-toolkit';

import { GraphEntityParser } from '@sfast/gql-schema-parser';

import { createCoreModule, SchemaProvider, ISession, TokenProvider, TwofaServiceProvider } from '@tclub/core';

import { MobileDBInitProvider, MobileInitProvider } from './providers';
import gql from 'graphql-tag';
import { isAuthenticated, validCardNumber, isAuthorized, parseFilter } from './resolvers/middlewares';

export interface IMobileModuleContext {
	version: string;
	token?: string;
	memberId?: string;
	tMemberModel?: any;
}

let gm: GraphQLModule = null;

const AUTHORIZATION_HEADER_NAME = 'authorization';

export const createMobileModule = (): GraphQLModule => {
	if (gm) return gm;

	gm = new GraphQLModule<{}, {}, IMobileModuleContext>({
		imports: [createCoreModule()],
		typeDefs: ({ injector }) => {
			// ** NOTE:: using the final Graphql generated
			const schemaPath: string = path.resolve(__dirname, './schema/');

			const mobileModuleTypeDefs: String[] = loadSchemaFiles(schemaPath);
			const baseTypeDefs: String = injector.get(SchemaProvider).typeDefs;

			const typeDef = [baseTypeDefs, ...mobileModuleTypeDefs].join('\n');
			const graphNodeGQLed = gql(typeDef);
			const processedWithGql = new GraphEntityParser(graphNodeGQLed);
			const finalSchema = processedWithGql.getSchema(graphNodeGQLed);
			return processedWithGql.printSchema(finalSchema);
		},
		resolvers: loadResolversFiles(path.resolve(__dirname, './resolvers/')),
		context: async (session: ISession, currentContext, { injector }) => {
			console.log('Mobile module context creation');

			const token = session.req.headers[AUTHORIZATION_HEADER_NAME];
			return {
				version: '0.0.1',
				token
			};
		},
		providers: [TokenProvider, TwofaServiceProvider, MobileDBInitProvider, MobileInitProvider],
		resolversComposition: {
			'Mutation.authenticateWithCardIdentity': [validCardNumber()],
			'Mutation.authorizeWithSMS': [isAuthenticated()],
			'Mutation.changePublicMode': [isAuthorized()],
			'Mutation.addEventToMyFavourites': [isAuthorized()],
			'Mutation.removeEventFromMyFavourites': [isAuthorized()],
			'Mutation.requestEventBooking': [isAuthorized()],
			'Mutation.cancelEventBooking': [isAuthorized()],
			'Mutation.bookService': [isAuthorized()],
			'Mutation.cancelBookService': [isAuthorized()],
			'Mutation.togglePushNotify': [isAuthorized()],
			'Mutation.activatePrivilege': [isAuthorized()],
			'Query.chatSearch': [isAuthorized()],
			'Query.chatMemberById': [isAuthorized()],
			'Query.me': [isAuthorized()],
			'Query.myEvents': [isAuthorized(), parseFilter()],
			'Query.myBookedServiceList': [isAuthorized(), parseFilter()],
			'Query.privilegeId': [isAuthorized()],
			'Query.privilegeList': [isAuthorized(), parseFilter()],
			'Query.myPrivilegeActvatedList': [isAuthorized(), parseFilter()],
			'Query.eventsList': [isAuthorized(), parseFilter()],
			'Query.eventInfo': [isAuthorized()]
		}
	});

	return gm;
};
