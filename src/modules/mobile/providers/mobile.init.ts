import { Injectable, ProviderScope, Inject } from '@graphql-modules/di';
import { ASyncInitProvider, TwofaServiceProvider } from '@tclub/core';

import { MobileDBInitProvider } from './mobile.db.init';

@Injectable({
	scope: ProviderScope.Application
})
export class MobileInitProvider extends ASyncInitProvider {
	constructor(
		@Inject(MobileDBInitProvider) private dbProvider: MobileDBInitProvider,
		@Inject(TwofaServiceProvider) private twofaProvider: TwofaServiceProvider
	) {
		super('MobileInitProvider');
	}

	public async initializer() {
		await this.twofaProvider.onInit();
		await this.dbProvider.onInit();

		/* Your code goes here */
		console.log('MobileInitProvider onInit - END');
	}
}
