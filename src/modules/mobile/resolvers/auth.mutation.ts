import { ModuleContext } from '@graphql-modules/core';
import { filter, map } from 'lodash';
import { ApolloError } from 'apollo-server-core';

import {
	TwofaServiceProvider,
	DefaultEnvProvider,
	TokenProvider,
	NamespaceEntityProvider,
	EntityEnum,
	AuthyResponseType,
	TokenTypeEnum
} from '@tclub/core';

import { MutationResolvers, TMember } from '@tclub/types';

import { IMobileModuleContext } from '../mobile.module';

// TODO::AVAR - need to be moved to ansible vars
const TWILLIO_SMS_HOURLY_LIMIT = process.env.NODE_ENV === 'dev' ? 200 : 20;

const useBackupKey = async (memberModel: any, backupKey: string): Promise<any> => {
	if (!memberModel.twofa.enabled) throw new ApolloError('Should be enabled, 400');

	const hasBackupKey = filter(memberModel.twofa.backupKeys, (elem) => {
		const { code, isUsed } = elem;
		if (code === backupKey && !isUsed) elem.isUsed = true;
		return true;
	});

	if (!hasBackupKey) throw new ApolloError('No unused backup keys available: 403');

	await memberModel.save();
	return true;
};

const TESTUSER_SMS_CODE = '123456';
const TEST_USER_AUTHY_ID = '123456';
const isProd = true; //!!process.env.NODE_ENV.match('prod');

export const Mutation: any = {
	authenticateWithCardIdentity: async (root, args, opt: any) => {
		const { injector, token } = opt;
		const { tMemberModel } = injector;
		const envProvider = injector.get(DefaultEnvProvider);

		const memberId = tMemberModel._id.toString();
		const TMemberModel = injector.get(NamespaceEntityProvider).getEntityModel('tclub', EntityEnum.T_MEMBER);
		const tMember: TMember = tMemberModel.toObject();

		// ** reading phone details added by admin
		const { phoneNumber, countryCode, twofa, isVerified } = tMember;

		const twofaService = injector.get(TwofaServiceProvider);
		const tokenProvider = injector.get(TokenProvider);

		let code = '';
		let generatedToken: string;
		let useMessage: boolean = isProd && !tMember.test;

		// ** is 2fa enabled ? or registration completed is same thing in our app
		if (!twofa.enabled || !isVerified) {
			console.log('AVAR::authenticateWithCardIdentity::1 - first time, not verified user');
			code = useMessage ? await twofaService.verificationSms(phoneNumber, countryCode) : TESTUSER_SMS_CODE;

			const twofaData: Record<string, any> = { sendMessagesTimes: [] };
			twofaData.code = code;
			twofaData.codeTime = Date.now();
			twofaData.sendMessagesTimes.push(Date.now());

			console.log('AVAR::authenticateWithCardIdentity::2 twofaData = ', twofaData);
			await TMemberModel.findByIdAndUpdate(memberId, { twofa: { ...twofa, ...twofaData } });

			// ** return authentication token
			generatedToken = tokenProvider.generatAuthenticationToken(memberId);
		} else {
			twofa.sendMessagesTimes = filter(twofa.sendMessagesTimes || [], (sendTime) => {
				return sendTime > Date.now() - 60 * 60 * 1000;
			});

			if (twofa.sendMessagesTimes.length >= TWILLIO_SMS_HOURLY_LIMIT)
				throw new ApolloError('Too many messages in last hour');

			// TODO::AVAR - maybe store all for
			// if (!twofa.phoneNumber) throw new ApolloError('Phone number is missing, contact TClub admin');
			const sendCodeResponse = useMessage
				? ((await twofaService.sendCode(tMember.twofa.authyid)) as AuthyResponseType)
				: {
						success: true,
						message: 'NO ERROR FOR TEST USER'
				  };

			if (!sendCodeResponse.success) throw new ApolloError(sendCodeResponse.message);

			tMemberModel.twofa.codeTime = Date.now();
			tMemberModel.twofa.sendMessagesTimes.push(Date.now());
			await tMemberModel.save();

			generatedToken = tokenProvider.generatAuthorizationToken(memberId);
		}

		const result = {
			token: generatedToken,
			code
		};

		// tslint:disable-next-line:curly
		if (envProvider.NODE_ENV !== 'dev') {
			delete result.code;
		}

		return result;
	},

	// ** from the token we can get the user auth identity
	authorizeWithSMS: async (root, args, { injector, token }: any) => {
		const { TWOFA_CODE_EXPIRE_TIME } = injector.get(DefaultEnvProvider);
		const tokenProvider = injector.get(TokenProvider);

		const tMember = injector.tMember.toObject();
		const tMemberModel = injector.tMember;
		const memberId = tMember._id;
		const { twofa, isVerified } = tMember;

		let useMessage: boolean = isProd && !tMember.test;
		const twofaService = injector.get(TwofaServiceProvider);
		if (!isVerified || !twofa.enabled) {
			const { phoneNumber, countryCode } = tMember;

			if (twofa.codeTime + TWOFA_CODE_EXPIRE_TIME > Date.now()) throw new ApolloError('Expired code.');

			const checkInitialCodeMatchFlag = useMessage ? twofa.code === args.code : true;
			if (!checkInitialCodeMatchFlag) throw new ApolloError('Wrong verification code.');

			const registerUserWithTwilloResponse = useMessage
				? ((await twofaService.registerUser(
						`${phoneNumber}@t.club`,
						phoneNumber,
						countryCode
				  )) as AuthyResponseType)
				: {
						success: true,
						message: 'NO ERROR FOR TEST USER',
						user: {
							id: TEST_USER_AUTHY_ID
						}
				  };

			if (!registerUserWithTwilloResponse.success) throw new ApolloError(registerUserWithTwilloResponse.message);

			tMemberModel.twofa.authyid = registerUserWithTwilloResponse.user.id;
			tMemberModel.isVerified = Date.now();

			tMemberModel.twofa.backupKeys = twofaService.generateBackupKeys();
			tMemberModel.twofa.backupTimeStamp = Date.now();
			tMemberModel.twofa.enabled = true;
			tMemberModel.twofa.code = null;
			tMemberModel.twofa.codeTime = null;
			await tMemberModel.save();
		} else {
			// ** this is when the user is a verified user and thats why this is login flow with code validation on Twillio side
			if (!tMember.twofa.enabled) throw new ApolloError('Should be enabled, 400');

			if (tMember.twofa.codeTime + TWOFA_CODE_EXPIRE_TIME > Date.now()) throw new ApolloError('Expired code.');

			const verifyCodeResponse = useMessage
				? ((await twofaService.verifyCode(tMember.twofa.authyid, args.code)) as AuthyResponseType)
				: {
						success: true,
						message: 'NO ERROR FOR TEST USER'
				  };

			if (!verifyCodeResponse.success) throw new ApolloError(verifyCodeResponse.message);

			tMemberModel.twofa.code = null;
			tMemberModel.twofa.codeTime = null;
			await tMemberModel.save();
		}

		const generatedToken: string = tokenProvider.generatAuthorizationToken(memberId);
		const firebaseToken = tokenProvider.generatFirebaseToken(memberId);

		return {
			token: generatedToken,
			firebaseToken
		};
	}
};
