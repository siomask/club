import { ModuleContext } from '@graphql-modules/core';
import { map, each, uniq, reduce } from 'lodash';

import { NamespaceEntityProvider, EntityEnum } from '@tclub/core';

import { QueryResolvers, ChatSearchInput, ChatMemberPublicInfo } from '@tclub/types';

import { IMobileModuleContext } from '../mobile.module';

const SEARCH_DEFAULT_LANGUAGE = 'en';

const determinesearchLanguage = (lang?: string): string => {
	switch (lang) {
		case 'ru':
		case 'en':
			return lang;
		default:
			return SEARCH_DEFAULT_LANGUAGE;
	}
};

const chatMemberPublicInfoProjection = {
	_id: true,
	uuid: true,
	firstname: true,
	lastname: true,
	profileUrl: true
};

const searchProjection = {
	_id: true,
	uuid: true,
	firstname: true,
	lastname: true,
	profileUrl: true
};

const prepareTextSearchParam = (str: string, size: number = 3): string[] => {
	if (str.length <= size) return [str];

	let i: number;
	let j: number;

	const result: string[] = [];

	// tslint:disable-next-line:curly
	for (i = 0; i <= str.length - size; i++) {
		// tslint:disable-next-line:curly
		for (j = str.length; j - i >= size; j--) {
			result.push(str.slice(i, j));
		}
	}
	return uniq(result);
};

export const Query: any = {
	chatSearch: async (root, args, { injector, token }: any): Promise<ChatMemberPublicInfo[]> => {
		const TMemberModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);
		const searchParam: ChatSearchInput = args.input;

		const memberId = injector.tMember._id.toString();
		let querySearch = {};

		// tslint:disable-next-line:curly
		if (searchParam && searchParam.search) {
			// ** creating search query
			const searchParts: string[] = searchParam.search.split(' ');

			let termsArray: string[] = [];
			each(searchParts, (searchParamPart: string) => {
				each(prepareTextSearchParam(searchParamPart.trim()), (term: string) => {
					termsArray.push(term);
				});
			});

			termsArray = uniq(termsArray);

			const orQueryArray = reduce(
				termsArray,
				(acc: any, term: string) => {
					acc.push({ firstname: { $regex: term, $options: 'i' } });
					acc.push({ lastname: { $regex: term, $options: 'i' } });
					return acc;
				},
				[]
			);

			querySearch = {
				$and: [{ isVerified: { $exists: true } }, { isBlocked: { $exists: false } }, { $or: [...orQueryArray] }]
			};
			console.log(`AVAR::chatSearch querySearch`, JSON.stringify(querySearch));
			// tslint:disable-next-line:curly
		} else {
			querySearch = { isVerified: { $exists: true }, isBlocked: { $exists: false } };
		}

		const searchMongoResult = await TMemberModel.find(
			{ ...querySearch, _id: { $nin: [memberId] }, 'appSettings.publicMode': true }
		);

		return map(searchMongoResult, (item) => {
			return {
				_id: item._id,
				uuid: item.uuid,
				firstname: item.firstname,
				lastname: item.lastname,
				profileUrl: item.profileUrl
			};
		});
	},

	chatMemberById: async (root, { id }, { injector }: ModuleContext<IMobileModuleContext>) => {
		const TMemberModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);
		const tMember = await TMemberModel.findById(id);
		return tMember.toObject();
	}
};
