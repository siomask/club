import { ModuleContext } from '@graphql-modules/core';
import { TokenProvider, TokenTypeEnum, NamespaceEntityProvider, EntityEnum, QUERY } from '@tclub/core';
import { IMobileModuleContext } from '../mobile.module';
import { ApolloError } from 'apollo-server-core';

const AUTHORIZATION_HEADER_NAME = 'authorization';
export const check_token = (session) => {
	const token = session.req.headers[AUTHORIZATION_HEADER_NAME];
	return {
		version: '0.0.1',
		token
	};
};
// ** NOTE - only not blocked user with valid card expiration could be considered as authorized one
export const isAuthenticated = () => (next) => async (root, args, { injector, token }: any, info) => {
	// tslint:disable-next-line:curly
	const tokenProvider = injector.get(TokenProvider);
	const { memberId } = tokenProvider.decodeToken(token);

	const TMemberModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);

	if (!memberId) throw new ApolloError('Token is not valid');
	const tMember = await TMemberModel.findById(memberId);

	// tslint:disable-next-line:curly
	if (!tMember || tMember.isBlocked || tMember.deletedAt) {
		throw new ApolloError('AccountDisabledError:: please contact TClub');
	}

	injector.tMember = tMember;
	return next(root, args, { injector, token, memberId }, { ...info, memberId });
};

// ** NOTE - only not blocked user with valid card expiration could be considered as authorized one
export const isAuthorized = () => (next) => async (root, args, { injector, token }: any, info) => {
	// tslint:disable-next-line:curly
	const tokenProvider = injector.get(TokenProvider);
	const { memberId } = tokenProvider.decodeToken(token, TokenTypeEnum.AUTHORIZATION);

	const TMemberModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);

	if (!memberId) throw new ApolloError('Token is not valid');
	const tMember = await TMemberModel.findById(memberId);

	// tslint:disable-next-line:curly
	if (!tMember || tMember.isBlocked) {
		throw new ApolloError('AccountDisabledError:: please contact TClub');
	}

	injector.tMember = tMember;
	return next(root, args, { injector, token, memberId, tMember }, { ...info, memberId, tMember });
};

export const validCardNumber = () => (next) => async (root, args, { injector, token }: any, info) => {
	const TMemberModel = injector.get(NamespaceEntityProvider).getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER);

	const refExp: any = new RegExp(args.cardIdentity.substr(-4));
	const tMemberModel = await TMemberModel.findOne({
		$or: [
			{
				'cardInfo.code': { $regex: refExp, $options: 'i' }
			},
			{
				'cardInfo.code': args.cardIdentity
			}
		]
	});

	// tslint:disable-next-line:curly
	if (!tMemberModel) {
		// ** TODO:: new WrongCardIdentityError()
		throw new ApolloError('WrongCardIdentityError:: please check your cardIdentity');
	}
	injector.tMemberModel = tMemberModel;
	return next(
		root,
		args,
		{ injector, token, tMemberModel },
		{
			...info,
			memberId: tMemberModel._id.toString(),
			tMemberModel
		}
	);
};

export const parseFilter = () => (next) => async (root, args, { injector, token }: any, info) => {
	const limit = args.input ? args.input.limit || QUERY.LIMIT : QUERY.LIMIT;
	const skip = args.input ? args.input.skip || QUERY.SKIP : QUERY.SKIP;
	const sort = args.input && args.input.sort ? { [args.input.sort.field]: args.input.sort.dir } : QUERY.SORT;

	return next(
		root,
		args,
		{ injector, token },
		{
			...info,
			limit,
			skip,
			sort
		}
	);
};
