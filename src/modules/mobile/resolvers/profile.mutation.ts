import { MutationResolvers } from '@tclub/types';
import { FirebaseProvider } from '@tclub/core';

export const Mutation: MutationResolvers = {
  changePublicMode: async (root, args, context: any) => {
    const { injector, token, memberId, tMember } = context;
    const tMemberModel = injector.tMember;
    tMemberModel.appSettings.publicMode = args.mode;
    await tMemberModel.save();

    return {
      userId: memberId,
      publicMode: args.mode
    };
  },
  togglePushNotify: async (root, args, context: any, info: any) => {
    const { injector } = context;
    const { tMember } = info;
    const firebaseProvider = injector.get(FirebaseProvider);
    const tMemberModel = injector.tMember;
    tMember.appSettings.pushMode = args.active;
    await tMemberModel.save();
    firebaseProvider.updateUser({ pushMode: args.active }, tMemberModel._id.toString());

    return {
      success: true
    };
  }
};
