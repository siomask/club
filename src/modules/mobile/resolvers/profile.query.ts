import { QueryResolvers } from '@tclub/types';

export const Query: QueryResolvers = {
	me: async (root, args, { injector, token }: any) => {
		return injector.tMember.toObject();
	}
};
