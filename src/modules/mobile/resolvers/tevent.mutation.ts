import { ModuleContext } from '@graphql-modules/core';
import { ApolloError } from 'apollo-server-core';
import moment from 'moment';
import { MutationResolvers, TEvent, TEventStatusEnum } from '@tclub/types';

import {
	NamespaceEntityProvider,
	NOTIFICATINOS,
	EntityEnum,
	TokenProvider,
	FirebaseProvider,
	MailProvider,
	TokenTypeEnum,
	POPULATE,
  addMemberToEvent,
	TEventBookingStatusEnum
} from '@tclub/core';
import { IMobileModuleContext } from '../mobile.module';

export const Mutation: MutationResolvers = {
	addEventToMyFavourites: async (root, { eventId }, { injector, token }: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventFavouriteModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_FAVOURITES);

		const tMemberModel = injector.tMember;
		const memberId = injector.tMember._id;
		// ** registration is open and event is public one
		const tEvent = await TEventModel.findById(eventId);

		// tslint:disable-next-line:curly
		if (!tEvent) {
			throw new ApolloError(`No such event with id ${eventId}`);
		}

		const tEventData: TEvent = tEvent.toObject() as TEvent;

		// tslint:disable-next-line:curly
		if (tEventData.status !== TEventStatusEnum.Open) {
			throw new ApolloError(`You can't take any action on private events, contact TClub admins.`);
		}
		const query = {
			memberId,
			eventId: tEvent._id
		};

		const tEventBooking = await TEventFavouriteModel.findOneAndUpdate(
			query,
			{ isFavourite: true },
			{
				upsert: true,
				new: true
			}
		);

		return tEventBooking.toObject();
	},

	removeEventFromMyFavourites: async (root, { eventId }, { injector }: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventFavouriteModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_FAVOURITES);

		const tMemberModel = injector.tMember;
		const memberId = injector.tMember._id;
		// ** registration is open and event is public one
		const tEvent = await TEventModel.findById(eventId);

		// tslint:disable-next-line:curly
		if (!tEvent) {
			throw new ApolloError(`No such event with id ${eventId}`);
		}

		const tEventData: TEvent = tEvent.toObject() as TEvent;

		// tslint:disable-next-line:curly
		if (tEventData.status !== TEventStatusEnum.Open) {
			throw new ApolloError(`You can't take any action on private events, contact TClub admins.`);
		}

		const query = {
			memberId,
			eventId: tEvent._id
		};

		const tEventBooking = await TEventFavouriteModel.findOneAndUpdate(
			query,
			{ $set: { isFavourite: false } },
			{
				upsert: true,
				new: true
			}
		);

		return tEventBooking.toObject();
	},

	requestEventBooking: async (root, { eventId }, { injector, token }: any) => {
		const tMemberModel = injector.tMember;
		const memberId = injector.tMember._id;
		return await addMemberToEvent({ eventId, memberId }, { injector, token });
	},

	cancelEventBooking: async (root, { eventId }, { injector, token }: any) => {
		const mailProvider = injector.get(MailProvider);
		const firebaseProvider = injector.get(FirebaseProvider);
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const tMemberModel = injector.tMember;
		const memberId = injector.tMember._id;
		// ** registration is open and event is public one
		const tEvent = await TEventModel.findById(eventId);

		// tslint:disable-next-line:curly
		if (!tEvent) {
			throw new ApolloError(`No such event with id ${eventId}`);
		}

		const tEventData: TEvent = tEvent.toObject() as TEvent;
		if (tEventData.status !== TEventStatusEnum.Open) {
			throw new ApolloError(`You can't book a private event, contact.`);
		}

		if (Date.now() > new Date(tEventData.canCancelBefore).getTime()) {
			throw new ApolloError(
				`You not able to cancel on event after ${moment(tEventData.canCancelBefore).format('lll')}`
			);
		}
		const TEventBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);

		const query = {
			memberId,
			eventId: tEvent._id,
			bookingStatus: {
				$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
			}
		};

		const tEventBooking = await TEventBookingModel.findOne(query);

		// tslint:disable-next-line:curly
		if (!tEventBooking) {
			throw new ApolloError(`Before canceling the booking you should be ardered it.`);
		}

		// ** AVAR::TODO - canceling the event part
		tEventBooking.bookingStatus = TEventBookingStatusEnum.MemberCanceled;
		await tEventBooking.save();
		firebaseProvider.notifyUsers(NOTIFICATINOS.CLIENT_CANCEL_BOOK_EVENT({ object: tEventData }), [memberId]);

		const eventBokk = (
			await TEventBookingModel.findById(tEventBooking._id).populate(POPULATE.EVENT_BOOK(injector))
		).toObject();
		mailProvider.sendCancelBookEventEmail(eventBokk);
		return { success: true };
	}
};
