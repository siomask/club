import { isString, isBoolean, reduce } from 'lodash';
import { ModuleContext } from '@graphql-modules/core';
import { ApolloError } from 'apollo-server-core';
import { QueryResolvers, TEventStatusEnum } from '@tclub/types';
import {
	NamespaceEntityProvider,
	EntityEnum,
	UtilsProvider,
	POPULATE,
	QUERY,
	TEventBookingStatusEnum
} from '@tclub/core';
import { IMobileModuleContext } from '../mobile.module';
const mongoose = require('mongoose');

export const Query: QueryResolvers = {
	myEvents: async (root, args: any, { injector, token }: any, { limit, skip, sort }: any) => {
		const utilsProvider = injector.get(UtilsProvider);
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);
		const TEventFavouriteModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_FAVOURITES);
		const tMemberModel = injector.tMember;
		const memberId = injector.tMember._id.toString();
		const rangeFilterQuery: any = {
			status: {
				$in: [TEventStatusEnum.Open, TEventStatusEnum.Closed]
			}
		};

		// tslint:disable-next-line:curly
		if (args.input) {
			if (isString(args.input.range)) {
				const { from, to } = utilsProvider.parseDailyRangeDate(args.input.range);
				rangeFilterQuery.startDate = { $gt: from };
				rangeFilterQuery.endDate = { $lt: to };
			} else {
				if (args.input.start) {
					const start = new Date(args.input.start);
					const end = new Date(args.input.end);
					if (isString(args.input.end)) {
						rangeFilterQuery.$or = [
							{
								startDate: { $gte: start, $lte: end }
							},
							{
								endDate: { $gte: start, $lte: end }
							}
						];
					} else {
						rangeFilterQuery.startDate = { $gte: start };
					}
				} else if (args.input.end) {
					const end = new Date(args.input.end);
					rangeFilterQuery.endDate = { $lte: end };
				}
			}
			if (args.input.typeId) {
				rangeFilterQuery.typeId = mongoose.Types.ObjectId(args.input.typeId);
			}
		}

		let favourites: any = {
			memberId,
			isFavourite: true
		};
		favourites = await TEventFavouriteModel.distinct('eventId', favourites);
		if (args.input && isBoolean(args.input.favourite)) {
			if (args.input.favourite === true) {
				rangeFilterQuery._id = [...new Set(favourites.map((el: any) => el.toString()))];
			} else {
				rangeFilterQuery._id = { $nin: [...new Set(favourites.map((el: any) => el.toString()))] };
			}
		}

		const tEventsInRange = await TEventModel.findActive(rangeFilterQuery)
			.sort(sort)
			.limit(limit)
			.skip(skip)
			.populate(POPULATE.EVENTS(injector));

		const bookQuery: any = {
			eventId: tEventsInRange.map((el) => el._id),
			bookingStatus: {
				$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
			}
		};

		const bookings = await TEventBookingModel.find(bookQuery);
		// ** filtering the event ids based on event status if present
		const eventIds = reduce(
			tEventsInRange,
			(acc, ev) => {
				// ** also filter events
				const evData = ev.toObject();
				const evId = evData._id;
				if (!args.input || !isString(args.input.status)) {
					acc.push(evId);
					return acc;
				}

				// tslint:disable-next-line:curly
				if (args.input.status.indexOf(evData.status) > -1) {
					acc.push(evId);
				}

				return acc;
			},
			[]
		);

		const memberEventInfoQuery: any = {
			// **
			memberId,
			eventId: { $in: eventIds },
			bookingStatus: {
				$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
			}
		};

		// tslint:disable-next-line:curly
		if (args.input && isBoolean(args.input.favourite)) {
			memberEventInfoQuery.favourite = { $eq: args.input.favourite };
		}

		// tslint:disable-next-line:curly
		if (args.input && isString(args.input.bookingStatus)) {
			memberEventInfoQuery.bookingStatus = { $eq: args.input.bookingStatus };
		}

		const memberEventsInfo = await TEventBookingModel.find(memberEventInfoQuery);
		const memberEventsMap = reduce(
			memberEventsInfo,
			(acc, memberEvent) => {
				acc[memberEvent.eventId] = memberEvent.toObject();
				return acc;
			},
			{}
		);

		const result = reduce(
			tEventsInRange,
			(acc, event) => {
				const evData = event.toObject();
				const eventId = evData._id;
				acc.push({
					...evData,
					bookedCapacity: bookings.filter((el) => el.eventId.toString() === evData._id.toString()).length,
					isFavourite:
						args.input && isBoolean(args.input.favourite)
							? args.input.favourite
							: favourites.filter((el) => el.toString() === evData._id.toString()).length > 0,
					info: memberEventsMap[eventId]
				});
				return acc;
			},
			[]
		);

		return result;
	},
	eventTypes: async (root, args, { injector, token }: any, { limit, skip, sort }: any) => {
		const TEventTypeModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_TYPE);

		const result = await TEventTypeModel.findActive({})
			.sort(sort)
			.limit(limit)
			.skip(skip);

		return result.map((el) => el.toObject());
	},

	eventInfo: async (root, args, { injector, token }: any) => {
		const TEventModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT);
		const TEventBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_MEMBER_EVENT_INFO);
    const TEventFavouriteModel = injector
      .get(NamespaceEntityProvider)
      .getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_EVENT_FAVOURITES);
		const tMemberModel = injector.tMember;
		const memberId = injector.tMember._id.toString();
		const { eventId } = args;
		const tEvent = await TEventModel.findById(eventId).populate(POPULATE.EVENTS(injector));

		// tslint:disable-next-line:curly
		if (!tEvent) {
			throw new ApolloError('There is no event with such id');
		}

		// tslint:disable-next-line:curly
		if (tEvent.status === TEventStatusEnum.Private) {
			// ** maybe notify admin about a securirty issue.
			throw new ApolloError('There is no event with such id.');
		}

		const evData = tEvent.toObject();

		const memberEventInfoQuery: any = {
			// **
			memberId,
			eventId: { $eq: eventId },
			bookingStatus: {
				$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
			}
		};

		const memberEventInfo = await TEventBookingModel.findOne(memberEventInfoQuery);
		let favourites: any = {
			eventId,
			memberId,
			isFavourite: true
		};
		return {
			...evData,
			isFavourite: (await TEventFavouriteModel.count( favourites)) > 0,
			bookedCapacity: await TEventBookingModel.count({
				eventId,
				bookingStatus: {
					$in: [TEventBookingStatusEnum.MemberRequested, TEventBookingStatusEnum.ConciergeConfirmed]
				}
			}),
			info: memberEventInfo ? memberEventInfo.toObject() : undefined
		};
	}
};
