import { ModuleContext } from '@graphql-modules/core';
import { ApolloError } from 'apollo-server-core';
import { MutationResolvers } from '@tclub/types';

import {
	NamespaceEntityProvider,
	NOTIFICATINOS,
	EntityEnum,
	TokenProvider,
	FirebaseProvider,
	TokenTypeEnum,
	POPULATE,
	MailProvider,
	TPrivilegePeriodRuleRepeat,
	QUERY,
	TPrivilegeStatus
} from '@tclub/core';
import { IMobileModuleContext } from '../mobile.module';
import shortid from 'shortid';

export const Mutation: MutationResolvers = {
	activatePrivilege: async (root, { privilegeId, input: { start, end } }, { injector, token }: any, info: any) => {
		const mailProvider = injector.get(MailProvider);
		const firebaseProvider = injector.get(FirebaseProvider);
		const TPrivilegeModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);
		const BookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_BOOKING);
		const PMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);

		const memberId = info.tMember._id.toString();
		const query = {
			memberId,
			privilegeId
		};

		if (!end) end = start;
		const startT = new Date(start).getTime();
		const endT = new Date(end).getTime();
		var startDay = new Date();
		startDay.setHours(0, 0, 0, 0);
		if (startT < endT) {
			throw new ApolloError(`time range is incorrect`);
		}

		if (startT < startDay.getTime()) {
			throw new ApolloError(`not able to activate on previous days!`);
		}

		if (!(await PMemberModel.findOne({ memberId, privilegeId }))) {
			throw new ApolloError(`Privilege is not available for you`);
		}
		const tPrivilege = await TPrivilegeModel.findById(privilegeId);
		if (!tPrivilege || tPrivilege.status !== TPrivilegeStatus.Active) {
			throw new ApolloError(`privilege does not available ${privilegeId}`);
		}
		if (startT < new Date(tPrivilege.start).getTime() || endT > new Date(tPrivilege.end).getTime()) {
			throw new ApolloError(`privilege is not available in selected period`);
		}
		const activePeriod = tPrivilege.rule;

		if (!activePeriod) {
			throw new ApolloError(`privilege does not have activations in selected period`);
		}
		const bookings = await BookingModel.find({ ...query });

		let activations = TPrivilegeModel.activationsByRule(tPrivilege, TPrivilegePeriodRuleRepeat, bookings, startT);
		if (activations.length >= activePeriod.count) {
			throw new ApolloError(
				`you used all available activations for selected period, please contact support to increase`
			);
		}

		const tBooking = await BookingModel.create({
			...query,
			start,
			end
		});

		const privilege = (
			await TPrivilegeModel.findById(privilegeId).populate(POPULATE.PRIVILEGE(injector))
		).toObject();
		privilege.last_activation = await BookingModel.findOne({ privilegeId: privilege._id })
			.sort(QUERY.SORT)
			.populate(POPULATE.BOOKING_PRIVILEGE(injector));

		activations = TPrivilegeModel.activationsByRule(
			tPrivilege,
			TPrivilegePeriodRuleRepeat,
			[...bookings, tBooking],
			startT
		);
		privilege.used = activations.length;
		privilege.remain = activePeriod.count - privilege.used;
		mailProvider.sendBookPrivilegeEmail({
			...tBooking.toObject(),
			privilegeId: privilege,
			memberId: info.tMember
		});
		return privilege;
	}
};
