import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers, TPrivilegeStatus } from '@tclub/types';

import { NamespaceEntityProvider, EntityEnum, POPULATE, TPrivilegePeriodRuleRepeat, QUERY } from '@tclub/core';

function calcActivates(el, activates = [], ItemModel) {
	if (el.toObject) el = el.toObject();
	const curActivate = el.rule;
	el.all_activations = activates.filter((activate) => activate.privilegeId.toString() === el._id.toString());
	el.last_activation = el.all_activations[0];

	let res = {};
	el.last_activations = ItemModel.activationsByRule(el, TPrivilegePeriodRuleRepeat, el.all_activations);
	el.used = el.last_activations.length;
	el.remain = curActivate.count - el.used;

	return el;
}
export const Query: QueryResolvers = {
	privilegeId: async (root, args: any, { injector }: ModuleContext<any>, { limit, skip, sort, memberId }: any) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);
		const PrivilegeMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);
		const PrivilegeBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_BOOKING);

		const item = (await ItemModel.findById(args._id)).toObject();
		const activates = await PrivilegeBookingModel.find({ privilegeId: item._id, memberId });
		return calcActivates(item, activates, ItemModel);
	},

	privilegeList: async (root, args, { injector }: ModuleContext<any>, { limit, skip, sort, memberId }: any) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);
		const PrivilegeMemberModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);
		const PrivilegeBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_BOOKING);

		const list = (
			await ItemModel.findActive({
				status: TPrivilegeStatus.Active,
				_id: { $in: await PrivilegeMemberModel.distinctActive('privilegeId', { memberId }) }
			})
				.sort(sort)
				.limit(limit)
				.skip(skip)
				.populate(POPULATE.PRIVILEGE(injector))
		).map((el) => el.toObject());
		const activates = await PrivilegeBookingModel.find({
			memberId,
			privilegeId: { $in: list.map((el) => el._id) }
		});

		return list.map((el) => calcActivates(el, activates, ItemModel));
	},

	myPrivilegeActvatedList: async (
		root,
		args,
		{ injector }: ModuleContext<any>,
		{ memberId, limit, skip, sort }: any
	) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE);
		const ItemMemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_MEMBER);
		const PrivilegeBookingModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_PRVILEGE_BOOKING);

		const privilegeId = await ItemMemModel.distinctActive('privilegeId', { memberId });
		const bookings = await PrivilegeBookingModel.distinctActive('privilegeId', { privilegeId });
		const list = await ItemModel.findActive({
			status: TPrivilegeStatus.Active,
			_id: { $in: bookings }
		})
			.sort(sort)
			.limit(limit)
			.skip(skip)
			.populate(POPULATE.PRIVILEGE(injector));
		const activates = await PrivilegeBookingModel.find({
			privilegeId: { $in: list.map((el) => el._id.toString()) },
			memberId
		}).sort(QUERY.SORT);

		return list.map((el) => calcActivates(el, activates, ItemModel));
	}
};
