import { ModuleContext } from '@graphql-modules/core';
import { QueryResolvers } from '@tclub/types';
const mongoose = require('mongoose');

import { NamespaceEntityProvider, EntityEnum, getService, getServiceBooking } from '@tclub/core';

export const Query: QueryResolvers = {
	serviceById: async (root, args, { injector }: ModuleContext<any>) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		return await getService(ItemModel, injector, args);
	},

	serviceList: async (root, args, { injector }: ModuleContext<any>, info: any) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		let req;
		if (args.input) {
			if (args.input.typeId) {
				req = { typeId: mongoose.Types.ObjectId(args.input.typeId) };
			}
		}
		return await getService(ItemModel, injector, null, info, req);
	},
	serviceTypes: async (root, args, { injector }: ModuleContext<any>, info: any) => {
		const ItemModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_TYPE);
		return await ItemModel.findActive({})
			.limit(info.limit)
			.skip(info.skip)
			.sort(info.sort);
	},

	myBookedServiceList: async (root, args, { injector }: ModuleContext<any>, { memberId, ...info }: any) => {
		const ItemModelBooking = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_BOOKING);
		let req;
		if (args.input) {
			if (args.input.typeId) {
				req = { typeId: mongoose.Types.ObjectId(args.input.typeId) };
				const ItemModel = injector
					.get(NamespaceEntityProvider)
					.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
				req = { eventId: { $in: await ItemModel.findActive(req).distinct('_id') } };
			}
		}
		return await getServiceBooking(
			ItemModelBooking,
			injector,
			null,
			{ memberId, deletedAt: { $exists: false }, ...req },
			info
		);
	}
};
