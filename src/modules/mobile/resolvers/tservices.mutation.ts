import { ModuleContext } from '@graphql-modules/core';
import { ApolloError } from 'apollo-server-core';
import { MutationResolvers, TEvent, TEventStatusEnum } from '@tclub/types';

import shortid from 'shortid';
import {
	NamespaceEntityProvider,
	EntityEnum,
	MailProvider,
	TokenProvider,
	TokenTypeEnum,
	getServiceBooking,
	TServiceBookingStatusEnum
} from '@tclub/core';
import { IMobileModuleContext } from '../mobile.module';

export const Mutation: MutationResolvers = {
	bookService: async (
		root,
		{ serviceId, input: { startDate, endDate } }: any,
		{ injector, token }: any,
		{ memberId }: any
	) => {
		const mailProvider = injector.get(MailProvider);
		const TServiceModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE);
		const TServiceBoolinkModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_BOOKING);

		if (Date.now() > new Date(startDate).getTime() - 1000 * 60) {
			throw new ApolloError(`No able to book service on previous time`);
		}
		if (new Date(startDate).getTime() > new Date(endDate).getTime()) {
			throw new ApolloError(`endDate should be more startDate`);
		}
		if ((await TServiceModel.count({ _id: serviceId, deletedAt: { $exists: false } }).length) === 0) {
			throw new ApolloError('No service available');
		}
		if (
			(await TServiceBoolinkModel.count({
				serviceId,
				memberId,
				startDate: { $gte: startDate },
				endDate: { $lte: endDate }
			})) > 0
		) {
			throw new ApolloError('You already has been booking the service in selected period');
		}

		const item = await TServiceBoolinkModel.create({
			startDate,
			endDate,
			serviceId,
			status: TServiceBookingStatusEnum.Requested,
			memberId
		});
		const service = await getServiceBooking(TServiceBoolinkModel, injector, item);
		mailProvider.sendBookServiceEmail(service);
		return service;
	},

	cancelBookService: async (root, { bookServiceId }: any, { injector, token }: any, { memberId }: any) => {
		const TServiceBoolinkModel = injector
			.get(NamespaceEntityProvider)
			.getEntityModel(EntityEnum.DB_NAME, EntityEnum.T_SERVICE_BOOKING);
		const item = await TServiceBoolinkModel.findOneAndUpdate(
			{
				_id: bookServiceId
			},
			{ $set: { status: TServiceBookingStatusEnum.Closed, updatedAt: Date.now() } }
		);

		if (!item) {
			throw new ApolloError('No booked service found!');
		}
		return getServiceBooking(TServiceBoolinkModel, injector, item);
	}
};
