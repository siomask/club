import path from 'path';
import { GraphQLModule } from '@graphql-modules/core';
import gql from 'graphql-tag';
import { GraphQLUpload } from 'graphql-upload';

let gm: GraphQLModule = null;

export const createUploadModule = (): GraphQLModule => {
	if (gm) return gm;

	console.log('createUploadModule');
	gm = new GraphQLModule<{}, {}, any>({
		typeDefs: gql`
			scalar Upload
		`,
		resolvers: {
			Upload: GraphQLUpload
		}
	});

	return gm;
};
