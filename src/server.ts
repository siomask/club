// ** AVAR::DONE
import express from 'express';
import path from 'path';

const { graphqlUploadExpress, GraphQLUpload } = require('graphql-upload');
import { createServer } from 'http';
import { GraphQLError } from 'graphql';
import { ContextFunction, ApolloError } from 'apollo-server-core';
import { ApolloServer, ApolloServerExpressConfig } from 'apollo-server-express';
import { GraphQLModule } from '@graphql-modules/core';

import { createAppModule } from '@tclub/app';

// **** WS part of authorization

const validateToken = (authToken) => {
	// ... validate token and return a Promise, rejects in case of an error
};

const findUser = (authToken) => {
	return (tokenValidationResult) => {
		// ... finds user by auth token and return a Promise, rejects in case of an error
	};
};

const onConnect = async (connectionParams, webSocket, context) => {
	console.log('Server::onConnect');
	if (connectionParams.authToken) {
		await validateToken(connectionParams.authToken);
		const user = await findUser(connectionParams.authToken);
		return {
			currentUser: user
		};
	}

	throw new Error('Missing auth token!');
};

const onDisConnect = async (webSocket, context) => {
	// ** when client disconnects smth can happen here
};

const startServer = async (resolve, reject) => {
	const PORT = process.env.PORT || 7000;
	const TCLUB_ENV = process.env.TCLUB_ENV || 'all';

  const appModule: GraphQLModule = createAppModule(TCLUB_ENV);

	const { subscriptions } = appModule;

	// ** by calling this it'll start the initialization of the modules and providers
	appModule.injector;

	// ** We are reading express req and res and passing to context which will be passed to all GQ resolvers
	const contextGenerator: ContextFunction = ({ req, res, connection }) => {
		return connection ? connection.context : { req, res };
	};

	const appoloServerConfig: ApolloServerExpressConfig = {
		// ** We can use GQL module in appolo server and use context builder like ...MyAccountsModule.context({ req, res })
		modules: [appModule],
		// ** schema,
		// ** resolvers,
		context: contextGenerator,
		// ** adding onConnect for auth-ing the ws connection
		subscriptions: { ...subscriptions, onConnect },
		// formatError: (err) => { console.log('TCLUB::ERRORSTACK', err.stack); return err; },
		// formatError: (error: GraphQLError) => {
		//   console.log('TCLUB::ERROR', error);
		//   return error.originalError instanceof ApolloError ? error : new GraphQLError(`Internal Error`);
		// },
		// ** for production we need debug to be false
		formatError: (err) => {
			if (process.env.NODE_ENV=='prod') {
				return {
					message: err.message
				};
			} else {
				return err;
			}
		},
		uploads: {
			maxFileSize: 10000000, // 10 MB
			maxFiles: 20
		},
		debug: true
	};

	const apolloServer = new ApolloServer(appoloServerConfig);

	const app = express();

	const staticDir = path.resolve(__dirname, '../resources');
	app.use(express.static(staticDir));
	apolloServer.applyMiddleware({ app });

	const httpServer = createServer(app);
	apolloServer.installSubscriptionHandlers(httpServer);

	httpServer.listen({ port: PORT }, () => {
		// tslint:disable-next-line: no-console
		console.info(`🚀 Server ready at http://localhost:${PORT}${apolloServer.graphqlPath}`);
		// tslint:disable-next-line: no-console
		console.info(`🚀 Subsciription ready at ws://localhost:${PORT}${apolloServer.subscriptionsPath}`);
		resolve();
	});
};

export { startServer };
