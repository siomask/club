import path from 'path';
import fs from 'fs';
import glob from 'glob';
import gql from 'graphql-tag';

import { generate } from '@graphql-codegen/cli';
import { DIRECTIVES as GQL_MONGOOSE_DIRECTIVES } from '@sfast/gql-schema-mongoose';
import { DIRECTIVES as GQL_PARSER_GENERAL_DIRECTIVES, GraphEntityParser } from '@sfast/gql-schema-parser';

export const generateTSTypes = async () => {
	try {
		const pathToModules = path.resolve(__dirname, '../modules');
		const mergedSchemas = mergeGraphqlSchemas(pathToModules);
		const schemaWithGQLDirectives = [GQL_MONGOOSE_DIRECTIVES, ...mergedSchemas].join('\n');

		// ** console.log('AVAR::generateTSTypes mergedSchemas = ', schemaWithGQLDirectives);

		const graphNodeGQLed = gql(schemaWithGQLDirectives);
		const processedWithGql = new GraphEntityParser(graphNodeGQLed);
		const finalSchema = processedWithGql.getSchema(graphNodeGQLed);

		console.log(` !!! GQL::generateTSTypes final schema ready !!! \n`);
		console.log(`GQL::generateTSTypes`, processedWithGql.printSchema(finalSchema));

		await generate(
			{
				schema: processedWithGql.printSchema(finalSchema),
				generates: {
					[`${pathToModules}/core/types/genTypes.ts`]: {
						plugins: ['typescript', 'typescript-resolvers']
					}
				}
			},
			true
		);
	} catch (err) {
		console.error(`GQL::ERROR::generateTSTypes::`, err);
	}
};

// const pathToModules = path.resolve(__dirname, '../modules');

export const generateGraphQLTypes = async (pathToModules: string) => {
	const mergedSchemas = mergeGraphqlSchemas(pathToModules);
	const schemaWithGQLDirectives = [GQL_MONGOOSE_DIRECTIVES, ...mergedSchemas].join('\n');

	// ** console.log('AVAR::generateTSTypes mergedSchemas = ', schemaWithGQLDirectives);

	const graphNodeGQLed = gql(schemaWithGQLDirectives);
	const processedWithGql = new GraphEntityParser(graphNodeGQLed);
	const finalSchema = processedWithGql.getSchema(graphNodeGQLed);
	return finalSchema;
};

export const mergeGraphqlSchemas = (pathToModulesArg: string) => {
	let ignores = [];
  if (process.env.TCLUB_ENV === 'admin') {
		ignores = [`${pathToModulesArg}/mobile/**/*`];
	} else {
		ignores = [`${pathToModulesArg}/admin/**/*`];
	}
	const graphqlTypes = glob
		.sync(`${pathToModulesArg}/**/*.graphql`, { ignore: [`${pathToModulesArg}/**/*.spec.graphql`, ...ignores] })
		.map((file) => fs.readFileSync(file, { encoding: 'utf8' }));
	return graphqlTypes;
};
