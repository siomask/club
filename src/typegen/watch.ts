import watch from 'glob-watcher';
import path from 'path';

import { generateTSTypes } from './utils';

const pathToModules = path.resolve(__dirname, '../modules');

const options = {
	ignoreInitial: false,
	delay: 1000
};

watch([`${pathToModules}/**/*.graphql`, `!${pathToModules}/**/*.spec.graphql`], options, (done) => {
	generateTSTypes();
	done();
});
