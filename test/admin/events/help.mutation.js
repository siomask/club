const faker = require('faker');
const EventClub = require('../../factories/EventClub');
const addEvent = (request, event = new EventClub()) => {
	const query = `mutation{
      addEvent(input:{
          capacity: ${event.capacity}
          typeId: "${event.typeId}"
          title: "${event.title}"
          description1: "${event.description1}"
          status: ${event.status}
      }){
       _id
        uuid
        capacity
        createdAt
        updatedAt
        title
        description1
        typeId{
          _id
        }
        status
    }
}`;
	return new Promise((resolve, reject) => {
		request
			.send({
				query
			})
			.end((err, res) => {
				if (err) return reject(err);
        resolve(res.body.data.addEvent);
			});
	});
};
const removeEvent = (request, event) => {
	return new Promise((resolve, reject) => {
		request
			.send({
				query: `mutation {
       removeEvent(
           _id:"${event._id}"
           ){
           success
        }
       }`
			})
			.end((err, res) => {
				if (err) return reject(err);
        resolve(res.body.data.removeEvent);
			});
	});
};
const updateEvent = (request, event) => {
	return new Promise((resolve, reject) => {
		request
			.send({
				query: `mutation {
       updateEvent(
           eventId:"${event._id}" input:{
           capacity: ${event.capacity}
           typeId: "${event.typeId}"
          title: "${event.title}"
          description1: "${event.description1}"
          status: ${event.status}
      }
           ){
             _id
            uuid
            capacity
            createdAt
            updatedAt
            title
            description1
            typeId{
              _id
            }
            status
        }
       }`
			})
			.end((err, res) => {
				if (err) return reject(err);
				resolve(res.body.data.updateEvent);
			});
	});
};

module.exports = {
	updateEvent,
	removeEvent,
	addEvent
};
