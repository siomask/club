const chai = require('chai');

const expect = chai.expect;
const { beforeWithAuthAdmin, afterAll } = require('../../config');
const { removeEvent, updateEvent, addEvent } = require('./help.mutation');
const EventClub = require('../../factories/EventClub');

describe('admin-EventClub-mutations', () => {
	let opt;
	let event;
	let newEvent = new EventClub();
	beforeEach(async () => {
		opt = await beforeWithAuthAdmin();
	}); 

	it('addEvent', async () => {
		event = await addEvent(opt.requestAdmin, newEvent);
		expect(event.title).to.equal(newEvent.title);
	});
	it('updateEvent', async () => {
		const updatedEventItem = { ...event, ...new EventClub() };
		const res = await updateEvent(opt.requestAdmin, updatedEventItem);
		expect(res.description1).to.equal(updatedEventItem.description1);
	});
	it('removeEvent', async () => {
		const res = await removeEvent(opt.requestAdmin, event);
		expect(res.success).to.equal(true);
	});
});
