const Member = require('../../factories/Member');
const addMember = (request, member = new Member()) => {
	const query = `mutation {
       addMember(
            input: {
                   firstname:"${member.firstname}"
                   lastname:"${member.lastname}"
                   countryCode: "${member.countryCode}"
                   phoneNumber: "${member.phoneNumber}"
                   countryCodeIndex : "${member.countryCodeIndex }"
                   cardInfo: {
                      code:"${member.cardInfo.code}"
                   }
                   email:  "${member.email}"
                   }
                 )
                 {
                   _id
                   firstname
                   lastname
                   countryCode
                   countryCodeIndex
                   phoneNumber
                   cardInfo{
                      code
                   }
                }
       }`;
	return new Promise((resolve, reject) => {
		request
			.send({
				query
			})
			.end((err, res) => {
				if (err) return reject(err);
        resolve(res.body.data.addMember);
			});
	});
};
const updateMember = (request, member = new Member()) => {
	const query = `mutation {
       updateMember(
          memberId:"${member._id}"
            input: {
                   firstname:"${member.firstname}"
                   lastname:"${member.lastname}"
                   countryCode: "${member.countryCode}"
                   phoneNumber: "${member.phoneNumber}"
                   countryCodeIndex : "${member.countryCodeIndex }"
                   cardInfo: {
                      code:"${member.cardInfo.code}"
                   }
                   email:  "${member.email}"
                   }
                 )
                 {
                   _id
                   firstname
                   lastname
                   countryCodeIndex
                   countryCode
                   phoneNumber
                   cardInfo{
                      code
                   }
                }
       }`;
	return new Promise((resolve, reject) => {
		request
			.send({
				query
			})
			.end((err, res) => {
				if (err) return reject(err);
        resolve(res.body.data.updateMember);
			});
	});
};
const removeMember = (request, member) => {
  return new Promise((resolve, reject) => {
		request
			.send({
				query: `mutation {
       removeMember(
           memberId:"${member._id}"
           ){
           success
        }
       }`
			})
			.end((err, res) => {
        if (err) return reject(err);
        resolve(res.body.data.removeMember);
			});
	});
};

module.exports = {
  updateMember,
	removeMember,
	addMember
};
