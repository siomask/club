const chai = require('chai');

const expect = chai.expect;
const { beforeWithAuthAdmin, afterAll } = require('../../config');
const { removeMember, updateMember, addMember } = require('./help');
const Member = require('../../factories/Member');

describe('admin-Member-mutations', () => {
	let opt;
	let member;
	let item = new Member();
	beforeEach(async () => {
		opt = await beforeWithAuthAdmin();
	});
	after(async () => {});

	it('addMember', async () => {
		member = await addMember(opt.requestAdmin, item);
		expect(member.firstname).to.equal(item.firstname);
		expect(parseInt(member.cardInfo.code)).to.equal(parseInt(item.cardInfo.code));
	});
	it('updateMember', async () => {
		const updatedItem = { ...member, ...new Member() };
		const res = await updateMember(opt.requestAdmin, updatedItem);
		expect(res.lastname).to.equal(updatedItem.lastname);
	});
	it('removeMember', async () => {
		const res = await removeMember(opt.requestAdmin, member);
    expect(res.success).to.equal(true);
	});
});
