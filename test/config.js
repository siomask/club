const url = `http://35.217.32.24:7005`;
const urlAdmin = `http://35.217.32.24:7006`;
const { removeMember, addMember } = require('./admin/member/help');
const request = require('supertest')(url);
const beforeWithoutAuth = () => {
	return {
		request: require('supertest')(url).post('/graphql')
	};
};
const beforeWithAuth = (member = null) => {
	return new Promise(async (resolve, reject) => {
		let opt = await beforeWithAuthAdmin();
		const requestAdmin = opt.requestAdmin;
		if (!member) member = await addMember(requestAdmin);

		beforeWithoutAuth()
			.request.send({
				query: `mutation { authenticateWithCardIdentity(cardIdentity:"${member.cardInfo.code}") { token code } }`
			})
			.expect(200)
			.end((err, res) => {
				if (err) return reject(err);
				Authorization = res.body.data.authenticateWithCardIdentity.token;
				beforeWithoutAuth()
					.request.set({
						Authorization
					})
					.send({
						query: `mutation{ authorizeWithSMS(code:"${res.body.data.authenticateWithCardIdentity.code}") {  token firebaseToken} }`
					})
					.expect(200)
					.end((err, res) => {
						if (err) return done(err);
						if (!res.body.data.authorizeWithSMS) {
							console.log(res.body, member);
						}
						FirebaseToken = res.body.data.authorizeWithSMS.firebaseToken;
						Authorization = res.body.data.authorizeWithSMS.token;
						resolve({
							requestAdmin,
							member,
							Authorization,
							FirebaseToken,
							request: beforeWithoutAuth().request.set({
								Authorization
							})
						});
					});
			});
	});
};
const beforeWithAuthAdmin = () => {
	const requestAdmin = require('supertest')(urlAdmin);
	return new Promise((resolve, reject) => {
		requestAdmin
			.post('/graphql')
			.send({ query: 'mutation { login(email:"simonpas323@gmail.com" password:"123456") { token } }' })
			.expect(200)
			.end((err, res) => {
				if (err) return reject(err);
				Authorization = res.body.data.login.token;
				resolve({
					Authorization,
					requestAdmin: require('supertest')(urlAdmin)
						.post('/graphql')
						.set({
							Authorization
						})
				});
			});
	});
};
const afterAll = (requestAdmin, member) => {
	return new Promise(async (resolve, reject) => {
		try {
			await removeMember(requestAdmin, member);
			resolve();
		} catch (e) {
			console.log(e);
			reject(e);
		}
	});
};
const initServers = async () => {};
module.exports = {
	url,
	beforeWithAuth,
	beforeWithAuthAdmin,
	beforeWithoutAuth,
	afterAll
};
