const faker = require('faker');
module.exports = function(item = {}) {
	return {
		typeId: item.typeId || "5e22f0dfe0632b1925f27df4",
		status: item.status || "OPEN",
		title: item.title || faker.name.title(),
		capacity: item.capacity || faker.random.number({ min: 11, max: 80 }),
		description1: item.description1 ||faker.name.title()|| faker.lorem.text()
	};
};
