const faker = require('faker');
module.exports = function(item = {}) {
	return {
		firstname: faker.name.firstName(),
		lastname: faker.name.lastName(),
		countryCode: faker.random.number({ min: 1, max: 3 }),
		phoneNumber: faker.random.number({ min: 1000000000, max: 9999999999 }),
    countryCodeIndex: faker.address.countryCode(),
		email: faker.internet.email(),
		cardInfo: {
			code: faker.random.number({ min: 1000000000, max: 9999999999 })
		}
	};
};
