const chai = require('chai');

const expect = chai.expect;
const { url, beforeWithoutAuth, beforeWithAuthAdmin } = require('../../config');
const { addMember, removeMember } = require('../../admin/member/help');

describe('mobile-Auth-query', () => {
	let opt;
	let optWithoutAuth;
	let member;
	let Authorization;
	before(async () => {
		opt = await beforeWithAuthAdmin();
		member = await addMember(opt.requestAdmin);
	});
	beforeEach(async () => {
		optWithoutAuth = beforeWithoutAuth();
	});

	after(async () => {
		await removeMember((await beforeWithAuthAdmin()).requestAdmin, member);
	});

	it('authenticateWithCardIdentity', async () => {
		const res = await optWithoutAuth.request.send({
			query: `mutation { authenticateWithCardIdentity(cardIdentity:"${member.cardInfo.code}") { token code } }`
		});
		expect(res.body.data.authenticateWithCardIdentity.token).to.be.a('string');
		expect(res.body.data.authenticateWithCardIdentity.code).to.have.lengthOf(6);
		code = res.body.data.authenticateWithCardIdentity.code;
		Authorization = res.body.data.authenticateWithCardIdentity.token;
	});

	it('authorizeWithSMS', async () => {
		const res = await optWithoutAuth.request
			.set({
				Authorization
			})
			.send({
				query: `mutation { authorizeWithSMS(code:"${code}") { token firebaseToken } }`
			});
		expect(res.body.data.authorizeWithSMS.token).to.be.a('string');
		expect(res.body.data.authorizeWithSMS.firebaseToken).to.be.a('string');
	});
});
