const addEventToMyFavourites = async (request, event) => {
	const res = await request.send({
		query: `mutation{ addEventToMyFavourites(eventId:"${event._id}") {
        _id
        memberId {
            _id
        }
        eventId  {
            _id
        }
        isFavourite
	}
	}`
	});
	return res.body.data.addEventToMyFavourites;
};
const removeEventFromMyFavourites = async (request, event) => {
	const res = await request.send({
		query: `mutation{ removeEventFromMyFavourites(eventId:"${event._id}") {
        _id
        memberId {
            _id
        }
        eventId  {
            _id
        }
        isFavourite
	}
	}`
	});
	return res.body.data.removeEventFromMyFavourites;
};
const requestEventBooking = async (request, event) => {
	const res = await request.send({
		query: `mutation{ requestEventBooking(eventId:"${event._id}") {
    success
	}
	}`
	});
	return res.body.data.requestEventBooking;
};
const cancelEventBooking = async (request, event) => {
	const res = await request.send({
		query: `mutation{ cancelEventBooking(eventId:"${event._id}") {
    success
	}
	}`
	});
	return res.body.data.cancelEventBooking;
};
module.exports = {
	cancelEventBooking,
	requestEventBooking,
	removeEventFromMyFavourites,
	addEventToMyFavourites
};
