const chai = require('chai');

const expect = chai.expect;
const { beforeWithAuth, beforeWithAuthAdmin, afterAll } = require('../../config');
const {
	cancelEventBooking,
	requestEventBooking,
	removeEventFromMyFavourites,
	addEventToMyFavourites
} = require('./help.mutation.js');
const { addEvent, removeEvent } = require('../../admin/events/help.mutation.js');

describe('mobile-Events-mutations', () => {
	let opt;
	let event;
	let member;
	beforeEach(async () => {
		try {
			if (!event) event = await addEvent((await beforeWithAuthAdmin()).requestAdmin);
			opt = await beforeWithAuth(member);
			member = opt.member;
		} catch (e) {
			console.log(e);
		}
	});
	afterEach(async () => {});
	after(async () => {
		await removeEvent((await beforeWithAuthAdmin()).requestAdmin, event);
		await afterAll((await beforeWithAuthAdmin()).requestAdmin, member);
	});

	it('addEventToMyFavourites', async () => {
		const res = await addEventToMyFavourites(opt.request, event);
		expect(res.eventId._id).to.equal(event._id);
		expect(res.isFavourite).to.equal(true);
	});
	it('removeEventFromMyFavourites', async () => {
		const res = await removeEventFromMyFavourites(opt.request, event);
		expect(res.eventId._id).to.equal(event._id);
		expect(res.isFavourite).to.equal(false);
	});
	it('requestEventBooking', async () => {
		const res = await requestEventBooking(opt.request, event);
		expect(res.success).to.equal(true);
	});
	it('cancelEventBooking', async () => {
		const res = await cancelEventBooking(opt.request, event);
		expect(res.success).to.equal(true);
	});
});
