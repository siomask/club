const chai = require('chai');

const expect = chai.expect;
const { request, beforeWithAuth,beforeWithAuthAdmin, afterAll } = require('../../config');

describe('mobile-Events', () => {
	let opt;
	beforeEach(async () => {
		opt = await beforeWithAuth();
	});
	afterEach(async () => {
		await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
	});
	after(async () => {
		await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
	});

	it('myEvents', (done) => {
		opt.request
			.send({ query: '{ myEvents(input:{limit:2}) { _id } }' })
			.expect(200)
			.end((err, res) => {
				if (err) return done(err);
				expect(res.body.data.myEvents.length).to.equal(2);
				done();
			});
	});

	it('Returns only favourites Events', async () => {
		expect(1).to.equal(1);
	});
	it('eventTypes', async () => {
		expect(1).to.equal(1);
	});
	it('eventInfo', async () => {
		expect(1).to.equal(1);
	});
});
