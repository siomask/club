const chai = require('chai');

const expect = chai.expect;
const { url, beforeWithAuth, beforeWithAuthAdmin, afterAll } = require('../../config');

describe('mobile-Privilege-mutation', () => {
	let opt;
	beforeEach(async () => {
		opt = await beforeWithAuth();
	});
	afterEach(async () => {
		await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
	}); 

	it('activatePrivilege', async () => {
		expect(1).to.equal(1);
	});
});
