const chai = require('chai');

const expect = chai.expect;
const { request, beforeWithAuth,beforeWithAuthAdmin, afterAll } = require('../../config');

describe('mobile-Privilege-query', () => {
  let opt;
  beforeEach(async () => {
    opt = await beforeWithAuth();
  });
  afterEach(async () => {
    await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
  });
  after(async () => {
    await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
  });

	it('privilegeId', async () => {
		expect(1).to.equal(1);
	});
	it('privilegeList', async () => {
		expect(1).to.equal(1);
	});
	it('myPrivilegeActvatedList', async () => {
		expect(1).to.equal(1);
	});
});
