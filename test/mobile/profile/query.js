const chai = require('chai');

const expect = chai.expect;
const { request, beforeWithAuth,beforeWithAuthAdmin, afterAll } = require('../../config');

describe('mobile-Profile-query', () => {
  let opt;
  beforeEach(async () => {
    opt = await beforeWithAuth();
  });
  afterEach(async () => {
    await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
  });
  after(async () => {
    await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
  });

  it('about me', async () => {
    expect(1).to.equal(1);
  });
});
