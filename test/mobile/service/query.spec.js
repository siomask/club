const chai = require('chai');

const expect = chai.expect;
const { request, beforeWithAuth,beforeWithAuthAdmin, afterAll } = require('../../config');

describe('mobile-Service-query', () => {
  let opt;
  beforeEach(async () => {
    opt = await beforeWithAuth();
  });
  afterEach(async () => {
    await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
  });
  after(async () => {
    await afterAll((await beforeWithAuthAdmin()).requestAdmin, opt.member);
  });

	it('serviceById', async () => {
		expect(1).to.equal(1);
	});
	it('serviceList', async () => {
		expect(1).to.equal(1);
	});
	it('serviceTypes', async () => {
		expect(1).to.equal(1);
	});
	it('serviceTypes', async () => {
		expect(1).to.equal(1);
	});
	it('myBookedServiceList', async () => {
		expect(1).to.equal(1);
	});
});
